/**
 * Calculate manual rollup fields on a parent record.
 * <p>
 * Usage:
 * <blockquote>
 *    Database.executeBatch(new CfG_RollupUpdate(CfG_MyObjectHandler.getRollupSpecs()));
 * </blockquote>
 *
 * @author Steve Cox
 */
public class CfG_RollupUpdate implements Database.Batchable<SObject> {
  //--------------------------------------------------------------------------
  // Properties
  final CfG_Rollup.Spec[] specs;
  final String parentName;
  
  //--------------------------------------------------------------------------
  // Constructor
  /**
   * create a rollup batch job with the specified specs
   * @param specs specification for the rollup
   */
  public CfG_RollupUpdate(CfG_Rollup.Spec[] specs) {
    CfG.preCondition(null != specs, 'CfG_RollupUpdate() - missing specs');
    
    // get the name of the parent object. There can only be one
    for (CfG_Rollup.Spec s : specs) {
      final String p = s.parent.getDescribe().getName();
      if ((null == p) || ((null != parentName) && (p != parentName))) {
        throw new CfG.AssertionException('Only one parent allowed in bulk rollup');
      }
      parentName = p;
    }
    
    this.specs = specs;
  }
  
  //--------------------------------------------------------------------------
  // Methods
  public Database.QueryLocator start(Database.BatchableContext BC) {
    CfG.preCondition(!CfG.isEmpty(parentName), 'CfG_RollupUpdate.start() - missing parentName');
    
    String soqlQuery = String.format('SELECT Id FROM {0} {1}', 
        new String[] {
            parentName,
            (Test.isRunningTest() ? ' ORDER BY CreatedDate DESC LIMIT 200' : ' ORDER BY Id')
        }
    );
    
    return Database.getQueryLocator(soqlQuery);
  }
  
  public void execute(Database.BatchableContext BC, SObject[] records) {
    final Set<String> parendIds = CfG.getIds(records);
    SObject[] parentUpdates = new SObject[] {};
    for (CfG_Rollup.Spec s : specs) {
      CfG_Rollup.fields(s, parendIds, parentUpdates);
    }
    update parentUpdates;
  }
  
  public void finish(Database.BatchableContext BC) {
    if (CfG.debugging) {
      notify(BC.getJobId());
    }
  }
  
  public void notify(Id jobId) {
    final AsyncApexJob a = [SELECT Status, NumberOfErrors, JobItemsProcessed,
      TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = :jobId];

    CfG.notifyCurrentUser(parentName + ' rollup update: ' + a.Status,
      'Processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures');
  }
}