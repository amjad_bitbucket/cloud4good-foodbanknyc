public class Program_RelationshipHandler extends CfG_BaseHandler implements CfG_ITrigger {
  //--------------------------------------------------------------------------
  // Methods
  public override void afterInsert(SObject so) {
    super.afterInsert(so);
    setClientHouseholdSize(null, so);
  }
  
  public override void afterUpdate(SObject oldSo, SObject so) {
    super.afterUpdate(oldSo, so);
    setClientHouseholdSize(null, so);
  }
  
  
  //--------------------------------------------------------------------------
  // Helpers
  private void setClientHouseholdSize(SObject oldSo, SObject so) {
    Program_Relationship__c old = (Program_Relationship__c)oldSo;
    Program_Relationship__c o = (Program_Relationship__c)so;
    if ((null != o.Client__c) && ((null == old) || (old.Household_Size_num__c != o.Household_Size_num__c))) {
      Client__c u = (Client__c)getUpdate(o.Client__c, Client__c.SObjectType);
      u.Total_Household_Size__c = o.Household_Size_num__c;
    }
  }
}