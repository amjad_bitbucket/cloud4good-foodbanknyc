public class TaxVirtualDocUploadController{
 private final sObject mysObject;
    private ApexPages.StandardController theController;    
    
    public TaxVirtualDocUploadController(ApexPages.StandardController stdController)
    {
        theController = stdController;
        this.mysObject = (sObject)stdController.getRecord();
    }
 public Flow.Interview.TxYr2016CompleteDocumentUpload goflow {get;set;}

public PageReference pRefFinishLocation{      
get {
            String temp = '/apex/Tax_Virtual_Intake_pg?sfdc.tabName=01rE00000001wq6'; 
            if(goflow!=null && goflow.finishvar=='Yes') temp = ('/' + string.valueOf(goflow.varProgramRelationshipTaxID));
            PageReference pRef = new PageReference(temp);
            return pRef;
}
set { pRefFinishLocation = value; }
 } }