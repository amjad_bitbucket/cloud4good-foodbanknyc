/**
 * A schedulable Apex unit test processor.
 *  <p>The following call schedules unit test processing every day at 6am
 *  (Logged-in user's time):<br/>
 *  <pre>System.schedule('Process Unit Tests', '0 0 6 * * ?', new CfG_Scheduler('CfG_TestJobProcessor'));</pre>
 *
 * <p>see <a href="http://developer.force.com/cookbook/recipe/automated-unit-test-execution">Luke Freeland's article</a>
 *
 * @author Steve Cox
 */
public class CfG_TestJobProcessor implements CfG_Scheduler.ISchedulable {
  //--------------------------------------------------------------------------
  // Constants
  private static final String CRLF = '\r\n';
  
  /** Subject of email sent when job completes */
  private static final String SUBJECT = 'CfG Unit Test Results ({0})';
  
  /** Comma-separated list of emails to notify when job completes */
  private static final String[] TO = new String[] {
    'scox67@gmail.com'
  };
  
  @TestVisible private static final String PASS_STATUS = 'Pass';
  @TestVisible private static final String COMPLETED_STATUS = 'Completed';

  //--------------------------------------------------------------------------
  // Methods
  public void execute(SchedulableContext context) {
    futureExecute();
  }

  // Use a future method so the email will be sent out.
  @TestVisible @future (callout = true)
  private static void futureExecute() {
    processAsyncResults();
  }
  
  //--------------------------------------------------------------------------
  // Helper methods
  @TestVisible private static void processAsyncResults() {
    processAsyncResults(null, null, null);
  }
  
  @TestVisible private static void processAsyncResults(ApexTestQueueItem[] qi,
    Map<Id, ApexTestQueueItem[]> jt, ApexTestResult[] tr) {
    
    // get all the unit test jobs currently in our queue
    CfG_QueuedTest__c[] queuedTests = [SELECT AsyncId__c FROM CfG_QueuedTest__c];
    Set<Id> queuedTestIds = new Set<Id>();
    for (CfG_QueuedTest__c q : queuedTests) {
      queuedTestIds.add(q.AsyncId__c);
    }
    
    ApexTestQueueItem[] queuedItems = (null != qi) ? qi :
      [SELECT ApexClass.Name, Status, ParentJobId FROM ApexTestQueueItem
        WHERE ParentJobId in :queuedTestIds];
     
    Map<Id, ApexTestQueueItem[]> jobTests = (null != jt) ? jt : new Map<Id, ApexTestQueueItem[]>();
    for (ApexTestQueueItem i : queuedItems) {
      ApexTestQueueItem[] groupedTests = jobTests.containsKey(i.ParentJobId) ?
        jobTests.get(i.ParentJobId) : new ApexTestQueueItem[] {};
      groupedTests.add(i);
      jobTests.put(i.ParentJobId, groupedTests);
    }
    
    Set<Id> completedJobs = jobTests.keySet();
    for (ApexTestQueueItem[] tests : jobTests.values()) {
      for (ApexTestQueueItem test : tests) {
        if (test.Status != COMPLETED_STATUS) {
          completedJobs.remove(test.ParentJobId);
        }
      }
    }
    
    if (!completedJobs.isEmpty() || Test.isRunningTest()) {
      ApexTestResult[] testResults = (null != tr) ? tr :
        [SELECT Outcome, MethodName, Message, StackTrace,
        AsyncApexJobId, ApexClass.Name, ApexClass.Body, ApexClass.LengthWithoutComments,
        ApexClass.NamespacePrefix, ApexClass.Status, ApexLogId, ApexLog.DurationMilliseconds,
        ApexLog.Operation, ApexLog.Request, ApexLog.Status, ApexLog.Location,
        ApexLog.Application FROM ApexTestResult WHERE AsyncApexJobId in :completedJobs];
      
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      mail.setToAddresses(TO);
      mail.setSubject(String.format(SUBJECT, new String[] {String.valueOf(Date.today())}));
      mail.setPlainTextBody(getTestResultEmailBody(testResults));
      Messaging.SendEmailResult[] results = Test.isRunningTest() ? new Messaging.SendEmailResult[]{} :
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
      
      CfG_QueuedTest__c[] queuedTestsToDelete = new CfG_QueuedTest__c[] {};
      for (CfG_QueuedTest__c t : queuedTests) {
        for (Id i : completedJobs) {
          if (t.AsyncId__c == i || Test.isRunningTest()) {
            queuedTestsToDelete.add(t);
            break;
          }
        }
      }

      delete queuedTestsToDelete;
    }
  }
  
  @TestVisible private static String getTestResultEmailBody(ApexTestResult[] testResults) {
    ApexTestResult[] successTests = new ApexTestResult[] {};
    ApexTestResult[] failedTests = new ApexTestResult[] {};
    for (ApexTestResult j : testResults) {
      if (j.Outcome == PASS_STATUS) {
        successTests.add(j);
      } else {
        failedTests.add(j);
      }
    }

    String body = '';
    body += 'Completed: ' + testResults.size() + CRLF;
    body += 'Passed: ' + successTests.size() + CRLF;
    body += 'Failed: ' + failedTests.size() + CRLF + CRLF;

    if (failedTests.size() > 0) {
      body += '=============================================================' + CRLF;
      body += 'Test Failures ===============================================' + CRLF + CRLF;
      
      Integer i = 1;
      for (ApexTestResult f : failedTests) {
        body += '-------------------------------------------------------------' + CRLF;
        body += i + ') ' + f.ApexClass.Name + ':' + f.MethodName + CRLF;
        body += f.message + CRLF;
        body += f.stackTrace + CRLF + CRLF;
        ++i;
      }
    }
    
    return body;
  }
}