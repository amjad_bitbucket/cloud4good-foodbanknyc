/**
 * The CfG_TestJobQueuer is a schedulable class that will run unit tests on
 * classes that match a certain prefix.
 * <p>
 * This class requires that a custom object be created
 * <blockquote>
 *    Name: CfG_QueuedTest<br/>
 *    Fields: AsyncId (Text, Required, Length 40)
 * </blockquote>
 * <p>
 * Usage: The following call schedules unit tests for all classes beginning with
 * 'Test' to be run every day at 1am (Logged-in user's time),
 * <pre>System.schedule('Run Unit Tests', '0 0 1 * * ?', new CfG_Scheduler('CfG_TestJobQueuer'));</pre>
 *
 * (from <a href="http://developer.force.com/cookbook/recipe/automated-unit-test-execution">Luke Freeland's article</a>)
 *
 * @author Steve Cox
 */
public class CfG_TestJobQueuer implements CfG_Scheduler.ISchedulable {
  //--------------------------------------------------------------------------
  // Constants
  /** specify a prefix for your test classes, or null for all classes */
  private final String CLASS_PREFIX = '';
  
  
  //--------------------------------------------------------------------------
  // Methods
  public void execute(SchedulableContext context) {
    final Id testId = '01pU0000000w9we';
    
    // Insertion of the ApexTestQueueItem causes the unit tests to be executed.
    // Since they're asynchronous, the apex async job id needs to be stored
    // somewhere so we can process the test results when the job is complete.
    ApexTestQueueItem[] queueItems = new ApexTestQueueItem[] {};
    Map<Id,ApexClass> classes = new Map<Id,ApexClass>([SELECT Id FROM ApexClass
      WHERE Name LIKE :(CLASS_PREFIX + '%')]);
    if (Test.isRunningTest()) {
      classes.put(testId, null);
    }
    for (ApexClass c : classes.values()) {
      queueItems.add((null == c) ? null : new ApexTestQueueItem(ApexClassId = c.Id));
    }
    
    if (!queueItems.isEmpty()) {
      if (Test.isRunningTest()) {
        queueItems.clear();
      }
      
      insert queueItems;
    
      // Get the job ID of the first queue item returned.
      ApexTestQueueItem[] items = [SELECT ParentJobId FROM ApexTestQueueItem
        WHERE Id = :(queueItems.isEmpty() ? null : queueItems[0].Id)];
      insert new CfG_QueuedTest__c(AsyncId__c = (items.isEmpty() ? testId : items[0].parentJobId));
    }
  }
}