@IsTest(SeeAllData=true)
class Program_RelationshipTest extends CfG_BaseTest {
  //--------------------------------------------------------------------------
  // Tests
  static testMethod void testCopyHhSizeWith() {
    System.runAs(t.adminUser) {
      t.init();
      
      Client__c[] clients = new Client__c[] {
        newClient('c0'),
        newClient('c1')
      };
      insert clients;
      
      Program_Relationship__c[] programs = new Program_Relationship__c[] {
        newProgram(null, null),
        newProgram(clients[0].Id, null),
        newProgram(clients[1].Id, 3)
      };
      
      t.start();
        // insert
        insert programs[0];
        upsert programs;
        System.assertEquals(0, [SELECT Total_Household_Size__c FROM Client__c WHERE Id = :clients[0].Id].Total_Household_Size__c);
        System.assertEquals(3, [SELECT Total_Household_Size__c FROM Client__c WHERE Id = :clients[1].Id].Total_Household_Size__c);
        
        // update
        programs[1].Number_of_Adults__c = '4';
        update programs[1];
        System.assertEquals(4, [SELECT Total_Household_Size__c FROM Client__c WHERE Id = :clients[0].Id].Total_Household_Size__c);
      t.stop();
    }
  }
  
  
  //--------------------------------------------------------------------------
  // Helpers
  private static Program_Relationship__c newProgram(Id clientId, Integer numberOfAdults) {
    return new Program_Relationship__c(
      Client__c = clientId,
      Number_of_Adults__c = (null != numberOfAdults) ? String.valueOf(numberOfAdults) : null
    );
  }
  
  private static Client__c newClient(String lastName) {
    return new Client__c(Last_Name__c = lastName);
  }
  
  static { CfG_BaseTest.t = new Program_RelationshipTest(); }
  static CfG_BaseTest t { get { return CfG_BaseTest.t; } }
}