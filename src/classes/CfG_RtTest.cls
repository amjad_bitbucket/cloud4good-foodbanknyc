@IsTest(SeeAllData=true)
class CfG_RtTest extends CfG_BaseTest {
  //--------------------------------------------------------------------------
  // Tests
  static testMethod void testRt() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        try {
          CfG_Rt.getId(Case.SObjectType, 'bogus');
          System.assert(false, 'invalid record type');
        } catch (CfG.AssertionException ex) {
        }
        
        /* exercise an existing record type
        System.assertNotEquals(null, CfG_Rt.getId(CfG_Test__c.SObjectType, CfG_Rt.Name.Test1));*/
      t.stop();
    }
  }
  
  //--------------------------------------------------------------------------
  // Helpers
  static { CfG_BaseTest.t = new CfG_RtTest(); }
  static CfG_BaseTest t { get { return CfG_BaseTest.t; } }
}