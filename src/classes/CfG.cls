/**
 * A utility class for app-wide constants, exceptions, and utility functions
 *
 * @author Steve Cox
 */
public class CfG {
  //--------------------------------------------------------------------------
  // Constants
  /** The package prefix, including '__', if the CfG library is part of a package */
  public static final String PKG = '';
  
  /** ASC sort indicator for use in SOQL queries */
  public static final String SOQL_ASC = 'ASC';
  /** DESC sort indicator for use in SOQL queries */
  public static final String SOQL_DESC = 'DESC';
  
  /** date orders for use with parseDate */
  public enum DATE_ORDER { MDY, YMD }
  
  /** the prefix to use on all System.debug calls */
  @TestVisible private static final String DEBUG_PREFIX = '~ ';
  
  /** active ApexTrigger.Status */
  private static final String TRIGGER_ACTIVE_STATUS = 'Active';
  
  
  //--------------------------------------------------------------------------
  // Exceptions
  /** base class for all exceptions */
  public abstract class CfG_Exception extends Exception {
  }
  
  /** a failed assertion */
  public class AssertionException extends CfG_Exception {
  }
  
  /** a data validation error of some sort */
  public class ValidationException extends CfG_Exception {
  }
  
  /** error in trigger definition or setup */
  public class TriggerException extends CfG_Exception {
  }
  
  /** HTTP error, including the status code */
  public class HttpException extends CfG_Exception {
    public Integer code { get; private set; }
    
    public HttpException(Integer code, String msg) {
      this(msg);
      this.code = code;
    }
  }
  
  
  //--------------------------------------------------------------------------
  // Diagnostics
  /** Use this flag to conditionally provide diagnostics */
  public static Boolean debugging { get { return CfG__c.getInstance().debugging__c; } }
  
  /** Write an error message to the log */
  public static void error(Object message) {
    debug(LoggingLevel.ERROR, message);
  }
  
  /** Write a warning message to the log */
  public static void warn(Object message) {
    debug(LoggingLevel.WARN, message);
  }
  
  /** Write an info message to the log */
  public static void info(Object message) {
    debug(LoggingLevel.INFO, message);
  }
  
  /** Write a debug message to the log */
  public static void debug(Object message) {
    debug(LoggingLevel.DEBUG, message);
  }
  
  /** begin a debugging 'context'. Subsequent logging calls will be indented */
  public static void debugBegin(Object message) {
    cachingDebugEntries = true;
    debug(LoggingLevel.DEBUG, message);
    ++debugEntryLevel;
  }
  
  /** end a debugging 'context'. Subsequent logging calls will be outdented */
  public static void debugEnd() {
    if (debugEntryLevel > 0) {
    --debugEntryLevel;
    }
    
    if ((0 == debugEntryLevel) && debugging) {
      System.debug(LoggingLevel.ERROR, debugEntriesToString());
      debugEntries.clear();
      cachingDebugEntries = false;
    }
  }
  
  /** end all debugging 'contexts'. Indent level is set back to 0 */
  public static void debugEndAll() {
    debugEntryLevel = 0;
    debugEnd();
  }
  
  /** when debugging, log and create an CfG_Log__c entry for the specified exception */
  public static void debugException(Exception ex) {
    if (debugging) {
      final Boolean saveCaching = cachingDebugEntries;
      cachingDebugEntries = true;
      debug(LoggingLevel.ERROR, 'Exception: ' + ex.getMessage() + '; ' + ex.getStackTraceString());
      cachingDebugEntries = saveCaching;
    
      insert new CfG_Log__c(Name = 'Exception', Text__c = debugEntriesToString().left(32768));
    }
  }
  
  public static void debug(LoggingLevel level, Object message) {
    if (debugging) {
      if (cachingDebugEntries) {
        debugEntries.add(new DebugEntry(DEBUG_PREFIX + message));
      } else {
        System.debug(level, DEBUG_PREFIX + message);
      }
    }
  }
  
  /** Assert a condition; throws on failure when debugging */
  public static void assert(Boolean condition) {
    assert(condition, null);
  }
  
  /** Assert equality; throws on failure when debugging */
  public static void assertEquals(Object v1, Object v2) {
    assert(v1 == v2, null);
  }
  
  /** Assert equality with a message; throws on failure when debugging */
  public static void assertEquals(Object v1, Object v2, Object message) {
    assert(v1 == v2, message);
  }
  
  /** Assert inequality; throws on failure when debugging */
  public static void assertNotEquals(Object v1, Object v2) {
    assert(v1 != v2, null);
  }
  
  /** Assert inequality with a message; throws on failure when debugging */
  public static void assertNotEquals(Object v1, Object v2, Object message) {
    assert(v1 != v2, message);
  }
  
  /** Assert a precondition with a message; throws on failure when debugging */
  public static void preCondition(Boolean condition, Object message) {
    assert(condition, 'preCondition failed: ' + message);
  }
  
  /** Assert a postcondition; throws on failure when debugging */
  public static void postCondition(Boolean condition, Object message) {
    assert(condition, 'postCondition failed: ' + message);
  }
  
  /** Assert an invariant; throws on failure when debugging */
  public static void invariant(Boolean condition, Object message) {
    assert(condition, 'invariant failed: ' + message);
  }
  
  /** Assert a condition; throws on failure when debugging*/
  public static void assert(Boolean condition, Object message) {
    if (debugging && !condition) {
      throw new AssertionException(String.valueOf(message));
    }
  }
  
  private static Boolean cachingDebugEntries = false;
  private static Integer debugEntryLevel = 0;
  
  /** A class for caching debug entires for later persistence */
  public class DebugEntry {
    public Integer level = debugEntryLevel;
    public String text;
    
    public DebugEntry(String text) {
      this.text = text;
    }
  }
  
  private static DebugEntry[] debugEntries = new DebugEntry[]{};
  
  @TestVisible private static String debugEntriesToString() {
    String result = '';
    for (DebugEntry e : debugEntries) {
      result += e.text.leftPad(e.level * 2 + e.text.length()) + '\n';
    }
    
    return result;
  }
  
  
  //--------------------------------------------------------------------------
  // Utility functions
  /**
   * given an object field, return the pick list values/labels as
   * a list of SelectOptions that can be displayed by a VF page.
   */
  public static SelectOption[] getPickListAsSelectOptions(SObjectField field) {
    preCondition(field != null, 'CfG.getPickListAsSelectOptions() - field is required');
    
    SelectOption[] result = new SelectOption[]{};
    for (PicklistEntry e : field.getDescribe().getPickListValues()) {
      result.add(new SelectOption(e.getValue(), e.getLabel()));
    }

    postCondition(result != null, 'CfG.getPickListAsSelectOptions() - null result');
    return result;
  }
  
  /** given an object field, return the pick list values */
  public static Set<String> getPickListValues(SObjectField field) {
    preCondition(field != null, 'CfG.getPickListValues() - field is required');
    
    Set<String> result = new Set<String>();
    for (PicklistEntry e : field.getDescribe().getPickListValues()) {
      result.add(e.getValue());
    }

    postCondition(result != null, 'CfG.getPickListValues() - null result');
    return result;
  }
  
  /**
   * String formatting short cut
   * <p>
   * Note: single apostrophes need to be double escaped.
   * @see String.format
   */
  public static String format(String format, Object arg1) {
    preCondition(format != null, 'CfG.format() - format is required');
    
    return String.format(format.replaceAll('\'', '\'\''), new String[] {
      String.valueOf(arg1)});
  }
  
  public static String format(String format, Object arg1, Object arg2) {
    preCondition(format != null, 'CfG.format() - format is required');
    
    return String.format(format.replaceAll('\'', '\'\''), new String[] {
      String.valueOf(arg1), String.valueOf(arg2)});
  }
  
  public static String format(String format, Object arg1, Object arg2, Object arg3) {
    preCondition(format != null, 'CfG.format() - format is required');
    
    return String.format(format.replaceAll('\'', '\'\''), new String[] {
      String.valueOf(arg1), String.valueOf(arg2), String.valueOf(arg3)});
  }

  /**
   * utility method for determining if an object is null or blank.
   *  For strings, 'blank' means empty or only whitespace. A 'blank' List has
   *  no elements.
   * <p>
   * <b>WARNING</b> this method doesn't work with Set or Map
   */
  public static Boolean isEmpty(Object o) {
    if (null == o) {
      return true;
    } else if (o instanceof String) {
      return (0 == ((String)o).trim().length());
    } else if (o instanceof List<Object>) {
      return (0 == ((List<Object>)o).size());
    } else {
      preCondition(false, 'CfG.isEmpty() - invalid object type');
      return false;
    }
  }

  /** return the specified string page parameter, or 'defaultValue' */
  public static String getPageParam(String key, String defaultValue) {
    preCondition(!isEmpty(key), 'CfG.getPageParam() - key is required');
    
    final String param = ApexPages.currentPage().getParameters().get(key);
    return (null == param) ? defaultValue : param;
  }
  
  /** return the specified integer page parameter, or 'defaultValue' */
  public static Integer getPageParam(String key, Integer defaultValue) {
    preCondition(!isEmpty(key), 'CfG.getPageParam() - key is required');
    
    try {
      if (ApexPages.currentPage().getParameters().containsKey(key)) {
        return Integer.valueOf(ApexPages.currentPage().getParameters().get(key));
      }
    } catch (Exception e) {
    }
    
    return defaultValue;
  }
  
  /**
   * @param key query parameter accepting comma-separated list of values
   * @return values as an array of strings
   */
  public static String[] getPageParams(String key) {
    final String param = getPageParam(key, (String)null);
    final String[] result = ((null != param) && ('null' != param)) ? param.split(',') : new String[]{};
    postCondition(result != null, 'CfG.getPageParams() - null result');
    return result;
  }
  
  /**
   * redirect helper
   * @param nextPage the page to redirect to
   * @param params a map of key => value pairs
   * @return the page reference to the new page
  */
  public static PageReference redirect(PageReference nextPage, Map<String,String> params) {
    preCondition(nextPage != null, 'CfG.redirect() - nextPage is required');
    preCondition(params != null, 'CfG.redirect() - params is required');
    
    nextPage.getParameters().putAll(params);
    nextPage.setRedirect(false);  // allow a postback rather than a get if possible
    return nextPage;
  }
  
  public static PageReference redirect(PageReference nextPage) {
    return redirect(nextPage, new Map<String,String>());
  }
  
  /**
   * return the unique values for a given field in a list of records. Null is not included.
   * @param objects the list of records
   * @param field values from this field will be returned
   * @return set of values; no null
   */
  public static Set<String> getFieldValues(SObject[] objects, SObjectField field) {
    return getFieldValues(objects, field.getDescribe().getName());
  }
  
  public static Set<String> getFieldValues(SObject[] objects, String field) {
    preCondition(!isEmpty(field), 'CfG.getFieldValues() - field is required');
    
    Set<String> result = new Set<String>();
    if (!isEmpty(objects)) {
      for (SObject o : objects) {
        result.add(String.valueOf(o.get(field)));
      }
      result.remove(null);
    }
    
    postCondition(result != null, 'CfG.getFieldValues() - null result');
    postCondition(!result.contains(null), 'CfG.getFieldValues() - null value');
    return result;
  }
  
  /**
   * return the unique Ids in a list of records.
   * @param objects the list of records
   * @return set of Ids; no null
   */
  public static Set<String> getIds(SObject[] objects) {
    return getFieldValues(objects, Case.Id);
  }
  
  /** return true if the trigger for the specified object is active */
  public static Boolean isTriggerActive(SObjectType o) {
    preCondition(null != o, 'CfG.isTriggerActive() - object type is required');
    
    // first check the global trigger flag
    try {
      if (false == Boolean.valueOf(CfG__c.getInstance().get('Enable_Triggers__c'))) {
        return false;
      }
    } catch (Exception ex) {
    }
    
    // next check the flag for this specific trigger
    final String triggerName = String.valueOf(o).removeEnd('__c').remove(PKG).replace('__', '_');
    try {
      CfG_BaseTest.throwIfTesting(new NullPointerException());
      if (false == Boolean.valueOf(CfG__c.getInstance().get('Enable_' + triggerName + '_Triggers__c'))) {
        return false;
      }
    } catch (Exception ex) {
    }
    
    return [SELECT Count() FROM ApexTrigger WHERE Name = :triggerName AND Status = :TRIGGER_ACTIVE_STATUS] > 0;
  }
  
  public static void notifyCurrentUser(String subject, String body) {
    if (debugging) {
      final String to = [SELECT Email FROM User WHERE Id = :UserInfo.getUserId()].Email;
      if (!isEmpty(to)) {
        new Email(new String[] {to}, null, null, subject, body).send();
      }
    }
  }
  
  /** return the string converted to an ID, or null if the ID is invalid */
  public static Id stringToId(String s) {
    try {
      return s;
    } catch (Exception e) {
      return null;
    }
  }
  
  /**
   * Convert a base64-encoded string to a Blob
   * @param data the string to decode and convert
   * @return the blob on success; otherwise, null if decoding failed (probably over 10MB)
   */
  public static Blob fileDataToBlob(String data) {
    preCondition(!isEmpty(data), 'CfG.fileDataToBlob - data is required');
    
    final String base64 = data.substring(data.indexOf(',') + 1);
    final Blob actualData = EncodingUtil.base64Decode(base64);
    return (actualData.size() > 0) ? actualData : null;
  }
  
  /**
   * Parses a string with specified format. It is assumed that the date parts are
   * integers.
   *
   * @param dateAsString the string
   * @param sep the separator (i.e. '-', '/'); must be at least one character
   * @param format the order of date parts
   * @return the date or null, if any problem occurs
   */
  public static Date parseDate(String dateAsString, String sep, DATE_ORDER format) {
    preCondition((null != sep) && (sep.length() > 0), 'CfG.parseDate - sep is required');
    preCondition(null != format, 'CfG.parseDate - format is required');
    
    try {
      String[] d = dateAsString.split('\\' + sep);
      if (DATE_ORDER.MDY == format) {
        return Date.newInstance(Integer.valueOf(d[2].trim()), Integer.valueOf(d[0].trim()), Integer.valueOf(d[1].trim()));
      } else {
        return Date.newInstance(Integer.valueOf(d[0].trim()), Integer.valueOf(d[1].trim()), Integer.valueOf(d[2].trim()));
      }
    } catch (Exception e) {
      return null;
    }
  }
  
  /** Content types for various file extensions */
  private static final Map<String,String> TYPES = new Map<String,String> {
    'pdf' => 'application/pdf',
    'xls' => 'application/vnd.ms-excel'
  };
  
  /** Get the content type for the specified file */
  public static String getFileType(String filename) {
    if (!isEmpty(filename)) {
      Integer dot = filename.lastIndexOf('.');
      String ext = (dot >= 0) ? filename.substring(dot + 1, filename.length()) : '';
      return TYPES.get(ext.trim().toLowerCase());
    } else {
      return null;
    }
  }
  
  /** Email utility class */
  public class Email {
    private Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    @TestVisible private Messaging.EmailFileAttachment[] attachments = new Messaging.EmailFileAttachment[]{};
    
    public Email(String[] to, String[] cc, String[] bcc, String subject, String body) {
      mail.setToAddresses(to);
      mail.setCcAddresses(cc);
      mail.setBccAddresses(bcc);
      mail.setSubject(subject);
      mail.setPlainTextBody(body);
    }
    
    public void addAttachment(String filename, Blob data) {
      preCondition(null != data, 'CfG.Email.addAttachment - data is required');
      Messaging.EmailFileAttachment a = new Messaging.EmailFileAttachment();
      a.setContentType(getFileType(filename));
      a.setFileName(filename);
      a.setBody(data);
      attachments.add(a);
    }
    
    public void addAttachment(Attachment a) {
      preCondition(null != a, 'CfG.Email.addAttachment - attachment is required');
      addAttachment(a.Name, a.Body);
    }
    
    public void send() {
      mail.setFileAttachments(attachments);
      Messaging.SendEmailResult[] results = Test.isRunningTest() ? new Messaging.SendEmailResult[]{} :
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
      for (Messaging.SendEmailResult r : results) {
        if (!r.isSuccess()) {
          CfG.debug(r.getErrors());
        }
      }
    }
  }
}