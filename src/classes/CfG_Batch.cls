/**
 * <p>Generic class for scheduled batch operations. The goal here is to be able to
 * reduce Apex code by providing a general utility that handles the majority of
 * batch DML needs.</p>
 *
 * <p>This class also contains the Monitor utility, which can be scheduled to
 * auto-start and stop CfG_Batch processes specified in the CfG Job custom settings.
 * </p>
 *
 * <p>The following call updates all accounts:<br/>
 * <pre>Database.executeBatch(new CfG_Batch('Test', CfG_Batch.OP.OP_UPDATE, 'SELECT Id FROM Account', null));</pre>
 *
 * <p>A handler can be used for more sophisticated processing:
 * <pre>
 * class AccountNameUpdater implements CfG_Batch.Handler {
 *   public void handle(SObject[] scope) {
 *     for (Account a : (Account[])scope) {
 *       o.Name += 'TEST';
 *     }
 *   }
 * }<br>
 *
 * CfG_Batch process = new CfG_Batch('Test', CfG_Batch.OP.OP_UPDATE, 'SELECT Name FROM Account',
 *   new AccountNameUpdater(), new String[] {'scox67@gmail.com'});<br>
 *
 * // run our process once
 * Database.executeBatch(process);<br>
 *
 * // schedule our process to run each day at 2 am
 * System.schedule('Account Update Job', '0 0 2 ? * *', process);<br>
 *
 * // abort and deactivate all CfG Jobs
 * CfG_Batch.abortAllJobs();
 * </pre>
 *
 * @see CfG_Scheduler
 * @author Steve Cox
 */
public class CfG_Batch implements Database.Batchable<SObject>, Database.Stateful,
  Database.AllowsCallouts, Schedulable {
  
  //--------------------------------------------------------------------------
  // Constants
  /** available operations */
  public enum OP { OP_UPDATE, OP_DELETE, OP_UNDELETE }
  
  private final static String ALL_ROWS =  ' ALL ROWS ';
  
  @testVisible private final static Integer MIN_BATCH_SIZE = 1;
  @testVisible private final static Integer MAX_BATCH_SIZE = 2000;
  @testVisible private final static Integer DEFAULT_BATCH_SIZE = 200;

  // {0} = process name, {1} = status
  private final static String NOTIFY_SUBJECT = 'Salesforce Batch Process: {0} - {1}';
  // {0} = total batches processed, {1} = number of failures
  private final static String NOTIFY_BODY = '{0} batch(es) processed, with {1} failure(s).';
  
  private final static Map<String,CfG_Batch.OP> OP_MAP = new Map<String,CfG_Batch.OP> {
    'update'   => CfG_Batch.OP.OP_UPDATE,
    'delete'   => CfG_Batch.OP.OP_DELETE,
    'undelete' => CfG_Batch.OP.OP_UNDELETE
  };
  
  //--------------------------------------------------------------------------
  // Properties
  @testVisible private String name;
  @testVisible private String query;
  @testVisible private OP operation;
  @testVisible private Handler handler;
  @testVisible private String[] notify;
  @testVisible private Integer batchSize;
  
  
  //--------------------------------------------------------------------------
  // Constructor
  /**
   * Create a batch process
   * @param name optional name of the process. This will be reported in the
   *  notification email.
   * @param operation the operation to perform
   * @param query the query used to select records
   * @param handler an optional handler. If specified, h.handle() will be called
   *  and passed the array of objects before the operation is performed.
   * @param notify optional email addresses for finish or error notifications
   */
  public CfG_Batch(String name, OP operation, String query, Handler handler, String[] notify, Integer batchSize) {
    CfG.preCondition(null != operation, 'CfG_Batch.CfG_Batch() - missing operation');
    CfG.preCondition(!CfG.isEmpty(query), 'CfG_Batch.CfG_Batch() - missing query');
    
    this.name = name;
    this.operation = operation;
    this.handler = handler;
    this.notify = notify;
    this.batchSize = (null != batchSize) ? batchSize : DEFAULT_BATCH_SIZE;
    this.batchSize = Math.min(MAX_BATCH_SIZE, Math.max(MIN_BATCH_SIZE, this.batchSize));
    
    if ((OP.OP_UNDELETE == operation) && !query.contains(ALL_ROWS)) {
      this.query = query + ALL_ROWS;
    } else {
      this.query = query;
    }
    
    // validate that the query is valid
    Database.query(this.query);
  }
  
  /** implement this interface to provide additional processing for records in your batch process */
  public interface Handler {
    SObject[] handle(SObject[] scope);
  }
  
  /** Database.Batchable method */
  public Database.QueryLocator start(Database.BatchableContext context) {
    return Database.getQueryLocator(query);
  }
  
  /** Database.Batchable method */
  public void execute(Database.BatchableContext context, SObject[] scope) {
    if (null != handler) {
      scope = handler.handle(scope);
    }
    
    if (OP.OP_UPDATE == operation) {
      update scope;
    } else if (OP.OP_DELETE == operation) {
      delete scope;
    } else if (OP.OP_UNDELETE == operation) {
      undelete scope;
    }
  }
  
  /** Database.Batchable method */
  public void finish(Database.BatchableContext context) {
    if (null != notify) {
      AsyncApexJob a = [SELECT Status, NumberOfErrors, JobItemsProcessed, TotalJobItems,
        CreatedBy.Name FROM AsyncApexJob WHERE Id = :context.getJobId()];
      
      final String subject = String.format(NOTIFY_SUBJECT, new String[]{
        String.valueOf(name), String.valueOf(a.Status)});
      final String body = String.format(NOTIFY_BODY, new String[]{
        String.valueOf(a.TotalJobItems), String.valueOf(a.NumberOfErrors)});
      
      new CfG.Email(notify, null, null, subject, body).send();
    }
  }
  
  /** Schedulable method */
  public void execute(SchedulableContext SC) {
    Database.executeBatch(new CfG_Batch(name, operation, query, handler, notify, batchSize), batchSize);
  }
  
  /**
   * The Monitor will watch your CfG Job custom settings and make changes as
   * necessary.
   *  - if a job is no longer active, it will be aborted
   *  - if a job is active and not running, it will be scheduled
   *  - if a job is active and running but the schedule has changed, it will
   *    be restarted
   */
  public class Monitor implements CfG_Scheduler.ISchedulable {
    public void execute(SchedulableContext SC) {
      Map<Id,CfG_Job__c> jobsToUpdate = new Map<Id,CfG_Job__c>();
      CfG_Job__c[] jobs = [SELECT Name, Schedule__c, Operation__c, Query__c, Handler__c,
        Active__c, Job_Id__c, Email_Recipients__c, Batch_Size__c FROM CfG_Job__c];
      
      for (CfG_Job__c j : jobs) {
        if (j.Active__c) {
          // get the cron trigger object; remove the job Id if it's invalid
          CronTrigger t = new CronTrigger();
          if (!CfG.isEmpty(j.Job_Id__c)) {
            try {
              t = [SELECT State, CronExpression FROM CronTrigger WHERE Id = :j.Job_Id__c];
            } catch (Exception ex) {
              CfG.debugException(new CfG.AssertionException('Invalid Job Id in CfG Job "' + j.Name + '": ' + j.Job_Id__c));
              j.Job_Id__c = null;
              jobsToUpdate.put(j.Id, j);
            }
            
            // if the job has changed, abort the old one; the only thing the
            // CronTrigger object gives us to compare against is the schedule
            if (j.Schedule__c != t.CronExpression) {
              abortJob(j.Job_Id__c);
              j.Job_Id__c = null;
              jobsToUpdate.put(j.Id, j);
            }
          }
          
          if (CfG.isEmpty(j.Job_Id__c)) {
            j = scheduleJob(j);
            jobsToUpdate.put(j.Id, j);
          }
        } else if (!CfG.isEmpty(j.Job_Id__c)) {
          // abort the jobs that have been deactivated
          abortJob(j.Job_Id__c);
          j.Job_Id__c = null;
          jobsToUpdate.put(j.Id, j);
        }
      }
      
      update jobsToUpdate.values();
    }
  }
  
  /** Abort and deactivate all jobs specified in the CfG Job custom setting. */
  public static void abortAllJobs() {
    CfG_Job__c[] jobs = [SELECT Job_Id__c FROM CfG_Job__c WHERE Job_Id__c != null OR Active__c = true];
    for (CfG_Job__c j : jobs) {
      // if an exception is thrown, the job was most likely deleted manually
      abortJob(j.Job_Id__c);
      j.Job_Id__c = null;
      j.Active__c = false;
    }
    
    update jobs;
  }
  
  /** a handler for emailing a report of CfG_Log__c entries */
  public class LogEmailer implements Handler {
    public SObject[] handle(SObject[] scope) {
      CfG.preCondition(!CfG.isEmpty(scope), 'CfG_Batch.LogEmailer.handle() - scope is required');
      
      String body = '';
      for (CfG_Log__c l : (CfG_Log__c[])scope) {
        body += l.Name + ' created by ' + l.CreatedBy.Name + ' at ' +
          l.CreatedDate.format() + ': ' + l.Text__c + '\n\n';
      }
      
      final String to = CfG__c.getInstance().Log_Recipients__c;
      if (!CfG.isEmpty(to)) {
        new CfG.Email(to.split(','), null, null, 'CfG Log', body).send();
      }
      
      return new SObject[]{};
    }
  }
  
  
  //--------------------------------------------------------------------------
  // Helpers
  @TestVisible private static CfG_Job__c scheduleJob(CfG_Job__c j) {
    CfG.preCondition(null != j, 'CfG_Batch.scheduleJob() - missing job');
    
    // get the handler; remove it if it's invalid
    Handler h;
    try {
      h = CfG.isEmpty(j.Handler__c)? null : (Handler)Type.forName(j.Handler__c).newInstance();
    } catch (NullPointerException ex) {
      CfG.debugException(new CfG.AssertionException('Invalid handler specified in CfG Job "' + j.Name + '": ' + j.Handler__c));
      j.Handler__c = null;
      j.Active__c = false;
    }
    
    if (j.Active__c && CfG.isEmpty(j.Job_id__c)) {
      // start the job
      j.Active__c = false;
      try {
        final CfG_Batch b = new CfG_Batch(j.Name, OP_MAP.get(j.Operation__c.toLowerCase()),
          j.Query__c, h, (null != j.Email_Recipients__c)? j.Email_Recipients__c.split(',') : null,
          (Integer)j.Batch_Size__c);
        
        j.Job_Id__c = System.schedule(j.Name, j.Schedule__c, b);
        j.Active__c = true;
      } catch (Exception ex) {
        CfG.debug('CfG_Batch.scheduleJob: ' + ex.getMessage());
        CfG.debugException(ex);
      }
    }
    
    return j;
  }
  
  @TestVisible private static void abortJob(Id jobId) {
    try {
      System.abortJob(jobId);
    } catch (Exception ex) {
      // ignore any exceptions - the job was most likely deleted manually
    }
  }
}