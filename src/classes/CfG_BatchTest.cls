@IsTest(SeeAllData=true)
class CfG_BatchTest extends CfG_BaseTest {
  final static String Q_CASES = 'SELECT Id FROM Case LIMIT 10';
  final static String SCHED = '0 0 0 ? * *';
  
  //--------------------------------------------------------------------------
  // Tests
  static testMethod void testErrors() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        try {
          new CfG_Batch('Test', null, 'SELECT Id FROM Account', null, null, null);
          System.assert(false, 'missing operation');
        } catch (CfG.AssertionException ex) {
        }
        
        try {
          new CfG_Batch('Test', CfG_Batch.OP.OP_UPDATE, null, null, null, null);
          System.assert(false, 'missing query');
        } catch (CfG.AssertionException ex) {
        }
        
        try {
          new CfG_Batch('Test', CfG_Batch.OP.OP_UPDATE, ' ', null, null, null);
          System.assert(false, 'missing query');
        } catch (CfG.AssertionException ex) {
        }
        
        try {
          new CfG_Batch('Test', CfG_Batch.OP.OP_UPDATE, 'junk', null, null, null);
          System.assert(false, 'invalid query');
        } catch (QueryException ex) {
        }
      t.stop();
    }
  }
  
  static testMethod void testInvalidQuery() {
    System.runAs(t.adminUser) {
      try {
        t.init();
        t.start();
          Database.executeBatch(new CfG_Batch('Test', CfG_Batch.OP.OP_UPDATE, 'junk', null, null, null));
        t.stop();
        
        System.assert(false, 'invalid query');
      } catch (QueryException ex) {
      }
    }
  }
  
  static testMethod void testBatchSize() {
    System.runAs(t.adminUser) {
      t.init();
      
      final String q = 'SELECT Id FROM Account';
      t.start();
        CfG_Batch b = new CfG_Batch('b', CfG_Batch.OP.OP_UPDATE, q, null, null, null);
        System.assertEquals(CfG_Batch.DEFAULT_BATCH_SIZE, b.batchSize);
        
        b = new CfG_Batch('b', CfG_Batch.OP.OP_UPDATE, q, null, null, CfG_Batch.DEFAULT_BATCH_SIZE);
        System.assertEquals(CfG_Batch.DEFAULT_BATCH_SIZE, b.batchSize);
        
        b = new CfG_Batch('b', CfG_Batch.OP.OP_UPDATE, q, null, null, CfG_Batch.MIN_BATCH_SIZE - 1);
        System.assertEquals(CfG_Batch.MIN_BATCH_SIZE, b.batchSize);
        
        b = new CfG_Batch('b', CfG_Batch.OP.OP_UPDATE, q, null, null, CfG_Batch.MAX_BATCH_SIZE + 1);
        System.assertEquals(CfG_Batch.MAX_BATCH_SIZE, b.batchSize);
      t.stop();
    }
  }
  
  /*static testMethod void testDelete() {
    System.runAs(t.adminUser) {
      t.init();
      
      Account[] accounts = createAccounts('a');
      insert accounts;
      
      t.start();
        System.assertEquals(accounts.size(), [SELECT Count() FROM Account WHERE Id IN :accounts]);
        Database.executeBatch(new CfG_Batch('Test', CfG_Batch.OP.OP_DELETE,
          'SELECT Id FROM Account WHERE Id IN (\'' + idsToCsv(accounts) + '\')', null, null, null));
      t.stop();
      
      System.assertEquals(0, [SELECT Count() FROM Account WHERE Id IN :accounts]);
    }
  }*/
  
  static testMethod void testUndelete() {
    System.runAs(t.adminUser) {
      t.init();
      
      Account[] accounts = createAccounts('a');
      insert accounts;
      
      t.start();
        System.assertEquals(accounts.size(), [SELECT Count() FROM Account WHERE Id IN :accounts]);
        delete accounts;
        System.assertEquals(0, [SELECT Count() FROM Account WHERE Id IN :accounts]);
        
        Database.executeBatch(new CfG_Batch('Test', CfG_Batch.OP.OP_UNDELETE,
          'SELECT Id FROM Account WHERE Id IN (\'' + idsToCsv(accounts) + '\')', null, null, null));
      t.stop();
      
      System.assertEquals(accounts.size(), [SELECT Count() FROM Account WHERE Id IN :accounts]);
    }
  }
  
  static testMethod void testUpdateWithHandler() {
    System.runAs(t.adminUser) {
      t.init();
      
      Account[] accounts = createAccounts('~CfG');
      insert accounts;
      
      t.start();
        Database.executeBatch(new CfG_Batch('Test', CfG_Batch.OP.OP_UPDATE, 'SELECT Name FROM Account WHERE Name LIKE \'~CfG%\'',
          new AccountNameUpdater(), null, null));
      t.stop();
      
      System.assertEquals(accounts.size(), [SELECT Count() FROM Account WHERE Id IN :accounts AND Name LIKE 'TEST~CfG%']);
    }
  }
  
  static testMethod void testSchedule() {
    t.init();
    
    final String SCHEDULE = '0 0 0 3 9 ? 2022';
    
    Account[] accounts = createAccounts('~CfG');
    insert accounts;
    
    CfG_Batch b = new CfG_Batch('Test', CfG_Batch.OP.OP_UPDATE, 'SELECT Name FROM Account WHERE Name LIKE \'~CfG%\'', new AccountNameUpdater(), null, null);
    t.start();
      final String jobId = System.schedule('test', SCHEDULE, b);
      
      CronTrigger ct = [SELECT CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
      System.assertEquals(SCHEDULE, ct.CronExpression);
      System.assertEquals(0, ct.TimesTriggered);
      System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
      System.assertEquals(0, [SELECT Count() FROM Account WHERE Id IN :accounts AND Name LIKE 'TEST~CfG%']);
    t.stop();
    
    //TODO why doesn't this work?!
    //System.assert([SELECT Name FROM Account WHERE Id = :account.Id].Name.endsWith('TEST'));
    b.execute(null);
  }
  
  static testMethod void scheduleJob() {
    System.runAs(t.adminUser) {
      t.init();
      
      delete [SELECT Id FROM CfG_Job__c];
      
      t.start();
        try {
          CfG_Batch.scheduleJob(null);
          System.assert(false, 'missing job');
        } catch (CfG.AssertionException ex) {
        }
        
        // invalid handler
        CfG_Job__c j = new CfG_Job__c(Handler__c = 'junk', Active__c = true);
        CfG_Batch.scheduleJob(j);
        System.assertEquals(null, j.Handler__c);
        System.assertEquals(false, j.Active__c);
        System.assertEquals(0, [SELECT Count() FROM CronTrigger WHERE Id = :j.Job_Id__c]);
        
        // job isn't active
        System.assertEquals(null, CfG_Batch.scheduleJob(new CfG_Job__c(
          Active__c = false)).Job_Id__c);
        
        // job already has an Id
        System.assertEquals('junk', CfG_Batch.scheduleJob(new CfG_Job__c(
          Active__c = true, Job_Id__c = 'junk')).Job_Id__c);
        
        // invalid operation
        System.assertEquals(null, CfG_Batch.scheduleJob(new CfG_Job__c(
          Name='n', Schedule__c='s', Operation__c='o', Query__c='q', Active__c=true)).Job_id__c);
        
        // invalid schedule
        System.assertEquals(null, CfG_Batch.scheduleJob(new CfG_Job__c(
          Name='n2', Schedule__c='s', Operation__c='UpDaTe', Query__c=Q_CASES, Active__c=true)).Job_id__c);
        
        // test CfG_Job__c required fields
        try {
          insert new CfG_Job__c(Name=null, Schedule__c='s', Operation__c='o', Query__c='q');
          System.assert(false, 'Name is required');
        } catch (DmlException e) {
        }
        
        try {
          insert new CfG_Job__c(Name='n', Schedule__c=null, Operation__c='o', Query__c='q');
          System.assert(false, 'Schedule is required');
        } catch (DmlException e) {
        }
        
        try {
          insert new CfG_Job__c(Name='n', Schedule__c='s', Operation__c=null, Query__c='q');
          System.assert(false, 'Operation is required');
        } catch (DmlException e) {
        }
        
        try {
          insert new CfG_Job__c(Name='n', Schedule__c='s', Operation__c='o', Query__c=null);
          System.assert(false, 'Query is required');
        } catch (DmlException e) {
        }
        
        j = new CfG_Job__c(Name='n2', Schedule__c=SCHED, Operation__c='UpDaTe', Query__c=Q_CASES, Active__c=true);
        CfG_Batch.scheduleJob(j);
        System.assert(!CfG.isEmpty(j.Job_id__c));
        System.assertEquals(1, [SELECT Count() FROM CronTrigger WHERE Id = :j.Job_Id__c]);
        final Id jobId = j.Job_Id__c;
        
        // duplicate job fails
        final Integer nJobs = [SELECT Count() FROM CronTrigger];
        j.Job_id__c = null;
        CfG_Batch.scheduleJob(j);
        System.assert(CfG.isEmpty(j.Job_id__c));
        System.assertEquals(nJobs, [SELECT Count() FROM CronTrigger]);
        
        CfG_Batch.abortJob(jobId);
      t.stop();
    }
  }
  
  static testMethod void testAbortJob() {
    System.runAs(t.adminUser) {
      t.init();
      
      delete [SELECT Id FROM CfG_Job__c];
      
      t.start();
        // invalid Id
        CfG_Batch.abortJob(null);
        CfG_Batch.abortJob([SELECT Id FROM User LIMIT 1].Id);
        
        System.assertEquals(0, [SELECT Count() FROM CfG_Job__c]);
        CfG_Job__c j = new CfG_Job__c(Name='n2', Schedule__c=SCHED,
          Operation__c='UpDaTe', Query__c=Q_CASES, Active__c=true);
        
        j = CfG_Batch.scheduleJob(j);
        System.assertEquals(1, [SELECT Count() FROM CronTrigger WHERE Id = :j.Job_Id__c]);
        CfG_Batch.abortJob(j.Job_Id__c);
        System.assertEquals(0, [SELECT Count() FROM CronTrigger WHERE Id = :j.Job_Id__c]);
      t.stop();
    }
  }
  
  static testMethod void testAbortAllJobs() {
    System.runAs(t.adminUser) {
      t.init();
      
      delete [SELECT Id FROM CfG_Job__c];
      
      t.start();
        // no jobs
        CfG_Batch.abortAllJobs();
        CfG_Job__c[] jobs = [SELECT Job_Id__c FROM CfG_Job__c];
        System.assertEquals(0, jobs.size());
        System.assertEquals(0, [SELECT Count() FROM CronTrigger WHERE Id IN :CfG.getFieldValues(jobs, CfG_Job__c.Job_Id__c)]);
        
        CfG_Job__c j = new CfG_Job__c(Name='n2', Schedule__c=SCHED, Operation__c='UpDaTe', Query__c=Q_CASES, Active__c=true);
        insert j;
        
        j = CfG_Batch.scheduleJob(j);
        update j;
        
        System.assertEquals(1, [SELECT Count() FROM CronTrigger WHERE Id = :j.Job_Id__c]);
        CfG_Batch.abortAllJobs();
        System.assertEquals(0, [SELECT Count() FROM CronTrigger WHERE Id = :j.Job_Id__c]);
        System.assertEquals(null, [SELECT Job_Id__c FROM CfG_Job__c WHERE Name = :j.Name].Job_Id__c);
      t.stop();
    }
  }
  
  static testMethod void testMonitor() {
    System.runAs(t.adminUser) {
      t.init();
      
      delete [SELECT Id FROM CfG_Job__c];
      
      t.start();
        // no jobs
        new CfG_Batch.Monitor().execute(null);
        
        // no active jobs
        CfG_Job__c j = new CfG_Job__c(Name='n2', Schedule__c=SCHED, Operation__c='UpDaTe', Query__c=Q_CASES, Active__c=false);
        insert j;
        new CfG_Batch.Monitor().execute(null);
        System.assertEquals(0, [SELECT Count() FROM CronTrigger WHERE Id = :j.Job_Id__c]);
        
        // an active job will get scheduled
        update new CfG_Job__c(Id = j.Id, Active__c = true);
        new CfG_Batch.Monitor().execute(null);
        j = [SELECT Job_Id__c FROM CfG_Job__c WHERE Id = :j.Id];
        System.assertEquals(1, [SELECT Count() FROM CronTrigger WHERE Id = :j.Job_Id__c]);
        
        // an inactive job will get aborted
        update new CfG_Job__c(Id = j.Id, Active__c = false);
        new CfG_Batch.Monitor().execute(null);
        System.assertEquals(0, [SELECT Count() FROM CronTrigger WHERE Id = :j.Job_Id__c]);
        System.assertEquals(null, [SELECT Job_Id__c FROM CfG_Job__c WHERE Id = :j.Id].Job_Id__c);
        
        // an active job will get scheduled - even if is has an invalid job Id
        update new CfG_Job__c(Id = j.Id, Active__c = true, Job_Id__c = 'junk');
        new CfG_Batch.Monitor().execute(null);
        j = [SELECT Job_Id__c FROM CfG_Job__c WHERE Id = :j.Id];
        System.assertEquals(1, [SELECT Count() FROM CronTrigger WHERE Id = :j.Job_Id__c]);
        
        // an active job will get aborted and rescheduled when the schedule is changed
        final String newSchedule = '1 0 0 ? * *';
        update new CfG_Job__c(Id = j.Id, Schedule__c = newSchedule);
        new CfG_Batch.Monitor().execute(null);
        System.assertEquals(0, [SELECT Count() FROM CronTrigger WHERE Id = :j.Job_Id__c]);
        j = [SELECT Job_Id__c, Schedule__c FROM CfG_Job__c WHERE Id = :j.Id];
        System.assertEquals(newSchedule, j.Schedule__c);
        System.assertEquals(1, [SELECT Count() FROM CronTrigger WHERE Id = :j.Job_Id__c]);
        
        CfG_Batch.abortAllJobs();
      t.stop();
    }
  }
  
  static testMethod void testLogEmailer() {
    System.runAs(t.adminUser) {
      t.init();
      
      CfG_Batch.LogEmailer l = new CfG_Batch.LogEmailer();
      
      CfG_Log__c[] logs = t.newLogs('l');
      insert logs;
      
      t.start();
        try {
          l.handle(null);
          System.assert(false, 'scope is required');
        } catch (CfG.AssertionException ex) {
        }
        
        try {
          l.handle(new CfG_Log__c[]{});
          System.assert(false, 'scope is required');
        } catch (CfG.AssertionException ex) {
        }
        
        logs = [SELECT Name, Text__c, CreatedBy.Name, CreatedDate FROM CfG_Log__c LIMIT 200];
        if (!CfG.isEmpty(logs)) {
          System.assert(CfG.isEmpty(l.handle(logs)));
        }
      t.stop();
    }
  }
  
  
  //--------------------------------------------------------------------------
  // Helpers
  class AccountNameUpdater implements CfG_Batch.Handler {
    public SObject[] handle(SObject[] scope) {
      for (Account a : (Account[])scope) {
        a.Name = 'TEST' + a.Name;
      }
      return scope;
    }
  }
  
  static String idsToCsv(SObject[] o) {
    return String.join(new List<String>(CfG.getIds(o)), '\',\'');
  }
  
  private static Account[] createAccounts(String Name) {
    RecordType[] rts = [SELECT Id FROM RecordType WHERE DeveloperName = 'Agency_Location' AND isActive = true AND SObjectType = 'Account' LIMIT 1];
    System.assert(!rts.isEmpty(), 'Should exist recordType for Account with dev name: Agency_Location');
    
    Account[] accounts = t.newAccounts(Name);
    for (Account account : accounts) {
      account.RecordTypeId = rts[0].Id;
    }
    
    return accounts;
  }
  
  static { CfG_BaseTest.t = new CfG_BatchTest(); }
  static CfG_BaseTest t { get { return CfG_BaseTest.t; } }
}