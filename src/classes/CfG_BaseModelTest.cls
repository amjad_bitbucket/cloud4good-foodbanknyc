@IsTest(SeeAllData=true)
class CfG_BaseModelTest extends CfG_BaseTest {
  /* REDUCE_DEPENDENCIES replace CfG_Test__c with a type available in your org
  //--------------------------------------------------------------------------
  // Constants
  static final String INVALID_NAME = 'invalid';
  
  
  //--------------------------------------------------------------------------
  // Tests
  static testMethod void testGetRecordTypeId() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        try {
          new TestModel(null).getRecordTypeId(null);
          System.assert(false, 'name is required');
        } catch (CfG.AssertionException e) {
        }
        
        try {
          new TestModel(null).getRecordTypeId(CfG_Rt.Name.Junk);
          System.assert(false, 'record type not found');
        } catch (CfG.AssertionException e) {
        }
        
        System.assertNotEquals(null, new TestModel(null).getRecordTypeId(CfG_Rt.Name.Test1));
      t.stop();
    }
  }
  
    static testMethod void testAddFields() {
    System.runAs(t.adminUser) {
      t.init();
      CfG_Test__c a = new CfG_Test__c(Name = 'test', Option__c = CfG_TestModel.Option.A.name());
      insert a;
      
      t.start();
        TestModel m = new TestModel(a.Id);
        try {
          System.assertEquals(a.Name, m.rec.Name);
          System.assert(false, 'query fields not set up');
        } catch (SObjectException e) {
        }
        
        m.addFields(new SObjectField[] { CfG_Test__c.Name });
        System.assertEquals(a.Name, m.rec.Name);
        
        try {
          System.assertNotEquals(null, m.rec.CreatedDate);
          System.assert(false, 'query fields not set up');
        } catch (SObjectException e) {
        }
        
        m.addFields(SObjectType.CfG_Test__c.fieldSets.TestObjTable);
        System.assertNotEquals(null, m.rec.CreatedDate);
      t.stop();
    }
  }
  
  static testMethod void testModelSave() {
    System.runAs(t.adminUser) {
      t.init();
      CfG_Test__c a = new CfG_Test__c(Name = 'test', Option__c = CfG_TestModel.Option.A.name());
      insert a;
      
      t.start();
        TestModel m = new TestModel(a.Id);
        m.addFields(new SObjectField[] { CfG_Test__c.Name });
        System.assertEquals(a.Name, m.rec.Name);
        
        m.rec.Name = INVALID_NAME;
        System.assertEquals(false, m.validate());
        m.save();
        System.assertNotEquals(m.rec.Name, [SELECT Name FROM CfG_Test__c WHERE Id = :a.Id].Name);
        
        m.rec.Name = 'valid';
        m.save();
        System.assertEquals(m.rec.Name, [SELECT Name FROM CfG_Test__c WHERE Id = :a.Id].Name);
      t.stop();
    }
  }
  
  static testMethod void testCrudAccess() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        TestModel m = new TestModel(null);
        System.assertEquals(true, m.canView());
        System.assertEquals(true, m.canInsert());
        System.assertEquals(true, m.canUpdate());
        System.assertEquals(true, m.canDelete());
      t.stop();
    }
  }
  
  
  //--------------------------------------------------------------------------
  // Helpers
  class TestModel extends CfG_BaseModel {
    public CfG_Test__c rec { get { return (CfG_Test__c)sObj; } }
    
    public TestModel(Id theId) {
      super(CfG_Test__c.SObjectType, theId);
    }
    
    public override Boolean validate(SObject obj) {
      return (INVALID_NAME != ((CfG_Test__c)obj).Name);
    }
  }
  
  class TestModelThrowsOnSave extends CfG_BaseModel {
    public TestModelThrowsOnSave() {
      super(CfG_Test__c.SObjectType, null);
    }
    
    public override Boolean validate(SObject obj) {
      return (INVALID_NAME != ((CfG_Test__c)obj).Name);
    }
    
    public override void save() {
      throw new CfG.ValidationException();
    }
  }
  
  static { CfG_BaseTest.t = new CfG_BaseModelTest(); }
  static CfG_BaseTest t { get { return CfG_BaseTest.t; } }
  */
}