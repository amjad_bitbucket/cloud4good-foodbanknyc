/**
 * Used to instantiate and execute Trigger Handlers associated with SObjects.
 * <p>
 * (from <a href="http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers">
 * Tony Scott's article</a>)</p>
 * <p>
 * A few additions have been made to Tony's pattern:
 * <ul>
 *   <li>the object type is used to determine the handler name and custom settings.</li>
 *   <li>a "handled" flag is tracked for each trigger so that handlers know if a
 *     trigger is firing multiple times during a single execution context.</li>
 *   <li>custom settings for disabling all or individual triggers are supported</li>
 * </ul>
 *
 * @author Steve Cox
 */
public class CfG_TriggerFactory {
  //--------------------------------------------------------------------------
  // Properties
  // keep track of which handlers have been called within this execution context
  private static Map<String,Boolean> handled = new Map<String,Boolean>();
  
  
  //--------------------------------------------------------------------------
  // Methods
  /**
   * handle the current trigger execution with the handler for the specified
   * object; also detect and notify the handler if this is a reentrant call.
   * @param o the object type; the handler must be named
   * <code>&lt;o&gt;Handler</code> and implement CfG_ITrigger
   */
  public static void handle(SObjectType o) {
    // determine if this trigger is enabled in custom settings
    if (CfG.isTriggerActive(o)) {
      final String triggerDesc = (Trigger.isBefore? 'Before' : 'After') +
        (Trigger.isInsert? 'Insert' : Trigger.isUpdate? 'Update' : Trigger.isDelete? 'Delete' : 'Undelete');
      CfG.debugBegin('Trigger: ' + triggerDesc + ' -> ' + o);
      
      // determine if this is the first call to this trigger in this execution context
      CfG_ITrigger t = getHandler(o);
      if (true == handled.get(o + triggerDesc)) {
        CfG.warn('Reentrant trigger call: ' + o);
        t.setHandled();
      } else {
        handled.put(o + triggerDesc, true);
      }
      
      execute(t);
    }
  }
  
  
  //--------------------------------------------------------------------------
  // Helpers
  /**
   * call various methods on the supplied handler based on the current trigger type.
   * @param handler the handler; may not be null
   */
  @TestVisible private static void execute(CfG_ITrigger handler) {
    try {
      if (Trigger.isBefore) {
        handler.BulkBefore();
        if (Trigger.isDelete) {
          for (SObject so : Trigger.old) {
            handler.beforeDelete(so);
          }
        } else if (Trigger.isInsert) {
          for (SObject so : Trigger.new) {
            handler.beforeInsert(so);
          }
        } else if (Trigger.isUpdate) {
          for (SObject so : Trigger.old) {
            handler.beforeUpdate(so, Trigger.newMap.get(so.Id));
          }
        }
      } else {
        handler.bulkAfter();
        if (Trigger.isDelete) {
          for (SObject so : Trigger.old) {
            handler.afterDelete(so);
          }
        } else if (Trigger.isInsert) {
          for (SObject so : Trigger.new) {
            handler.afterInsert(so);
          }
        } else if (Trigger.isUpdate) {
          for (SObject so : Trigger.old) {
            handler.afterUpdate(so, Trigger.newMap.get(so.Id));
          }
        } else if (Trigger.isUndelete) {
          for (SObject so : Trigger.new) {
            handler.afterUndelete(so);
          }
        }
      }
      
      handler.andFinally();
      CfG_BaseTest.throwIfTesting(new NullPointerException());
    } catch (Exception ex) {
      CfG.debugException(ex);
      throw ex;
    } finally {
      CfG.debugEnd();
    }
  }
  
  /**
   * get the handler for the specified SObject
   * @param o the object type
   */
  private static CfG_ITrigger getHandler(SObjectType o) {
    CfG.preCondition(null != o, 'CfG_TriggerFactory.getHandler() - missing object type');
    
    try {
      CfG_BaseTest.throwIfTesting(new NullPointerException());
      return (CfG_ITrigger)Type.forName(String.valueOf(o).removeEnd('__c').
        remove(CfG.PKG).replace('__', '_') + 'Handler').newInstance();
    } catch (NullPointerException ex) {
      return null;
    }
  }
}