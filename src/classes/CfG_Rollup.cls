/**
 * Utility for calculating rollup values from children to the parent.
 * <p>
 * (based on <a href="http://www.anthonyvictorio.com/salesforce/roll-up-summary-utility">
 * Anthony Victorio's article</a>)
 *
 * @author Steve Cox
 */
public with sharing class CfG_Rollup {
  //--------------------------------------------------------------------------
  // Constants
  /** operations that can be used for rollup fields */
  public enum Op { COUNT, COUNT_DISTINCT, SUM, MIN, MAX, AVG }
  
  
  //--------------------------------------------------------------------------
  // Methods
  /**
   * calculate a set of rollup values for the specified records and fields. An
   * additional query filter can also be specified.
   * <p>
   * Usage:
   * <blockquote>
   * CfG_Rollup.FieldDef[] fieldDefs = new CfG_Rollup.FieldDef[] {<br/>
   *   new CfG_Rollup.FieldDef(CfG_Rollup.Op.SUM, Asset.Price, Account.AssetCost));<br/>
   * };<br/>
   * <br/>
   * update CfG_Rollup.fields(fieldDefs, Trigger.new, <br/>
   *   Asset.SObjectType, Asset.AccountId, Account.SObjectType, '');<br/>
   * </blockquote>
   */
  public static SObject[] fields(FieldDef[] fieldDefs, SObject[] records, SObjectType childObject,
    SObjectField lookupField, SObjectType parentObject, String filter, SObject[] parentUpdates) {
    CfG.preCondition(null != lookupField, 'CfG_Rollup.fields() - missing lookupField');
    return fields(fieldDefs, records, childObject, lookupField.getDescribe().getName(), parentObject, filter, parentUpdates);
  }
  
  public static SObject[] fields(FieldDef[] fieldDefs, SObject[] records, SObjectType childObject,
    String lookupField, SObjectType parentObject, String filter, SObject[] parentUpdates) {
    Set<String> parentIds = CfG.getFieldValues(records, lookupField);
    return fields(fieldDefs, parentIds, childObject, lookupField, parentObject, filter, parentUpdates);
  }
  
  public static SObject[] fields(Spec spec, SObject[] records, SObject[] parentUpdates) {
    return fields(spec.fields, records, spec.child, spec.lookup, spec.parent, spec.filter, parentUpdates);
  }
  
  public static SObject[] fields(Spec spec, Set<String> parentIds, SObject[] parentUpdates) {
    return fields(spec.fields, parentIds, spec.child, spec.lookup, spec.parent, spec.filter, parentUpdates);
  }
  
  public static SObject[] fields(FieldDef[] fieldDefs, Set<String> parentIds, SObjectType childObject,
    String lookupField, SObjectType parentObject, String filter, SObject[] parentUpdates) {
    CfG.preCondition(null != fieldDefs, 'CfG_Rollup.fields() - missing fieldDefs');
    CfG.preCondition(null != parentIds, 'CfG_Rollup.fields() - missing parentIds');
    CfG.preCondition(null != childObject, 'CfG_Rollup.fields() - missing childObject');
    CfG.preCondition(!CfG.isEmpty(lookupField), 'CfG_Rollup.fields() - missing lookupField');
    CfG.preCondition(null != parentObject, 'CfG_Rollup.fields() - missing parentObject');
    
    if (null == parentUpdates) {
      parentUpdates = new SObject[]{};
    }
    
    String[] fieldsToAggregate = new String[] {};
    String[] parentFields = new String[] {};
    for (FieldDef d : fieldDefs) {
      fieldsToAggregate.add(d.op.name() + '(' + d.child + ')');
      parentFields.add(d.parent);
    }
    
    if (!parentIds.isEmpty() && !fieldsToAggregate.isEmpty() && !parentFields.isempty()) {
      // get aggregate object for each parent
      Map<Id, AggregateResult> parentValueMap = new Map<Id, AggregateResult>();
      String query = 'SELECT ' + String.join(fieldsToAggregate, ',') + ',' +
        lookupField + ' FROM ' + childObject.getDescribe().getName() +
        ' WHERE ' + lookupField + ' IN :parentIds ' +
        (!CfG.isEmpty(filter) ? 'AND ' + filter : '') +
        ' GROUP BY ' + lookupField;
      try {
        for (AggregateResult r : Database.query(query)) {
          parentValueMap.put((Id)r.get(lookupField), r);
        }
      } catch (Exception e) {
        //CfG.debug('query: ' + query);
        //CfG.debug('parentIds: ' + parentIds);
        throw e;
      }
       
      // for each affected parent object, retrieve aggregate results and
      // for each field definition add aggregate value to parent field
      query = 'SELECT ' + String.join(parentFields, ',') +
       ' FROM ' + parentObject.getDescribe().getName() + ' WHERE Id IN :parentIds';
      for (SObject o : Database.query(query)) {
        // find the matching parent record, if any
        SObject parent;
        Boolean added = false;
        for (SObject x : parentUpdates) {
          if (x.Id == o.Id) {
            parent = x;
            added = true;
            break;
          }
        }
        
        if (null == parent) {
          parent = o;
        }
        
        for (Integer i = 0; i < fieldDefs.size(); ++i) {
          final AggregateResult r = parentValueMap.get(o.Id);
          final Object v = (null != r) ? r.get('expr' + i) : null;
          if (parent.get(fieldDefs[i].parent) != ((null == v) ? null : v)) {
            parent.put(fieldDefs[i].parent, (null == v) ? null : v);
            if (!added) {
              parentUpdates.add(parent);
              added = true;
            }
          }
        }
      }
    }
     
    return parentUpdates;
  }
    
  //--------------------------------------------------------------------------
  // Helpers
  // a summary field definition
  public class FieldDef {
    Op op;
    String child, parent;
     
    public FieldDef(Op op, SObjectField child, SObjectField parent) {
      CfG.preCondition(null != op, 'CfG_Rollup.FieldDef() - missing op');
      CfG.preCondition(null != child, 'CfG_Rollup.FieldDef() - missing child');
      CfG.preCondition(null != parent, 'CfG_Rollup.FieldDef() - missing parent');
      
      this.op = op;
      this.child = child.getDescribe().getName();
      this.parent = parent.getDescribe().getName();
    }
  }
  
  // a helper class for easily specifying multiple field definitions
  public class Spec {
    public FieldDef[] fields = new FieldDef[] {};
    public SObjectType child, parent;
    public String lookup;
    public String filter;
    
    public Spec(FieldDef[] fields, SObjectType child, SObjectField lookup, SObjectType parent, String filter) {
      this(fields, child, lookup.getDescribe().getName(), parent, filter);
    }
    
    public Spec(FieldDef[] fields, SObjectType child, String lookup, SObjectType parent, String filter) {
      CfG.preCondition(null != fields, 'CfG_Rollup.Spec() - missing fields');
      CfG.preCondition(null != child, 'CfG_Rollup.Spec() - missing child');
      CfG.preCondition(!CfG.isEmpty(lookup), 'CfG_Rollup.Spec() - missing lookup');
      CfG.preCondition(null != parent, 'CfG_Rollup.Spec() - missing parent');
      
      this.fields = fields;
      this.child = child;
      this.parent = parent;
      this.lookup = lookup;
      this.filter = filter;
    }
  }
}