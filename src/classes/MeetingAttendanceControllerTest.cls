@IsTest(SeeAllData=true)
class MeetingAttendanceControllerTest {
  //--------------------------------------------------------------------------
  // Tests

  static testMethod void testAggregateQueryException() {
     CookShop_Program__c program = createProgram();
     Facility__c facility = createFacility();
     
     // create meeting
     Meeting__c meeting = createMeeting(program, facility);

     // create contact
     Contact contact = createContact(null);

     // create Meeting Attendee
     Meeting_Attendee__c[] maList = new Meeting_Attendee__c[] {};
     for (Integer i=0; i<250; i++) {
       Meeting_Attendee__c meet = new Meeting_Attendee__c(
         Meeting_Name__c = meeting.Id,
         Contact__c = contact.Id,
         Registration_Status__c = 'Registered',
         Attended__c = true
       );

       maList.add(meet);
     }

     insert maList;

     // create controller
     MeetingAttendanceController controller = initController(meeting);
     
     Test.StartTest();
       System.assertEquals(1, controller.getlistAttendees().size(), 'Should exist only one attendees');
       controller.getlistAttendees()[0].meeting.Check_in__c = true;
       controller.getlistAttendees()[0].meeting.Check_out__c = true;
       controller.getlistAttendees()[0].Save();
     Test.StopTest();
      
     Meeting_Attendee__c testMeet = [SELECT Id, Check_In__c, Check_out__c FROM Meeting_Attendee__c 
                                      WHERE Id = :controller.getlistAttendees()[0].meeting.Id];
     System.assertEquals(true, testMeet.Check_In__c);
     System.assertEquals(true, testMeet.Check_out__c );
  }

  static testMethod void testConstructor() {
     CookShop_Program__c program = createProgram();
     Facility__c facility = createFacility();
     
     // create meeting
     Meeting__c meeting = createMeeting(program, facility);
     
     // create organization
     // Account organization = createOrganization();
     
     // create contact
     Contact contact = createContact(null);
     
     /*// create enrollment
     Enrollment__c enrollment = createEnrollment(program, contact);*/
     
     // create Meeting Attendee
     Meeting_Attendee__c meet = createMeetingAttendee(meeting, contact);
     
     MeetingAttendanceController controller;
     
     Test.StartTest();
         // create controller
         controller = initController(meeting);
     Test.StopTest();
     
     System.assertEquals(facility.Id, controller.getMeeting().Facility__c);
     System.assertEquals(program.Id, controller.getMeeting().CookShop_Program__c);
     
     System.assertEquals(1, controller.getlistAttendees().size(), 'Should exist only one attendees');
     
     MeetingAttendanceController.SelecTableAtendee attendee = controller.getlistAttendees()[0];
     System.assertEquals(contact.Id, attendee.contact.Id);
     System.assertEquals(meet.Id, attendee.meeting.Id);
  }

  static testMethod void testSaveTableSelectedElement() {
      CookShop_Program__c program = createProgram();
      Facility__c facility = createFacility();
     
      // create meeting
      Meeting__c meeting = createMeeting(program, facility);
     
      // create organization
      // Account organization = createOrganization();
     
      // create contact
      Contact contact = createContact(null);
     
     /* // create enrollment
      Enrollment__c enrollment = createEnrollment(program, contact);
      
      Enrollment__c enrollment2 = createEnrollment(program, contact);
      enrollment2.Dropped_Out__c = true;
      update enrollment2;*/
     
      // create Meeting Attendee
      Meeting_Attendee__c meet = createMeetingAttendee(meeting, contact);
     
      // create controller
      MeetingAttendanceController controller = initController(meeting);
      
      Test.StartTest();
          System.assertEquals(1, controller.getlistAttendees().size(), 'Should exist only one attendees');
          controller.getlistAttendees()[0].meeting.Check_in__c = true;
          controller.getlistAttendees()[0].meeting.Check_out__c = true;
          controller.getlistAttendees()[0].Save();
      Test.StopTest();
      
      Meeting_Attendee__c testMeet = [SELECT Id, Check_In__c, Check_out__c FROM Meeting_Attendee__c 
                                      WHERE Id = :controller.getlistAttendees()[0].meeting.Id];
      System.assertEquals(true, testMeet.Check_In__c);
      System.assertEquals(true, testMeet.Check_out__c );
  }  

  /** test register popup */
  static testMethod void testShowPopup() {
      CookShop_Program__c program = createProgram();
      Facility__c facility = createFacility();
     
      // create meeting
      Meeting__c meeting = createMeeting(program, facility);
      
      // create controller
      MeetingAttendanceController controller = initController(meeting);
      
      Test.StartTest();
          controller.RegisterNewAttendee();
          System.assertEquals(true, controller.viewPopUpWindow);
          
          controller.CloseRegisterPopup();
          System.assertEquals(false, controller.viewPopUpWindow);
      Test.StopTest();
  }
  
  static testMethod void testPrefillContactInfo() {
      CookShop_Program__c program = createProgram();
      Facility__c facility = createFacility();
     
      // create meeting
      Meeting__c meeting = createMeeting(program, facility);
      
      // create controller
      MeetingAttendanceController controller = initController(meeting);
      controller.RegisterNewAttendee();
      
      // create contact
      Contact contact = createContact(null);
      
      controller.meetingAttendee.Contact__c = contact.Id;
      
      Test.StartTest();
          controller.PreFillContactInfo();
      Test.StopTest();
      
      System.assertEquals(contact.FirstName, controller.newRegisterContact.FirstName);
      System.assertEquals(contact.LastName, controller.newRegisterContact.LastName);
      System.assertEquals(contact.Email, controller.newRegisterContact.Email);
      System.assertEquals(contact.Position__c, controller.newRegisterContact.Position__c);
      System.assertEquals(contact.AccountId, controller.newRegisterContact.AccountId);
      System.assertEquals(SchoolStaffRecordType.Id, controller.newRegisterContact.RecordTypeId);
  }
  
  static testMethod void testConfirm_GetExistContact(){
      CookShop_Program__c program = createProgram();
      Facility__c facility = createFacility();
     
      // create meeting
      Meeting__c meeting = createMeeting(program, facility);
      
      // create controller
      MeetingAttendanceController controller = initController(meeting);
      controller.RegisterNewAttendee();
      
      // create contact
      Contact contact = createContact(null);
      
      // manual prefill contact info field
      controller.newRegisterContact.FirstName = contact.FirstName;
      controller.newRegisterContact.LastName= contact.LastName;
      controller.newRegisterContact.Email= contact.Email;
      controller.newRegisterContact.Position__c= contact.Position__c;
      controller.newRegisterContact.AccountId = contact.AccountId;
      
      Test.StartTest();
          controller.Confirm();
      Test.StopTest();
      
      Meeting_Attendee__c[] meetings = [SELECT Id FROM Meeting_Attendee__c WHERE Contact__c = :contact.Id AND Meeting_Name__c = :meeting.Id];
      
      System.assertEquals(1, meetings.size(), 'Should create only one meeting attendee');
      System.assertEquals(1, controller.getListAttendees().size(), 'Should exist only one attendee');
  }
  
  static testMethod void testFilter() {
      CookShop_Program__c program = createProgram();
      Facility__c facility = createFacility();
     
      // create meeting
      Meeting__c meeting = createMeeting(program, facility);
      
      // create contact
      Contact contact = createContact(null);
      
      // create controller
      MeetingAttendanceController controller = initController(meeting);
      
      // create list of  attendees
      MeetingAttendanceController.SelecTableAtendee[] attendees = new MeetingAttendanceController.SelecTableAtendee[] {};
      
      // create Meeting Attendee
      Meeting_Attendee__c meet1 = createMeetingAttendee(meeting, contact);
      meet1.Check_In__c = true;
      MeetingAttendanceController.SelecTableAtendee attendee1 = new MeetingAttendanceController.SelecTableAtendee(contact, meet1.Id, program.Id);
      attendee1.meeting = meet1;
      attendees.add(attendee1);
      
      Meeting_Attendee__c meet2 = createMeetingAttendee(meeting, contact);
      meet2.Check_In__c = false;
      MeetingAttendanceController.SelecTableAtendee attendee2 = new MeetingAttendanceController.SelecTableAtendee(contact, meet2.Id, program.Id);
      attendee2.meeting = meet2;
      attendees.add(attendee2);
      
      controller.setListContact(attendees);
      
      Test.StartTest();
          controller.filterForAttendees = 'Attendees not checked in';
          System.assertEquals(1, controller.viewListOfAttendees.size());
          System.assertEquals(meet2.Id, controller.viewListOfAttendees[0].meeting.Id);
          
          controller.filterForAttendees = 'Attendees checked in';
          System.assertEquals(1, controller.viewListOfAttendees.size());
          System.assertEquals(meet1.Id, controller.viewListOfAttendees[0].meeting.Id);
          
          controller.filterForAttendees = 'All Registered Attendees';
          System.assertEquals(2, controller.viewListOfAttendees.size());
      Test.StopTest();
  }
  
  static testMethod void testSortColumnByContactName() {
      CookShop_Program__c program = createProgram();
      Facility__c facility = createFacility();
     
      // create meeting
      Meeting__c meeting = createMeeting(program, facility);

      // create controller
      MeetingAttendanceController controller = initController(meeting);
      
      // create list of  attendees
      MeetingAttendanceController.SelecTableAtendee[] attendees = new MeetingAttendanceController.SelecTableAtendee[] {};
      
      // create contact
      Contact contact1 = createContact(null);
      contact1.FirstName = 'A';
      contact1.LastName = 'A';
      update contact1;
      contact1 = [SELECT Id, Name, FirstName, LastName FROM Contact WHERE Id = :contact1.Id];
      
      // create Meeting Attendee
      Meeting_Attendee__c meet1 = createMeetingAttendee(meeting, contact1);
      meet1.Check_In__c = true;
      MeetingAttendanceController.SelecTableAtendee attendee1 = new MeetingAttendanceController.SelecTableAtendee(contact1, meet1.Id, program.Id);
      attendee1.meeting = meet1;
      attendees.add(attendee1);
      
      // create contact
      Contact contact2 = createContact(null);
      contact2.FirstName = 'B';
      contact2.LastName = 'B';
      update contact2;
      contact2 = [SELECT Id, Name, FirstName, LastName FROM Contact WHERE Id = :contact2.Id];
      
      Meeting_Attendee__c meet2 = createMeetingAttendee(meeting, contact2);
      meet2.Check_In__c = false;
      MeetingAttendanceController.SelecTableAtendee attendee2 = new MeetingAttendanceController.SelecTableAtendee(contact2, meet2.Id, program.Id);
      attendee2.meeting = meet2;
      attendees.add(attendee2);
      
      controller.setListContact(attendees);
      
      controller.filterForAttendees = 'All Registered Attendees';
      
      Test.StartTest();
          controller.sortExpression = 'Name';
          controller.SortDirection = 'DESC';
          controller.SortData();
          
          System.assertEquals('B', controller.viewListOfAttendees[0].Contact.FirstName);
          System.assertEquals('A', controller.viewListOfAttendees[1].Contact.FirstName);
          
          controller.SortDirection = 'ASC';
          controller.SortData();
          
          System.assertEquals('A', controller.viewListOfAttendees[0].Contact.FirstName);
          System.assertEquals('B', controller.viewListOfAttendees[1].Contact.FirstName);
      Test.StopTest();
  }
  
  /*
  static testMethod void MeetingAttendanceController_Test() {
        
      CookShop_Program__c pro1 = new CookShop_Program__c(Name= 'Program1_Test');
      insert pro1;
        
      Meeting__c meeting1 = new Meeting__c(Name = 'Meeting1_Test', CookShop_Program__c = pro1.Id, Date_Time__c = System.now().addHours(-15));
      insert meeting1;
        
      Meeting__c meeting2 = new Meeting__c(Name = 'Meeting2_Test', CookShop_Program__c = pro1.Id, Date_Time__c = System.now().addHours(-15));
      insert meeting2;
        
      Contact con1 = new Contact(LastName = 'Test1', FirstName = 'Contact1_Test');
      insert con1;
        
      Contact con2 = new Contact(LastName = 'Test2', FirstName = 'Contact2_Test');
      insert con2;
        
      Contact con3 = new Contact(LastName = 'Test3', FirstName = 'Contact3_Test');
      insert con3;
        
      Contact con4 = new Contact(LastName = 'Test4', FirstName = 'Contact4_Test');
      insert con4;
      
      createMeetingAttendee(meeting1, con1);
      createMeetingAttendee(meeting1, con2);
                
      Enrollment__c enroll1 = new Enrollment__c( Contact__c = con1.Id, CookShop_Program__c = pro1.Id);
      insert enroll1;
        
      Enrollment__c enroll2 = new Enrollment__c( Contact__c = con2.Id, CookShop_Program__c = pro1.Id);
      insert enroll2;
        
      //Enrollment__c enroll4 = new Enrollment__c( Contact__c = con4.Id, CookShop_Program__c = pro1.Id);
      //insert enroll4;
                
      ApexPages.StandardController sc = new ApexPages.StandardController(meeting1);
      MeetingAttendanceController controller = new MeetingAttendanceController(sc);   
        
      PageReference pageRef;
      MeetingAttendanceController.SelecTableAtendee atendee;
        
      try{            
          atendee = controller.getListAttendees().get(0);
          atendee.selectedAttended = true;
          atendee.reason = '...';
          atendee.Save();          
         // pageRef = controller.SaveAttendance();
      }catch(Exception e){
          // Test the exception lines
      }
        
      try{            
          atendee = controller.getListAttendees().get(0);
          atendee.selectedAttended = false;
          atendee.reason = '';           
          atendee.Save(); 
          //pageRef = controller.SaveAttendance();
      }catch(Exception e){
          // Test the exception lines
      }
        
      try{            
          List<MeetingAttendanceController.SelecTableAtendee> attendees = controller.getListAttendees();
          attendees.get(0).selectedAttended = false;
          attendees.get(0).reason = '...';
          attendees.get(0).selectedDrop = true;
          controller.setListContact(attendees);
          for (MeetingAttendanceController.SelecTableAtendee attendee : attendees) {
              attendee.Save();
          }
          //pageRef = controller.SaveAttendance();
      }catch(Exception e){
          // Test the exception lines
      }
        
      // Contact 1
      System.assertEquals(2,controller.getListAttendees().size());
      atendee = controller.getListAttendees().get(0);
      atendee.selectedAttended = true;
      controller.getListAttendees().add(0, atendee);
        
      // Contact 2
      atendee = controller.getListAttendees().get(1);
      atendee.selectedDrop = true;
      atendee.reason = 'Test Reason';
      controller.getListAttendees().add(1, atendee);
      
      atendee.Save();  
      //pageRef = controller.SaveAttendance();
        
      sc = new ApexPages.StandardController(meeting2);
      controller = new MeetingAttendanceController(sc);   
        
      // Meeting 2    
      atendee = controller.getListAttendees().get(0);
      atendee.selectedAttended = true;
      controller.getListAttendees().add(0, atendee);
        
        
      //Test extra lines
      controller.setListContact(controller.getListAttendees());
      controller.getMeeting();
      atendee.Save();
     // pageRef = controller.SaveAttendance();  
        
      // Meeting 1
      Enrollment__c enroll3 = new Enrollment__c( Contact__c = con3.Id, CookShop_Program__c = pro1.Id);
      insert enroll3;
        
      sc = new ApexPages.StandardController(meeting1);
      controller = new MeetingAttendanceController(sc);   
                
      System.assertEquals(3,controller.getListAttendees().size());
      atendee = controller.getListAttendees().get(0);
      atendee.selectedAttended = true;
      controller.getListAttendees().add(0, atendee);
        
      atendee = controller.getListAttendees().get(1);
      atendee.selectedDrop = true;
      atendee.selectedAttended = true;
      controller.getListAttendees().add(1, atendee);
        
      //pageRef = controller.SaveAttendance();
        
      meeting1.Date_Time__c = System.now();
      update meeting1;
      sc = new ApexPages.StandardController(meeting1);
      controller = new MeetingAttendanceController(sc);   
        
        
      meeting1.Date_Time__c = System.now().addHours(-1);
      update meeting1;
        
      sc = new ApexPages.StandardController(meeting1);
      controller = new MeetingAttendanceController(sc);        
        
      atendee = controller.getListAttendees().get(0);
      atendee.selectedDrop = true;
      atendee.selectedAttended = true;
      atendee = controller.getListAttendees().get(1);
      atendee.selectedDrop = true;
      atendee.selectedAttended = true;
      atendee = controller.getListAttendees().get(2);
      atendee.selectedDrop = true;
      atendee.selectedAttended = true;
      //pageRef = controller.SaveAttendance();
        
      sc = new ApexPages.StandardController(meeting1);
      controller = new MeetingAttendanceController(sc);        
        
      atendee = controller.getListAttendees().get(0);
      atendee.selectedDrop = false;
      atendee.selectedAttended = true;
      atendee = controller.getListAttendees().get(1);
      atendee.selectedDrop = false;
      atendee.selectedAttended = true;
      atendee = controller.getListAttendees().get(2);
      atendee.selectedDrop = false;
      atendee.selectedAttended = true;
      //pageRef = controller.SaveAttendance();
                
      controller.getValidDateTime();
        
  }
  */
  //--------------------------------------------------------------------------
  // Helpers
  private static RecordType SchoolOrganizatioinRecordType {
      get {
          if (SchoolOrganizatioinRecordType == null) {
              RecordType[] rcs = [SELECT Id FROM RecordType WHERE DeveloperName = 'School' AND IsActive = true AND SobjectType = 'Account'];
              if (rcs.isEmpty()) {
                  System.assert(false, 'Record Type "School" is not exist for Account');
              } else {
                  SchoolOrganizatioinRecordType  = rcs[0];
              }
          }
          
          return SchoolOrganizatioinRecordType;
      }
      set;
  }
  
  private static RecordType SchoolStaffRecordType {
      get {
          if (SchoolStaffRecordType == null) {
              RecordType[] rcs = [SELECT Id FROM RecordType WHERE DeveloperName = 'Cookshop_Participant' AND IsActive = true AND SobjectType = 'Contact'];
              if (rcs.isEmpty()) {
                  System.assert(false, 'Record Type "Cookshop_Participant" is not exist for Contact');
              } else {
                  SchoolStaffRecordType = rcs[0];
              }
          }
          
          return SchoolStaffRecordType;
      }
      set;
  }
  
  // problem with create account
  private static Account createOrganization() {
      Account account = new Account(
          Name = 'Test School',
          School_Number__c = '123hpG6',
          Borough__c = 'Brooklyn',
          RecordTypeId = SchoolOrganizatioinRecordType.Id
      );
      
      insert account;
      return account;
  }
  
 /* private static Enrollment__c createEnrollment(CookShop_Program__c program, Contact contact) {
      Enrollment__c enrollment = new Enrollment__c(
          CookShop_Program__c = program.Id,
          Contact__c = contact.Id
      );
      
      insert enrollment;
      return enrollment; 
  }*/
  
  private static Contact createContact(Account organization) {
      Contact contact = new Contact(
          FirstName = 'Contact1_Test',
          LastName = 'test',
          //AccountId = organization.Id,
          RecordTypeId = SchoolStaffRecordType.Id,
          Position__c = 'Teacher',
          Email = 'test@gmail.com'
      );
      
      insert contact;
      return contact;
  }
  
  private static Meeting_Attendee__c createMeetingAttendee(Meeting__c meeting, Contact contact) {
      Meeting_Attendee__c meet = new Meeting_Attendee__c(
          Meeting_Name__c = meeting.Id,
          Contact__c = contact.Id,
          Registration_Status__c = 'Registered'
      );
      
      insert meet;
      return meet;
  }
  
  private static CookShop_Program__c createProgram() {
      CookShop_Program__c program = new CookShop_Program__c(
          Name = 'test program'
      );
      
      insert program;
      return program;
  }
  
  private static Facility__c createFacility() {
      Facility__c facility = new Facility__c(
          Name = 'test facility'
      );
      
      insert facility;
      return facility;
  }
  
  private static Meeting__c createMeeting(CookShop_Program__c program, Facility__c facility) {
      Meeting__c meeting = new Meeting__c(
          Name = 'test meeting',
          CookShop_Program__c = program.Id,
          Meeting_Date__c = Date.today(),
          Meeting_Time__c = '12:30 PM',
          Meeting_End_Time__c = '4:30 PM',
          Facility__c = facility.Id,
          Meeting_Capacity__c = 100,
          Date_Time__c = Datetime.now().addDays(-1)
      );
      
      insert meeting;
      return meeting;
  } 
  
  private static MeetingAttendanceController initController(Meeting__c meeting) {
      ApexPages.StandardController sc = new ApexPages.standardController(meeting);
      MeetingAttendanceController controller = new MeetingAttendanceController(sc);
      
      PageReference pageRef = Page.MeetingAttendance;
      pageRef.getParameters().put('id', String.valueOf(meeting.Id));
      Test.setCurrentPage(pageRef);
      
      return controller;
  }
}