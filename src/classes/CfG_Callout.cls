/**
 * Utility class for making HTTP callouts and parsing the responses
 *
 * @author Steve Cox
 */
public with sharing class CfG_Callout {
  //--------------------------------------------------------------------------
  // Constants
  // timeout values - in milliseconds
  /** minimum timeout value, in milliseconds */
  public static Integer TIMEOUT_MIN = 1;
  /** maximum timeout value, in milliseconds */
  public static Integer TIMEOUT_MAX = 120000;
  /** default timeout value, in milliseconds */
  public static Integer TIMEOUT_DEFAULT = 100;
  
  private static String AUTH_TITLE = 'Authorization';
  private static String AUTH_PREFIX = 'Basic ';
  
  private static String HEADER_TYPE = 'Content-Type';
  private static String HEADER_JSON = 'application/json;';
  
  
  //--------------------------------------------------------------------------
  // Methods
  /**
   * Do an HTTP request and return the response. Basic authentication is provided.
   * Any HTTP status code >= 300 will throw an HttpException.
   * @param username/password login credentials
   * @param url the endpoint
   * @param params map of key/values to be added to the url; may be null
   * @param jsonBody JSON-encoded body; may be null
   * @param headers map of key/values to be added to the header; may be null
   * @param timeout timeout in milliseconds
   */
  public static String get(String username, String password, String url,
    Map<String,Object> params, String jsonBody, Map<String,String> headers, Integer timeout) {
    return callout('GET', username, password, url, params, jsonBody, headers, timeout);
  }
  public static String get(String username, String password, String url, Map<String,Object> params) {
    return callout('GET', username, password, url, params, null, null, TIMEOUT_DEFAULT);
  }
  
  public static String post(String username, String password, String url,
    Map<String,Object> params, String jsonBody, Map<String,String> headers, Integer timeout) {
    return callout('POST', username, password, url, params, jsonBody, headers, timeout);
  }
  public static String post(String username, String password, String url, Map<String,Object> params) {
    return callout('POST', username, password, url, params, null, null, TIMEOUT_DEFAULT);
  }
  
  public static String put(String username, String password, String url,
    Map<String,Object> params, String jsonBody, Map<String,String> headers, Integer timeout) {
    return callout('PUT', username, password, url, params, jsonBody, headers, timeout);
  }
  public static String put(String username, String password, String url, Map<String,Object> params) {
    return callout('PUT', username, password, url, params, null, null, TIMEOUT_DEFAULT);
  }
  
  public static String del(String username, String password, String url,
    Map<String,Object> params, String jsonBody, Map<String,String> headers, Integer timeout) {
    return callout('DELETE', username, password, url, params, jsonBody, headers, timeout);
  }
  public static String del(String username, String password, String url, Map<String,Object> params) {
    return callout('DELETE', username, password, url, params, null, null, TIMEOUT_DEFAULT);
  }
  
  
  //--------------------------------------------------------------------------
  // Helpers
  private static String callout(String method, String username, String password,
    String url, Map<String,Object> params, String jsonBody,
    Map<String,String> headers, Integer timeout) {
    CfG.preCondition(new Set<String>{'GET','POST','PUT','DELETE'}.contains(method), 'CfG_Callout.callout() - invalid method');
    CfG.preCondition(!CfG.isEmpty(username), 'CfG_Callout.callout() - invalid username');
    CfG.preCondition(!CfG.isEmpty(password), 'CfG_Callout.callout() - invalid password');
    CfG.preCondition(!CfG.isEmpty(url), 'CfG_Callout.callout() - invalid url');
    CfG.preCondition((timeout >= TIMEOUT_MIN) && (timeout <= TIMEOUT_MAX), 'CfG_Callout.callout(): invalid timeout value');
    
    HttpRequest request = new HttpRequest();
    request.setMethod(method);
    
    request.setTimeout(Math.max(TIMEOUT_MIN, Math.min(TIMEOUT_MAX, timeout)));
    
    // url + parameters
    if (null != params) {
      Integer i = 0;
      for (String k : params.keySet()) {
        url += ((0 == i++) ? '?' : '&') + k + params.get(k);
      }
    }
    CfG.debug('CfG_Callout: http(' + method + ', ' + username + ', ' + password + ', ' + url + ')');
    request.setEndpoint(url);
    
    // authentication
    final Blob creds = Blob.valueOf(username + ':' + password);
    request.setHeader(AUTH_TITLE, AUTH_PREFIX + EncodingUtil.base64Encode(creds));
    if (null != headers) {
      for (String h : headers.keySet()) {
        final String value = headers.get(h);
        request.setHeader(h, (null == value) ? '' : value);
      }
    }
        
    // request parameters
    if (!CfG.isEmpty(jsonBody)) {
      request.setHeader(HEADER_TYPE, HEADER_JSON);
      request.setBody(jsonBody);
    }
    
    // make the request and handle the response
    final Http h = new Http();
    HttpResponse response = h.send(request);
    final Integer status = (null != response) ? response.getStatusCode() : 0;
    if (status >= 300) {
      throw new CfG.HttpException(status, response.getBody());
    } else {
      return (null != response) ? response.getBody() : '';
    }
  }
}