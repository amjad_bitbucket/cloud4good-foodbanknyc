public class FBConnectIntakeController{
private final sObject mysObject;
private ApexPages.StandardController theController;    
    
public FBConnectIntakeController(ApexPages.StandardController stdController)
    {
        theController = stdController;
        this.mysObject = (sObject)stdController.getRecord();
    }
public Flow.Interview.MCSBenefitsIntake goflow {get;set;}

public String strOutputVariable {
 get {
 String strTemp = '/apex/FoodBankConnectIntake?sfdc.tabName=01rE0000000MxVT';
 if(goflow != null && goflow.FinshVar !='Yes') {strTemp = '/apex/E_PART?scontrolCaching=1&id='+string.valueOf(goflow.ClientID);}

 return strTemp;
 }
 set { strOutputVariable = value; }
}

public PageReference pRefFinishLocation{      
get {
 PageReference prRef = new PageReference(strOutputVariable);
 prRef.setRedirect(true);
 return prRef;
 }
 set { pRefFinishLocation = value; }
 }
}