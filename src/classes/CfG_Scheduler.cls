/**
 * Class for scheduling any apex class. The name of the class should be passed
 * in to the constructor and the class must implement CfG_Scheduler.ISchedulable.
 *
 * <p>Here's a sample class<br/>
 * <pre>public class Cleanup implements CfG_Scheduler.ISchedulable {
 *   public void execute(SchedulableContext SC) {
 *     //... do your work here
 *   }
 * }
 * </pre>
 *
 * <p>A cleanup process could be scheduled like this:
 * <pre>System.schedule('Cleanup Job', '0 0 2 ? * *', new CfG_Scheduler('Cleanup'));</pre>
 *
 * <p>Monitor jobs specified in your CfG Jobs custom settings like so:
 * <pre>System.schedule('Monitor CfG Jobs', '0 12 * ? * *', new CfG_Scheduler('CfG_Batch.Monitor'));</pre>
 *
 * @author Steve Cox
 */
public class CfG_Scheduler implements Schedulable {
  //--------------------------------------------------------------------------
  // Properties
  private final String className;
  
  /** implement this interface to execute your scheduled job */
  public interface ISchedulable {
    void execute(SchedulableContext SC);
  }
  
  //--------------------------------------------------------------------------
  // Methods
  /** Schedule the batch monitor */
  public CfG_Scheduler() {
    this('CfG_Batch.Monitor');
  }
  
  /**
   * Constructs a scheduler object
   * @param className the name of the class to schedule. Must implement
   *  CfG_Scheduler.ISchedulable
   */
  public CfG_Scheduler(String className) {
    this.className = className;
    
    // make sure this is a valid schedulable class
    try {
      ISchedulable s = (ISchedulable)Type.forName(className).newInstance();
    } catch (Exception ex) {
      throw new SchedulerException('Class must implement CfG_Scheduler.ISchedulable: ' + className);
    }
  }
  
  /** Schedulable method */
  public void execute(SchedulableContext SC) {
    ((ISchedulable)Type.forName(className).newInstance()).execute(SC);
  }
  
  /** declare a new exception type so we're not dependent on the CfG class */
  public class SchedulerException extends System.Exception {}
}