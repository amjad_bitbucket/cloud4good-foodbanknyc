@IsTest(SeeAllData=true)
class Food_Pantry_VisitTest extends CfG_BaseTest {
  //--------------------------------------------------------------------------
  // Tests
  static testMethod void testDateOfLastShop() {
    System.runAs(t.adminUser) {
      t.init();
      Program_Relationship__c[] programs = new Program_Relationship__c[] {
        newProgram(),
        newProgram()
      };
      insert programs;
      
      final Date latestShopDate = Date.today().addDays(10);
      Food_Pantry_Visit__c[] visits = new Food_Pantry_Visit__c[] {
        newMonthlyVisit(programs[0].Id, null),
        newMonthlyVisit(programs[0].Id, latestShopDate),
        newMonthlyVisit(programs[0].Id, Date.today()),
        newMonthlyVisit(programs[1].Id, null)
      };
      
      t.start();
        // program with no visits
        System.assertEquals(null, [SELECT Date_of_Last_Shop__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Last_Shop__c);
        
        // add a visit - but no Date_of_Shop__c
        insert visits[0];
        System.assertEquals(null, [SELECT Date_of_Last_Shop__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Last_Shop__c);
        
        // program with visits - and valid shop dates
        upsert visits;
        System.assertEquals(latestShopDate, [SELECT Date_of_Last_Shop__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Last_Shop__c);
        System.assertEquals(null, [SELECT Date_of_Last_Shop__c FROM Program_Relationship__c WHERE Id = :programs[1].Id].Date_of_Last_Shop__c);
        
        // delete the latest shop date
        delete visits[1];
        System.assertEquals(Date.today(), [SELECT Date_of_Last_Shop__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Last_Shop__c);
        
        // undelete the latest shop date
        undelete visits[1];
        System.assertEquals(latestShopDate, [SELECT Date_of_Last_Shop__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Last_Shop__c);
        
        // delete all
        delete visits;
        System.assertEquals(null, [SELECT Date_of_Last_Shop__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Last_Shop__c);
        System.assertEquals(null, [SELECT Date_of_Last_Shop__c FROM Program_Relationship__c WHERE Id = :programs[1].Id].Date_of_Last_Shop__c);
      t.stop();
    }
  }
  
  static testMethod void testDateOfEmergencyBag() {
    System.runAs(t.adminUser) {
      t.init();
      Program_Relationship__c[] programs = new Program_Relationship__c[] {
        newProgram(),
        newProgram()
      };
      insert programs;
      
      final Date latestShopDate = Date.today().addDays(10);
      Food_Pantry_Visit__c[] visits = new Food_Pantry_Visit__c[] {
        newEmergencyVisit(programs[0].Id, null),
        newEmergencyVisit(programs[0].Id, latestShopDate),
        newEmergencyVisit(programs[0].Id, Date.today()),
        newEmergencyVisit(programs[1].Id, null)
      };
      
      t.start();
        // program with no visits
        System.assertEquals(null, [SELECT Date_of_Emergency_Bag__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Emergency_Bag__c);
        
        // add a visit - but no Date_of_Shop__c
        insert visits[0];
        System.assertEquals(null, [SELECT Date_of_Emergency_Bag__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Emergency_Bag__c);
        
        // program with visits - and valid shop dates
        upsert visits;
        System.assertEquals(latestShopDate, [SELECT Date_of_Emergency_Bag__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Emergency_Bag__c);
        System.assertEquals(null, [SELECT Date_of_Emergency_Bag__c FROM Program_Relationship__c WHERE Id = :programs[1].Id].Date_of_Emergency_Bag__c);
        
        // delete the latest shop date
        delete visits[1];
        System.assertEquals(Date.today(), [SELECT Date_of_Emergency_Bag__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Emergency_Bag__c);
        
        // undelete the latest shop date
        undelete visits[1];
        System.assertEquals(latestShopDate, [SELECT Date_of_Emergency_Bag__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Emergency_Bag__c);
        
        // delete all
        delete visits;
        System.assertEquals(null, [SELECT Date_of_Emergency_Bag__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Emergency_Bag__c);
        System.assertEquals(null, [SELECT Date_of_Emergency_Bag__c FROM Program_Relationship__c WHERE Id = :programs[1].Id].Date_of_Emergency_Bag__c);
      t.stop();
    }
  }
  
  static testMethod void testMixedShopTypes() {
    System.runAs(t.adminUser) {
      t.init();
      Program_Relationship__c[] programs = new Program_Relationship__c[] {
        newProgram(),
        newProgram()
      };
      insert programs;
      
      final Date latestShopDate = Date.today().addDays(10);
      Food_Pantry_Visit__c[] visits = new Food_Pantry_Visit__c[] {
        newMonthlyVisit(programs[0].Id, null),
        newMonthlyVisit(programs[0].Id, latestShopDate),
        newMonthlyVisit(programs[0].Id, Date.today()),
        newEmergencyVisit(programs[0].Id, Date.today()),
        newEmergencyVisit(programs[1].Id, null)
      };
      
      t.start();
        insert visits;
        System.assertEquals(latestShopDate, [SELECT Date_of_Last_Shop__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Last_Shop__c);
        System.assertEquals(Date.today(), [SELECT Date_of_Emergency_Bag__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Emergency_Bag__c);
        
        // delete the latest shop date
        delete visits[1];
        System.assertEquals(Date.today(), [SELECT Date_of_Last_Shop__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Last_Shop__c);
        System.assertEquals(Date.today(), [SELECT Date_of_Emergency_Bag__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Emergency_Bag__c);
        
        // delete the latest emergency date
        delete visits[3];
        System.assertEquals(Date.today(), [SELECT Date_of_Last_Shop__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Last_Shop__c);
        System.assertEquals(null, [SELECT Date_of_Emergency_Bag__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Emergency_Bag__c);
        
        // undelete the latest shop date
        undelete visits[1];
        undelete visits[3];
        System.assertEquals(latestShopDate, [SELECT Date_of_Last_Shop__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Last_Shop__c);
        System.assertEquals(Date.today(), [SELECT Date_of_Emergency_Bag__c FROM Program_Relationship__c WHERE Id = :programs[0].Id].Date_of_Emergency_Bag__c);
      t.stop();
    }
  }
  
  
  //--------------------------------------------------------------------------
  // Helpers
  private static Program_Relationship__c newProgram() {
    return new Program_Relationship__c();
  }
  
  private static Food_Pantry_Visit__c newMonthlyVisit(Id programId, Date shopDate) {
    return newVisit(programId, shopDate, 'Monthly');
  }
  
  private static Food_Pantry_Visit__c newEmergencyVisit(Id programId, Date shopDate) {
    return newVisit(programId, shopDate, 'Emergency');
  }
  
  private static Food_Pantry_Visit__c newVisit(Id programId, Date shopDate, String type) {
    return new Food_Pantry_Visit__c(
      Pantry_Registration__c = programId,
      Date_of_Shop__c = shopDate,
      Type_of_Shop__c = type
    );
  }
  
  static { CfG_BaseTest.t = new Food_Pantry_VisitTest(); }
  static CfG_BaseTest t { get { return CfG_BaseTest.t; } }
}