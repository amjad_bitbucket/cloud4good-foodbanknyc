@IsTest(SeeAllData=true)
class CfG_TestJobProcessorTest extends CfG_BaseTest {
  //--------------------------------------------------------------------------
  // Tests
  static testMethod void test() {
    System.runAs(t.adminUser) {
      t.init();
      
      final String jobId = '01pU0000000w9wZ';
      
      ApexTestQueueItem[] qi = new ApexTestQueueItem[] {
        new ApexTestQueueItem()
      };
      
      Map<Id, ApexTestQueueItem[]> jt = new Map<Id, ApexTestQueueItem[]>();
      jt.put(jobId, new ApexTestQueueItem[] {
        new ApexTestQueueItem()
      });
      
      ApexTestResult[] tr = new ApexTestResult[] {
        new ApexTestResult(Outcome = CfG_TestJobProcessor.PASS_STATUS),
        new ApexTestResult(Outcome = 'bombed')
      };
      
      CfG_QueuedTest__c qt = new CfG_QueuedTest__c(AsyncId__c = '01pU0000000w9wZ');
      insert qt;
      
      t.start();
        CfG_TestJobProcessor p = new CfG_TestJobProcessor();
        p.execute(NULL);
        CfG_TestJobProcessor.futureExecute();
        CfG_TestJobProcessor.processAsyncResults(qi, jt, tr);
        CfG_TestJobProcessor.getTestResultEmailBody(new ApexTestResult[] {});
      t.stop();
    }
  }
  
  //--------------------------------------------------------------------------
  // Helpers
  static { CfG_BaseTest.t = new CfG_TestJobProcessorTest(); }
  static CfG_BaseTest t { get { return CfG_BaseTest.t; } }
}