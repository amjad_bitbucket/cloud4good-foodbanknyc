public class Food_Pantry_VisitHandler extends CfG_BaseHandler implements CfG_ITrigger {
  //--------------------------------------------------------------------------
  // Methods
  public override void bulkAfter() {
    super.bulkAfter();
    
    SObject[] records = Trigger.isDelete ? Trigger.old : Trigger.new;
    Set<String> programIds = CfG.getFieldValues(records, 'Pantry_Registration__c');
    SObject[] parentUpdates = new SObject[] {};
    for (CfG_Rollup.Spec s : getRollupSpecs()) {
      CfG_Rollup.fields(s, programIds, parentUpdates);
    }
    
    update parentUpdates;
  }
  
  
  //--------------------------------------------------------------------------
  // Helpers
  private static CfG_Rollup.Spec[] getRollupSpecs() {
    return new CfG_Rollup.Spec[] {
      new CfG_Rollup.Spec(
        new CfG_Rollup.FieldDef[] {
          new CfG_Rollup.FieldDef(CfG_Rollup.Op.MAX, Food_Pantry_Visit__c.Date_of_Shop__c, Program_Relationship__c.Date_of_Last_Shop__c)
        }, Food_Pantry_Visit__c.SObjectType, Food_Pantry_Visit__c.Pantry_Registration__c, Program_Relationship__c.SObjectType,
        'Type_of_Shop__c = \'Monthly\''),
      
      new CfG_Rollup.Spec(
        new CfG_Rollup.FieldDef[] {
          new CfG_Rollup.FieldDef(CfG_Rollup.Op.MAX, Food_Pantry_Visit__c.Date_of_Shop__c, Program_Relationship__c.Date_of_Emergency_Bag__c)
        }, Food_Pantry_Visit__c.SObjectType, Food_Pantry_Visit__c.Pantry_Registration__c, Program_Relationship__c.SObjectType,
        'Type_of_Shop__c = \'Emergency\'')
    };
  }
}