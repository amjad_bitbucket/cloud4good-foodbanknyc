@IsTest(SeeAllData=true)
class CfG_TriggerFactoryTest extends CfG_BaseTest {
  //--------------------------------------------------------------------------
  // Tests
  static testMethod void testTriggerFactory() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        try {
          CfG_TriggerFactory.handle(null);
          System.assert(false, 'uncaught missing handler');
        } catch (CfG.AssertionException ex) {
        }
        
        /* REDUCE_DEPENDENCIES exercise an existing trigger
        CfG_Test__c o = new CfG_Test__c(Option__c = 'A');
        insert o;
        update o;
        delete o;
        undelete o;
        
        // test exception coverage
        CfG_BaseTest.testException = true;
        try {
          CfG_TriggerFactory.handle(CfG_Test__c.SObjectType);
          System.assert(false, 'cover execution exception');
        } catch (NullPointerException ex) {
        }
        */
        
        try {
          CfG_TriggerFactory.execute(null);
          System.assert(false, 'testing throw from execute');
        } catch (NullPointerException ex) {
        }
      t.stop();
    }
  }
  
  //--------------------------------------------------------------------------
  // Helpers
  static { CfG_BaseTest.t = new CfG_TriggerFactoryTest(); }
  static CfG_BaseTest t { get { return CfG_BaseTest.t; } }
}