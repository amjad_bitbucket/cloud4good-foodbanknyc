public class CommonExtension {

   // These four member variables maintain the state of the wizard.
   // When users enter data into the wizard, their input is stored
   // in these variables. 
   Account account;
  

   // The next four methods return one of each of the four member
   // variables. If this is the first time the method is called,
   // it creates an empty record for the variable.
   public Account getAccount() {
      if(account == null) account = new Account();
      return account;
   }

   
   // The next three methods control navigation through
   // the wizard. Each returns a PageReference for one of the three pages
   // in the wizard. Note that the redirect attribute does not need to
   // be set on the PageReference because the URL does not need to change
   // when users move from page to page.
   public PageReference step1() {
      return Page.C4G_AccountWizard1;
   }

   public PageReference step2() {
      return Page.C4G_AccountWizard2;
   }


   // This method cancels the wizard, and returns the user to the 
   // Opportunities tab
    public PageReference cancel() {
			PageReference opportunityPage = new ApexPages.StandardController(account).view();
			opportunityPage.setRedirect(true);
			return opportunityPage; 
    }

   // This method performs the final save for all four objects, and
   // then navigates the user to the detail page for the new
   // opportunity.
   public PageReference save() {

      system.debug('====Account Name===='+account.name);
      system.debug('====account.site===='+account.site);
      
      // Create the account. Before inserting, copy the contact's
      // phone number into the account phone number field.
     
      //insert account;

      


      PageReference opptyPage = new ApexPages.StandardController(account).view();
      opptyPage.setRedirect(true);

      return opptyPage;
   }

}