/**
 * This class contains unit tests for many of the utility methods in the CfG Library
 * @author Steve Cox
 */
@IsTest(SeeAllData=true)
class CfG_Test extends CfG_BaseTest {
  //--------------------------------------------------------------------------
  // Tests
  static testMethod void testGetPickListValues() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        try {
          CfG.getPickListValues(null);
          System.assert(false, 'field is required');
        } catch (CfG.AssertionException e) {
        }
        
        System.assertEquals(true, CfG.getPickListValues(Account.Id).IsEmpty());
        System.assertEquals(false, CfG.getPickListValues(Account.Type).IsEmpty());
      t.stop();
    }
  }
  
  static testMethod void testGetPickListAsSelectedOptions() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        try {
          CfG.getPickListAsSelectOptions(null);
          System.assert(false, 'invalid field');
        } catch (CfG.AssertionException e) {
        }
        
        System.assertEquals(true, CfG.getPickListAsSelectOptions(Account.Id).IsEmpty());
        System.assertEquals(false, CfG.getPickListAsSelectOptions(Account.Type).IsEmpty());
      t.stop();
    }
  }

  static testMethod void testIsEmpty() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        System.assertEquals(true, CfG.isEmpty((String)null));
        System.assertEquals(true, CfG.isEmpty((Integer)null));
        System.assertEquals(true, CfG.isEmpty(''));
        System.assertEquals(true, CfG.isEmpty(' '));
        System.assertEquals(true, CfG.isEmpty('\t  \n\r'));
        System.assertEquals(true, CfG.isEmpty((String[])null));
        System.assertEquals(true, CfG.isEmpty(new String[] {}));
        System.assertEquals(true, CfG.isEmpty((Set<String>)null));
        System.assertEquals(true, CfG.isEmpty((Map<String,String>)null));
        
        System.assertEquals(true, !CfG.isEmpty('hi'));
        System.assertEquals(true, !CfG.isEmpty(new String[] {'hi'}));
        
        try {
          CfG.isEmpty(new Set<String> {'hi'});
          System.assert(false, 'invalid type');
        } catch (CfG.AssertionException e) {
        }
        
        try {
          CfG.isEmpty(new Map<String,String> {'hi'=>'hi'});
          System.assert(false, 'invalid type');
        } catch (CfG.AssertionException e) {
        }
        
        t.debugging = false;
        System.assertEquals(false, CfG.isEmpty(new Set<String> {'hi'}));
      t.stop();
    }
  }
  
  static testMethod void testFormat() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        try {
          CfG.format(null, null);
          System.assert(false, 'format is required');
        } catch (CfG.AssertionException e) {
        }
        
        try {
          CfG.format(null, '');
          System.assert(false, 'format is required');
        } catch (CfG.AssertionException e) {
        }
        
        try {
          CfG.format(null, '', '');
          System.assert(false, 'format is required');
        } catch (CfG.AssertionException e) {
        }
        
        try {
          CfG.format(null, '', '', '');
          System.assert(false, 'format is required');
        } catch (CfG.AssertionException e) {
        }
        
        System.assertEquals('hi there', CfG.format('hi {0}', 'there'));
        System.assertEquals('hi 0', CfG.format('hi {0}', 0));
        System.assertEquals('hi 0.19', CfG.format('hi {0}', 0.19));
        System.assertEquals('hi (1, 2, 3)', CfG.format('hi {0}', new Integer[] {1, 2, 3}));
        System.assertEquals('hi y\'all', CfG.format('hi {0}', 'y\'all'));
        System.assertEquals('10 there', CfG.format('{1} {0}', 'there', 10));
        System.assertEquals('bye y\'all', CfG.format('{1} {0}', 'y\'all', 'bye'));
        System.assertEquals('H 8 er!', CfG.format('{0} {1} {2}', 'H', 8, 'er!'));
      t.stop();
    }
  }

  static testMethod void testGetPageParam() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        try {
          CfG.getPageParam(null, '');
          System.assert(false, 'key is required');
        } catch (CfG.AssertionException e) {
        }
        
        try {
          CfG.getPageParam(null, 0);
          System.assert(false, 'key is required');
        } catch (CfG.AssertionException e) {
        }
        
        try {
          CfG.getPageParams(null);
          System.assert(false, 'key is required');
        } catch (CfG.AssertionException e) {
        }
        
        System.assertEquals('', CfG.getPageParam('x', ''));
        
        System.assertEquals('y', CfG.getPageParam('x', 'y'));
        System.assertEquals(99, CfG.getPageParam('x', 99));
        
        PageReference pageRef = ApexPages.currentPage();
        pageRef.getParameters().put('x', 'hi');
        Test.setCurrentPage(pageRef);
        System.assertEquals('hi', CfG.getPageParam('x', ''));
        System.assertEquals(99, CfG.getPageParam('x', 99));
        
        pageRef.getParameters().put('x', '7');
        Test.setCurrentPage(pageRef);
        System.assertEquals('7', CfG.getPageParam('x', ''));
        System.assertEquals(7, CfG.getPageParam('x', 99));
        
        pageRef.getParameters().put('x', '1,2,3');
        Test.setCurrentPage(pageRef);
        String[] params = CfG.getPageParams('x');
        params.sort();
        System.assertEquals(3, params.size());
        System.assertEquals('1', params[0]);
        System.assertEquals('2', params[1]);
        System.assertEquals('3', params[2]);
      t.stop();
    }
  }
  
  static testMethod void testRedirect() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        try {
          CfG.redirect(null);
          System.assert(false, 'page is required');
        } catch (CfG.AssertionException e) {
        }
        
        try {
          CfG.redirect(null, new Map<String,String>());
          System.assert(false, 'page is required');
        } catch (CfG.AssertionException e) {
        }
        
        final String testUrl = 'http://www.google.com';
        final PageReference p = new PageReference(testUrl);
        String result = CfG.redirect(p).getUrl().toLowerCase();
        System.assert(result.contains(testUrl));
        
        result = CfG.redirect(p, new Map<String,String> {'Key' => 'Value'}).getUrl().toLowerCase();
        System.assert(result.contains(testUrl));
        System.assert(result.contains('key=value'));
      t.stop();
    }
  }
  
  static testMethod void testCustomExceptions() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        new CfG.AssertionException();
        new CfG.ValidationException();
        new CfG.TriggerException();
        CfG.HttpException h = new CfG.HttpException(99, 'test');
        System.assertEquals(99, h.code);
      t.stop();
    }
  }
  
  static testMethod void testDiagnostics() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        t.debugging = false;
        
        CfG.error('message');
        CfG.warn('message');
        CfG.info('message');
        CfG.debug('message');
        
        // with debugging disabled, none of these fire
        CfG.assert(false);
        CfG.assert(false, 'message');
        CfG.assertEquals(0, 1);
        CfG.assertEquals(0, 1, 'message');
        CfG.assertNotEquals(0, 0);
        CfG.assertNotEquals(0, 0, 'message');
        CfG.preCondition(false, 'message');
        CfG.postCondition(false, 'message');
        CfG.invariant(false, 'message');
        
        // when debugging is enabled, they do fire
        t.debugging = true;
        System.assertEquals(true, CfG.debugging);
        try { CfG.assert(false); System.assert(false); } catch (CfG.AssertionException e) {}
        try { CfG.assert(false, 'message'); System.assert(false); } catch (CfG.AssertionException e) {}
        try { CfG.assertEquals(0, 1); System.assert(false); } catch (CfG.AssertionException e) {}
        try { CfG.assertEquals(0, 1, 'message'); System.assert(false); } catch (CfG.AssertionException e) {}
        try { CfG.assertNotEquals(0, 0); System.assert(false); } catch (CfG.AssertionException e) {}
        try { CfG.assertNotEquals(0, 0, 'message'); System.assert(false); } catch (CfG.AssertionException e) {}
        try { CfG.preCondition(false, 'message'); System.assert(false); } catch (CfG.AssertionException e) {}
        try { CfG.postCondition(false, 'message'); System.assert(false); } catch (CfG.AssertionException e) {}
        try { CfG.invariant(false, 'message'); System.assert(false); } catch (CfG.AssertionException e) {}
      t.stop();
    }
  }
  
  static testMethod void testDiagnosticCaching() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        System.assert(CfG.isEmpty(CfG.debugEntriesToString()));
        
        // if debugBegin is not called, do normal diagnostics
        CfG.debug('normal');
        System.assert(CfG.isEmpty(CfG.debugEntriesToString()));
        
        CfG.debugBegin('1');
          CfG.error('error');
          CfG.debugBegin('2');
            CfG.warn('warn');
            CfG.info('info');
          CfG.debugEnd();
          CfG.debug('debug');
          
          String e = CfG.debugEntriesToString();
          System.assert(e.contains('1'));
          System.assert(e.contains('    ' + CfG.DEBUG_PREFIX + 'warn'));
          System.assert(e.contains('  ' + CfG.DEBUG_PREFIX + 'debug'));
        CfG.debugEnd();
        System.assert(CfG.isEmpty(CfG.debugEntriesToString()));
        
        CfG.debugBegin('3');
          CfG.error('error');
          CfG.debugBegin('4');
            CfG.warn('warn');
            
            e = CfG.debugEntriesToString();
            System.assert(e.contains(CfG.DEBUG_PREFIX + '3'));
            System.assert(e.contains('    ' + CfG.DEBUG_PREFIX + 'warn'));
        CfG.debugEndAll();
        System.assert(CfG.isEmpty(CfG.debugEntriesToString()));
        
        CfG.debug('normal');
        System.assert(CfG.isEmpty(CfG.debugEntriesToString()));
      t.stop();
    }
  }
  
  static testMethod void testDebugException() {
    final DateTime now = System.now();
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        // when not debugging, no log is created
        t.debugging = false;
        try {
          throw new CfG.ValidationException('ex1');
        } catch (Exception ex) {
          CfG.debugException(ex);
        }
        System.assertEquals(0, [SELECT Count() FROM CfG_Log__c WHERE CreatedDate >= :now]);
        
        t.debugging = true;
        try {
          throw new CfG.ValidationException('ex2');
        } catch (Exception ex) {
          CfG.debugException(ex);
        }
        System.assert([SELECT Text__c FROM CfG_Log__c WHERE CreatedDate >= :now LIMIT 1].Text__c.contains('ex2'));
      t.stop();
    }
  }
  
  static testMethod void testGetFieldValues() {
    System.runAs(t.adminUser) {
      t.init();
      User[] users = new User[] {
        t.newUser('0@x.com'), t.newUser('1@x.com'), t.newUser('2@x.com'),
        t.newUser('3@x.com'), t.newUser('4@x.com'), t.newUser('5@x.com')
      };
      insert users;
      
      t.start();
        final Set<String> idFields = CfG.getIds(users);
        System.assertEquals(users.size(), idFields.size());
        
        // test preConditions
        try {
          CfG.getFieldValues(users, (String)null);
          System.assert(false, 'field is required');
        } catch (CfG.AssertionException e) {
        }
        
        // test postConditions
        Set<String> names = CfG.getFieldValues(null, User.UserName);
        System.assert(null != names);
        
        names = CfG.getFieldValues(users, User.MobilePhone);
        System.assert(!names.contains(null));
        
        // successful call
        names = CfG.getFieldValues(users, User.UserName);
        for (User u : users) {
          System.assert(names.contains(u.UserName));
        }
      t.stop();
    }
  }
  
  static testMethod void testIsTriggerActive() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        try {
          System.assertEquals(false, CfG.isTriggerActive(null));
          System.assert(false, 'no such trigger');
        } catch (CfG.AssertionException e) {
        }
        
        // trigger doesn't exist
        System.assertEquals(false, CfG.isTriggerActive(Case.SObjectType));
        
        // exception coverage
        CfG_BaseTest.testException = true;
        System.assertEquals(false, CfG.isTriggerActive(Case.SObjectType));
      t.stop();
    }
  }
  
  static testMethod void testGetIds() {
    System.runAs(t.adminUser) {
      t.init();
      User[] users = new User[] {
        t.newUser('0@x.com'), t.newUser('1@x.com'), t.newUser('2@x.com'),
        t.newUser('3@x.com'), t.newUser('4@x.com'), t.newUser('5@x.com')
      };
      insert users;
      
      t.start();
        // no objects specified
        Set<String> idFields = CfG.getIds(null);
        System.assertEquals(0, idFields.size());
        
        idFields = CfG.getIds(users);
        System.assertEquals(users.size(), idFields.size());
      t.stop();
    }
  }
  
  static testMethod void testNotifyCurrentUser() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        CfG.notifyCurrentUser('subject', 'body');
      t.stop();
    }
  }
  
  static testMethod void testStringToId() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        // invalid Ids
        System.assertEquals(null, CfG.stringToId(null));
        System.assertEquals(null, CfG.stringToId(''));
        System.assertEquals(null, CfG.stringToId('junk'));
        
        // valid Ids
        String userId = UserInfo.getUserId();
        System.assertEquals(userId, CfG.stringToId(userId));
        System.assertEquals(userId, CfG.stringToId(userId.left(15)));
      t.stop();
    }
  }
  
  static testMethod void testFileDataToBlob() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        try {
          CfG.fileDataToBlob(null);
          System.assert(false, 'data is required');
        } catch (CfG.AssertionException e) {
        }
        
        try {
          CfG.fileDataToBlob('');
          System.assert(false, 'data is required');
        } catch (CfG.AssertionException e) {
        }
        
        System.assertNotEquals(null, CfG.fileDataToBlob(EncodingUtil.base64Encode(Blob.valueOf('0'))));
        System.assertNotEquals(null, CfG.fileDataToBlob(EncodingUtil.base64Encode(Blob.valueOf('test 1'))));
      t.stop();
    }
  }
  
  static testMethod void testParseDate() {
    System.runAs(t.adminUser) {
      t.init();
      final Date target = Date.newInstance(2010, 1, 31);
      
      t.start();
        try {
          System.assertEquals(null, CfG.parseDate(null, null, CfG.DATE_ORDER.YMD));
          System.assert(false, 'sep is required');
        } catch (CfG.AssertionException e) {
        }
        
        try {
          System.assertEquals(null, CfG.parseDate(null, '', CfG.DATE_ORDER.YMD));
          System.assert(false, 'sep is required');
        } catch (CfG.AssertionException e) {
        }
        
        try {
          System.assertEquals(null, CfG.parseDate(null, '-', null));
          System.assert(false, 'format is required');
        } catch (CfG.AssertionException e) {
        }
        
        System.assertEquals(null, CfG.parseDate('', '-', CfG.DATE_ORDER.YMD));
        // non-numeric characters aren't allowed
        System.assertEquals(null, CfG.parseDate('Jan 31, 2010', ' ', CfG.DATE_ORDER.MDY));
        // separator must divide the string into 3 components
        System.assertEquals(null, CfG.parseDate('1/31/2010', '-', CfG.DATE_ORDER.MDY));
        System.assertEquals(null, CfG.parseDate('1-31', '-', CfG.DATE_ORDER.MDY));
        // only one separator may be specified
        System.assertEquals(null, CfG.parseDate('1-31-2010', '-/', CfG.DATE_ORDER.MDY));
        
        System.assertEquals(target, CfG.parseDate('2010-1-31', '-', CfG.DATE_ORDER.YMD));
        System.assertEquals(target, CfG.parseDate('2010.1.31', '.', CfG.DATE_ORDER.YMD));
        System.assertEquals(target, CfG.parseDate('2010 1 31', ' ', CfG.DATE_ORDER.YMD));
        System.assertEquals(target, CfG.parseDate('2010/1/31', '/', CfG.DATE_ORDER.YMD));
        System.assertEquals(target, CfG.parseDate('2010-01-31', '-', CfG.DATE_ORDER.YMD));
        System.assertEquals(target, CfG.parseDate('1-31-2010', '-', CfG.DATE_ORDER.MDY));
        // leading 0's are ignored
        System.assertEquals(target, CfG.parseDate('01-031-002010', '-', CfG.DATE_ORDER.MDY));
        // spaces are ignored
        System.assertEquals(target, CfG.parseDate(' 01 - \t031  - 002010 ', '-', CfG.DATE_ORDER.MDY));
      t.stop();
    }
  }
  
  static testMethod void testGetFileType() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        System.assertEquals(null, CfG.getFileType(null));
        System.assertEquals(null, CfG.getFileType(''));
        System.assertEquals(null, CfG.getFileType('.'));
        System.assertNotEquals(null, CfG.getFileType('Test.Pdf'));
        System.assertNotEquals(null, CfG.getFileType(' Test. Pdf '));
        System.assertNotEquals(null, CfG.getFileType(' My.Test. Pdf '));
      t.stop();
    }
  }
  
  static testMethod void testEmail() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        CfG.Email m = new CfG.Email(null, null, null, null, null);
        
        try {
          m.addAttachment(null);
          System.assert(false, 'attachment is required');
        } catch (CfG.AssertionException e) {
        }
        
        try {
          m.addAttachment('f0', null);
          System.assert(false, 'data is required');
        } catch (CfG.AssertionException e) {
        }
        
        final Blob data = Blob.valueOf('d0');
        System.assertEquals(0, m.attachments.size());
        m.addAttachment(new Attachment(Name = 'a0', Body = data));
        System.assertEquals(1, m.attachments.size());
        m.addAttachment(null, data);
        System.assertEquals(2, m.attachments.size());
        m.addAttachment('f1', data);
        System.assertEquals(3, m.attachments.size());
        
        // coverage
        m = new CfG.Email(new String[] {'scox67@gmail.com'}, null, null, null, 'test');
        m.send();
      t.stop();
    }
  }
  
  static testMethod void testExceptionHandling() {
    System.runAs(t.adminUser) {
      t.init();
      
      t.start();
        // no exception, since flag is not set
        CfG_BaseTest.throwIfTesting(new NullPointerException());
        
        CfG_BaseTest.testException = true;
        try {
          CfG_BaseTest.throwIfTesting(new NullPointerException());
          System.assert(false, 'exception should have been thrown');
        } catch (NullPointerException e) {
        }
      t.stop();
    }
  }
  
  //--------------------------------------------------------------------------
  // Platform testing
  static testMethod void testHeapUsage() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        final Integer init = Limits.getHeapSize();
        System.debug('testHeapUsage init: ' + Limits.getHeapSize());
        
        // 1 SObject
        Account[] a = new Account[] { new Account(name='testing 1 2 3') };
        System.debug('with 1 SObject:      ' + (Limits.getHeapSize() - init));
        
        // clear frees most of the heap for that object
        a.clear();
        System.debug('after SObject clear: ' + (Limits.getHeapSize() - init));
        
        // setting to 'null' clears + frees container object
        a = new Account[] { new Account(name='testing 1 2 3') };
        a = null;
        System.debug('after SObject null:  ' + (Limits.getHeapSize() - init));
      t.stop();
    }
  }
  
  
  //--------------------------------------------------------------------------
  // Helpers
  static { CfG_BaseTest.t = new CfG_Test(); }
  static CfG_BaseTest t { get { return CfG_BaseTest.t; } }
}