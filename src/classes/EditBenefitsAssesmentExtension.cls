public with sharing class EditBenefitsAssesmentExtension {
    private final String clientId;
	private final Benefits_Assessment__c ourRecord;
    
    public EditBenefitsAssesmentExtension(ApexPages.StandardController controller) {
        ourRecord = (Benefits_Assessment__c)controller.getRecord();
        List<Benefits_Assessment__c> benefitAssesment = [SELECT Program_Relationship__r.Client__c 
                                                         FROM Benefits_Assessment__c
                                                         WHERE Id = :ourRecord.Id];
        
        if (benefitAssesment.size() == 1) {                                                     
            clientId = benefitAssesment[0].Program_Relationship__r.Client__c;
        }
    }
    
    public PageReference Redirect() {
        PageReference pageRef = Page.E_PART;
        
        if (clientId != null) {
            pageRef.getParameters().put('id', clientId);
        }

		pageRef.getParameters().put('benefitId', ourRecord.Id);
        
        pageRef.setRedirect(true);
        return pageRef;
    }

}