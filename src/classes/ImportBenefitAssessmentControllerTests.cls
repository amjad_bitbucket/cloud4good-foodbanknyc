@isTest
private class ImportBenefitAssessmentControllerTests {

  static testMethod void testImportBenefitAssessmentControllerConstructor() {
    SetupTestDataClass setup_data = new SetupTestDataClass();
    Test.StartTest();
    setup_data.initializeClassUnderTest();
    Test.stopTest();
    System.assertEquals(false, setup_data.ClassUnderTest.IsFileUploaded);
    System.assertEquals(false, setup_data.ClassUnderTest.IsFileProccessed);
    System.assertEquals(false, setup_data.ClassUnderTest.IsWrongFileUploaded);
  }

  static testMethod void testImportBenefitAssessmentControllerImportWithWrongFile() {
    SetupTestDataClass setup_data = new SetupTestDataClass();
    Test.StartTest();
    setup_data.initializeClassUnderTest();
    setup_data.ClassUnderTest.import();
    Test.StopTest();
    List<ApexPages.Message> page_messages = apexpages.getMessages();
    System.assertEquals(true, setup_data.ClassUnderTest.IsFileUploaded);
    System.assertEquals(1, page_messages.size());
    System.assertEquals('An error has occured while importing data. Please make sure input csv file is correct.', page_messages.get(0).getSummary());
  }

  static testMethod void testImportBenefitAssessmentControllerImportWithEmptyFile() {
    SetupTestDataClass setup_data = new SetupTestDataClass();
    Test.StartTest();
    setup_data.initializeClassUnderTest();
    setup_data.setCsvFile(setup_data.emptyCSV);
    setup_data.ClassUnderTest.import();
    Test.StopTest();
    List<ApexPages.Message> page_messages = apexpages.getMessages();
    System.assertEquals(true, setup_data.ClassUnderTest.IsFileUploaded);
    System.assertEquals(1, page_messages.size());
    System.assertEquals('At least header and one record should be present in file.', page_messages.get(0).getSummary());
  }

  static testMethod void testImportBenefitAssessmentControllerImportWithToLargeFile() {
    SetupTestDataClass setup_data = new SetupTestDataClass();
    Test.StartTest();
    setup_data.initializeClassUnderTest();
    String giantCSVFile = setup_data.createLargeCsvFile();
    setup_data.setCsvFile(giantCSVFile);
    setup_data.ClassUnderTest.import();
    Test.StopTest();
    List<ApexPages.Message> page_messages = apexpages.getMessages();
    System.assertEquals(true, setup_data.ClassUnderTest.IsFileUploaded);
    System.assertEquals(1, page_messages.size());
    System.assertEquals('File should not have more then 1000 records.', page_messages.get(0).getSummary());
  }

  static testMethod void testImportBenefitAssessmentControllerImportWithEmptyHeaderLineFile() {
    SetupTestDataClass setup_data = new SetupTestDataClass();
    Test.StartTest();
    setup_data.initializeClassUnderTest();
    setup_data.setCsvFile(setup_data.emptyHeaderLineCSV);
    setup_data.ClassUnderTest.import();
    Test.StopTest();
    List<ApexPages.Message> page_messages = apexpages.getMessages();
    System.assertEquals(true, setup_data.ClassUnderTest.IsFileUploaded);
    System.assertEquals(1, page_messages.size());
    System.assertEquals('Column number 3 header is missing from the file.', page_messages.get(0).getSummary());
  }

/*
  static testMethod void testImportBenefitAssessmentControllerImportWithCorrectFile() {
    SetupTestDataClass setup_data = new SetupTestDataClass();
    Test.StartTest();
    setup_data.initializeClassUnderTest();
    setup_data.setCsvFile(setup_data.correctCSV);
    setup_data.ClassUnderTest.import();
    Test.StopTest();
    System.assertEquals(1, setup_data.ClassUnderTest.CSVRows.size());
    System.assertEquals(true, setup_data.ClassUnderTest.isFileProccessed);
  }
  */

  static testMethod void testImportBenefitAssessmentControllerImportWithExistingData() {
    SetupTestDataClass setup_data = new SetupTestDataClass();
    Test.StartTest();
    setup_data.initializeClassUnderTest();
    setup_data.setCsvFile(setup_data.correctCSV);
    setup_data.insertClient();
    setup_data.insertRelationship();
    setup_data.insertAssessment();
    setup_data.insertHouseholdMember();
    setup_data.ClassUnderTest.import();
    Test.StopTest();
    Client__c[] testClients = [SELECT Id FROM Client__c WHERE Date_of_Birth__c = :setup_data.testClient.Date_of_Birth__c AND First_Name__c = :setup_data.testClient.First_Name__c AND Last_Name__c = :setup_data.testClient.Last_Name__c];
    System.assertEquals(1, testClients.size());
    Program_Relationship__c[] testRelationships = [SELECT Id FROM Program_Relationship__c WHERE Client__c = :setup_data.testClient.Id];
    System.assertEquals(1, testRelationships.size());
    Benefits_Assessment__c[] testAssessments = [SELECT Id FROM Benefits_Assessment__c WHERE Program_Relationship__c = :setup_data.testRelationship.Id];
    System.assertEquals(1, testAssessments.size());
    Household_Member__c[] testMembers = [SELECT Id FROM Household_Member__c WHERE cfg_Benefits_Assessment__c = :setup_data.testAssesment.Id];
    System.assertEquals(2, testMembers.size());
  }

  private class SetupTestDataClass {
    public ImportBenefitAssessmentController ClassUnderTest { get; set; }

    public Client__c testClient;
    public Program_Relationship__c testRelationship;
    public Benefits_Assessment__c testAssesment;
    public Household_Member__c testMember;
    public String emptyCSV = 'im empty header';
    public String emptyHeaderLineCSV = 'header, header2,,header4 \n ,line 1,line 2, line 3, line4 \n,,,,,';
    public String correctCSV = 'File Path,Date,Prescreener,Assistance Type,Location of Screening,How did you hear of us?,Language,Ethnicity,Gender,EFS eligibility,How Hear Notes,Zip,Primary Phone,First Name 1,Last Name 1,Relationship 1,Citizen Status 1,DOB 1,Age 1,Type of Income 1,Student 1,First Name 2,Last Name 2,Relationship 2,Citizen Status 2,DOB 2,Age 2,Type of Income 2,Student 2,First Name 3,Last Name 3,Relationship 3,Citizen Status 3,DOB 3,Age 3,Type of Income 3,Student 3,First Name 4,Last Name 4,Relationship 4,Citizen Status 4,DOB 4,Age 4,Type of Income 4,Student 4,First Name 5,Last Name 5,Relationship 5,Citizen Status 5,DOB 5,Age 5,Type of Income 5,Student 5,First Name 6,Last Name 6,Relationship 6,Citizen Status 6,DOB 6,Age 6,Type of Income 6,Student 6,First Name 7,Last Name 7,Relationship 7,Citizen Status 7,DOB 7,Age 7,Type of Income 7,Student 7,Is the Household Homeless?,Does the Household have a disqualified member due to SNAP sanction or IPV?,1. Is anyone elderly (60+) / disabled? ,2. Does the household have out-of-pocket Child /Dependent care costs?,3. How many people are in the SNAP Household?,"4. How many, if any, ineligible immigrants in the household?","      5.  Household\'s total monthly gross earned income (salary, self-employment after expenses, etc.) ","      8.  Monthly unearned Cash Assistance income (TANF, Safety Net, etc.) ",      9.  Monthly unearned Social Security income (Retirement/Survivor\'s/SSI/SSD),"     10. Monthly unearned ""Other Income""  (Alimony, Child Support, Annuity, Pensions, Contributions,etc.)",     12.  Actual amount of legally obligated Child Support,medical expenses,dependant care expenses,Rent or mortgage,Estimated Allotment,Screemimg Outcome,Referral 1,Referral 2,Referral 3,Referral 4,Referral 5,Other,Application Assist 1,Application Assist 2,Date 1,Outcome 1,Benefit Amount 1,Confirmed By 1,Notes 1,Date 2,Outcome 2,Benefit Amount 2,Confirmed By 2,Notes 2,Date 3,Outcome 3,Benefit Amount 3,Confirmed By 3,Notes 3,Date 4,Outcome 4,Benefit Amount 4,Confirmed By 4,Notes 4,Date 5,Outcome 5,Benefit Amount 5,Confirmed By 5,Notes 5,"Expidited \n SNAP Follow Up Due Date" \n ,10/10/2013,prescreener,New Application,locationofscreening,Former Tax Client,English,American Indian/Alaskan Native,M,1/1/1915,hohearnotes,zip,primaryphone,First Name,last name,"relatio,nship","Eligible, Noncitizen",10/10/1985,age,Unearned,Y,firstname2,last name 2,Grandchild,Ineligible Noncitizen,10/10/1986,age2,Earned/Unearned,N,1/0/1900,1/0/1900,1/0/1900,1/0/1900,1/0/1900,0,1/0/1900,1/0/1900,1/0/1900,1/0/1900,0,1/0/1900,1/0/1900,0,1/0/1900,Y,1/0/1900,1/0/1900,0,1/0/1900,1/0/1900,0,1/0/1900,1/0/1900,1/0/1900,1/0/1900,0,1/0/1900,1/0/1900,0,1/0/1900,1/0/1900,1/0/1900,1/0/1900,1/0/1900,1/0/1900,1/0/1900,0,1/0/1900,1/0/1900,Y,N,Y,N,2,3,1233,4123,512,758,0,1111,2222,5554,0,MARU,Adult Protective Services,Child Care,Children\'s Services (ACS),Free Cell Phone,Health Insurance,other,Translation,Replacement SNAP,2354,Application Submitted,234,lol,lol1,2354,Application NOT Submitted,234,lol3,lol4,345,Single Issuance Received,6456,lol5,lol6,1243,Denied Benefits,234,lol7,lol8,23,Does Not Want to Apply,234,lol9,lol10,FALSE \n,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,';

    public SetupTestDataClass() {
      Test.setCurrentPage(Page.ImportBenefitAssessment);
    }

    public void initializeClassUnderTest() {
      ClassUnderTest = new ImportBenefitAssessmentController();
    }

    public void setCsvFile(String csv) {
      ClassUnderTest.csvFileBody = blob.valueOf(csv);
    }

    public String createLargeCsvFile() {
      String output = 'im new line \n';

      for (Integer i = 0; i <1001; i++) {
        output = output + ' ,im new line \n';
      }

      return output;
    }

    public void insertClient() {
      testClient = new Client__c();
      testClient.First_Name__c = 'First Name';
      testClient.Last_Name__c = 'last name';
      testClient.Date_of_Birth__c = date.Parse('10/10/1985');
      insert testClient;
    }

    public void insertRelationship() {
      testRelationship = new Program_Relationship__c();
      testRelationship.Client__c = testClient.Id;
      testRelationship.RecordTypeId = [SELECT Id, SobjectType, Name FROM RecordType WHERE Name = 'Benefits' AND SobjectType = 'Program_Relationship__c' LIMIT 1].Id;
      testRelationship.Date__c = date.parse('10/10/2013');
      testRelationship.Location_of_Screening__c = 'locationofscreening';
      insert testRelationship;
    }

    public void insertAssessment() {
      testAssesment = new Benefits_Assessment__c();
      testAssesment.Program_Relationship__c = testRelationship.Id;
      testAssesment.cfg_Date_of_Assessment__c = date.parse('10/10/2013');
      User[] prescreener = [SELECT Id FROM User WHERE Name = 'prescreener'];
      if (prescreener.size() != 0) {
        testAssesment.cfg_Prescreener__c = prescreener.get(0).Id;
      }
      insert testAssesment;
    }

    public void insertHouseholdMember() {
      testMember = new Household_Member__c();
      testMember.First_Name__c = 'First Name2';
      testMember.Last_Name__c = 'last name2';
      testMember.Date_of_Birth__c = date.Parse('10/10/1986');
      testMember.cfg_Benefits_Assessment__c = testAssesment.Id;
      insert testMember;
    }
  }
}