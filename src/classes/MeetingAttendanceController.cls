public class MeetingAttendanceController {
    private final static String CONTACT_SCHOOL_STAFF_RT = 'Cookshop_Participant';
    private final static String MEETING_ATTENDEE_REGISTERED = 'Registered';

    public class SelecTableAtendee {
        
        public Contact contact {get; set;}
        public String reason {get; set;}
        
        public Boolean selectedAttended {get; set;}
        public ID id {get; set;}
        public Meeting_Attendee__c meeting {
          get; 
          set {
            if (value != meeting) {
              if (null != value && null != value.Early_check_out_time__c) {
                earlyCheckOutTime = value.Early_check_out_time__c.time();
              }

              meeting = value;
            }
          }
        }

        public Id program {get; set;}

        public SelecTableAtendee(Contact con, ID id, Id program) {
            this.contact = con;  
            this.id = id;
            this.selectedAttended = false;
            this.program = program;
        }
        
        public Time earlyCheckOutTime {get; set;}

        public PageReference Save() {
            meeting.Date__c = System.today();
            meeting.Early_check_out_time__c = (null == earlyCheckOutTime) ? (null) : (DateTime.newInstance(System.today(), earlyCheckOutTime));
            meeting.Attended__c = meeting.Check_out__c;
            update meeting;

            AggregateResult[] groupedResults = [SELECT COUNT(ID) cnt, Meeting_Name__c FROM Meeting_Attendee__c
                                                 WHERE Attended__c = true 
                                                       AND Meeting_Name__r.CookShop_Program__c = :program
                                                       AND Meeting_Name__r.Date_Time__c <=: System.now()
                                                 GROUP BY Meeting_Name__c];

            Double NumberOfMeeting = 0; 
            Double NumberOfMeetingAttendees = 0;

            for (AggregateResult ar : groupedResults) {
              NumberOfMeeting++;
              NumberOfMeetingAttendees += Decimal.valueOf(''+ar.get('cnt'));
            }

            /*System.QueryException: Aggregate query has too many rows for direct assignment, use FOR loop
            This error happens when trying to use aggregate function (COUNT - size) on a query that has too
            many rows(200+)

            for(Meeting__c meet : [SELECT Id, (SELECT Id FROM Meeting_Attendees__r WHERE Attended__c = true) 
                                   FROM Meeting__c WHERE CookShop_Program__c =:program AND Date_Time__c <=: System.now()]){
                  
                NumberOfMeeting += 1;
                NumberOfMeetingAttendees += meet.Meeting_Attendees__r.size();
            }*/
            
            if(NumberOfMeeting > 0){  
                CookShop_Program__c program = new CookShop_Program__c(Id = program);
                program.Average_Attendance_Rate__c = NumberOfMeetingAttendees / NumberOfMeeting;
                update program;  
            }
            
            return null;
        }
    }

    public String filterForAttendees {
        get; 
        set {
            if (value != filterForAttendees) {
                filterForAttendees = value;
                FilterListOfAttendees();
            }
        }
    }
    
    private void FilterListOfAttendees() {
        viewListOfAttendees = new SelecTableAtendee[] { };
        
        if (null != listAttendees) {
            for (SelecTableAtendee item : listAttendees) {
                if ((filterForAttendees == 'All Registered Attendees') 
                        || (filterForAttendees == 'Attendees not checked in' && item.meeting.Check_in__c == false)
                        || (filterForAttendees == 'Attendees checked in' && item.meeting.Check_in__c == true)) {
                    viewListOfAttendees.add(item);
                }
            } 
            
            SortData();
        }
    }

    public SelecTableAtendee[] viewListOfAttendees { get; private set; }
    
    public Boolean viewPopUpWindow {get; private set;}
    
    public Set<Id> registeredContacts {get; private set;}
    
    public Contact newRegisterContact {get; set;}
    public Meeting_Attendee__c meetingAttendee {get; set;}
    
    private Map<String, Id> contactRecordTypes = new Map<String, Id>();
    private Id getContactRecordType(String recordTypeName) {
      if (contactRecordTypes.containsKey(recordTypeName)) {
        return contactRecordTypes.get(recordTypeName);
      } else {
        RecordType[] rts = [SELECT Id FROM RecordType WHERE isActive = true AND SObjectType = 'Contact' AND DeveloperName = :recordTypeName];

        Id rtId;
        if (false == rts.isEmpty()) {
          rtId = rts[0].Id;
        }

        contactRecordTypes.put(recordTypeName, rtId);
        return rtId;
      }
    }

    public PageReference PreFillContactInfo() {
        if (registeredContacts.contains(meetingAttendee.Contact__c)) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'This user is already registered');
            ApexPages.addMessage(myMsg);
        } else {
            Contact[] selectedContact = [SELECT Id, FirstName, LastName, Email, Position__c, AccountId 
                                       FROM Contact WHERE Id = :meetingAttendee.Contact__c];
            
            if (!selectedContact.isEmpty()) {                           
                newRegisterContact.FirstName = selectedContact[0].FirstName;
                newRegisterContact.LastName = selectedContact[0].LastName;
                newRegisterContact.Email = selectedContact[0].Email;
                newRegisterContact.Position__c = selectedContact[0].Position__c;
                newRegisterContact.AccountId = selectedContact[0].AccountId;
            }
        }
        
        return null;
    }
    
    public Id getContactSchoolRecordTypeId() {
        return getContactRecordType(CONTACT_SCHOOL_STAFF_RT);
    }
    
    public PageReference RegisterNewAttendee() {
        viewPopUpWindow = true;
        newRegisterContact = new Contact(RecordTypeId = getContactRecordType(CONTACT_SCHOOL_STAFF_RT));
        meetingAttendee = new Meeting_Attendee__c();
        return null;
    } 
    
    public PageReference Confirm() {
        // check of existion Contact
        Contact[] contacts = [SELECT Id FROM Contact WHERE FirstName = :newRegisterContact.FirstName 
                              AND LastName = :newRegisterContact.LastName 
                              AND Email = :newRegisterContact.Email
                              AND Position__c = :newRegisterContact.Position__c
                              AND AccountId = :newRegisterContact.AccountId];
                              
        if (contacts.isEmpty()) {
            insert newRegisterContact;
        } else {
            newRegisterContact = contacts[0];
        }
        
        newRegisterContact = [SELECT Id, Name, FirstName, LastName, Email, AccountId, 
                              Position__c, Account.Name FROM Contact WHERE Id = :newRegisterContact.Id LIMIT 1 ];
        
        // create Meeting Attendee record
        meetingAttendee.Contact__c = newRegisterContact.Id;
        meetingAttendee.Meeting_Name__c = meeting.Id;
        meetingAttendee.Registration_Status__c = MEETING_ATTENDEE_REGISTERED;
        insert meetingAttendee;
        
        SelecTableAtendee contactSelection = new SelecTableAtendee(newRegisterContact, meetingAttendee.Id, meeting.CookShop_Program__c);
        registeredContacts.add(newRegisterContact.Id);
        contactSelection.meeting = meetingAttendee;
        
        if (null == listAttendees) {
            listAttendees = new SelecTableAtendee[] {contactSelection};
        } else {
            listAttendees.add(contactSelection);
        }
        
        FilterListOfAttendees();
        
        viewPopUpWindow = false;
        return null;
    }
    
    public PageReference CloseRegisterPopup() {
        viewPopUpWindow = false;
        return null;
    } 
    
    public String sortExpression {
        get;
        set {
           //if the column is clicked on then switch between Ascending and Descending modes
           if (value == sortExpression) {
               sortDirection = (sortDirection == 'ASC') ? 'DESC' : 'ASC';
           } else {
             sortDirection = 'ASC';
           }
           
           sortExpression = value;
        }
    }
    
    public String SortDirection {
        get {
            //if not column is selected 
            if (sortExpression == null || sortExpression == '') {
                return 'ASC';
            } else {
                return sortDirection;
            }
        }
        set;
    }
    
    public PageReference SortData() {
        Map<String, SelecTableAtendee[]> mapAttendees = new Map<String, List<SelecTableAtendee>>();
        
        for (SelecTableAtendee item : viewListOfAttendees) {
            String key = '';
            if (sortExpression == 'Name') {
                key = item.Contact.Name;
            } else if (sortExpression == 'School') {
                key = item.Contact.Account.Name;
            }
            
            if (mapAttendees.get(key) == null) {
                mapAttendees.put(key, new SelecTableAtendee[] {item});
            } else {
                mapAttendees.get(key).add(item);
            }
        }
        
        String[] keyToSort = new String[] { };
        keyToSort.addAll(mapAttendees.keySet());
        keyToSort.sort();
        
        viewListOfAttendees = new SelecTableAtendee[] {};
        for (Integer i=0; i<keyToSort.size(); i++) {
            Integer index = i;
            if (SortDirection == 'DESC') {
                index = keyToSort.size() - i - 1;
            }
            
            String key = keyToSort.get(index);
            viewListOfAttendees.addAll(mapAttendees.get(key));
        }
        
        return null;
    }
    
    public class MyException extends Exception {}
        
    private List<SelecTableAtendee> listAttendees = null;
    private final ApexPages.StandardController theController;
    private Meeting__c meeting;
    private Map<ID, Meeting_Attendee__c> listExistMeetingAttendee = new Map<ID, Meeting_Attendee__c>(); // <Contact__c.Id,Meeting_Attendee__c>
    
    public MeetingAttendanceController(ApexPages.StandardController stdcontroller) {
        
        registeredContacts = new Set<Id>();
        
        theController = stdController;
        meeting = [SELECT Id, Name, Special_Expenses__c, CookShop_Program__c, Primary_Facilitator__c, Description__c, Date_Time__c, Facility__c FROM Meeting__c WHERE Id = :theController.getId()];
            
        List<Contact> contacts = [ Select Id, FirstName, LastName, Name, Position__c, Account.Name, Email 
                                    From Contact where Id IN (Select Contact__c From Meeting_Attendee__c 
                                                              where Meeting_Name__r.CookShop_Program__c = :meeting.CookShop_Program__c 
                                                              and Registration_Status__c = :MEETING_ATTENDEE_REGISTERED ) 
                                  ];
        // List MeetingAttendee
        for(Meeting_Attendee__c meet: [SELECT Id, Contact__c, Reason_if_not_Attended__c, Attended__c, Check_in__c, Check_out__c, 
                                        Early_check_out_time__c, K_2_Curriculum_Training__c, Classroom__c FROM Meeting_Attendee__c
                                        WHERE Meeting_Name__c = :theController.getId()]){
            listExistMeetingAttendee.put(meet.Contact__c, meet);            
        }
                    
        if(contacts.size() > 0){
            listAttendees = new List<SelecTableAtendee>();
            for (Contact contact : contacts) {
                SelecTableAtendee contactSelection;
                if(!listExistMeetingAttendee.isEmpty() && listExistMeetingAttendee.containsKey(contact.Id)){
                    Meeting_Attendee__c meet = listExistMeetingAttendee.get(contact.Id);
                    if (null != meet) {
                        contactSelection = new SelecTableAtendee(contact, meet.Id, meeting.CookShop_Program__c);
                        registeredContacts.add(contact.Id);
                        contactSelection.reason = meet.Reason_if_not_Attended__c;
                        contactSelection.selectedAttended = meet.Attended__c;
                        contactSelection.meeting = meet;
                        listAttendees.add(contactSelection);
                    }
                } 

            }
        }


        // set filter selected value
        filterForAttendees = 'All Registered Attendees';
        viewPopUpWindow = false;
    }
    
    public List<SelecTableAtendee> getListAttendees() {       
        return listAttendees;
    }
    
    public void setListContact(List<SelecTableAtendee> s) {
        listAttendees = s;
        FilterListOfAttendees(); 
    }
    
    public Meeting__c getMeeting(){
        return meeting;
    }
}