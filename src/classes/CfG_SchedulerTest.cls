@IsTest(SeeAllData=true)
class CfG_SchedulerTest extends CfG_BaseTest {
  //--------------------------------------------------------------------------
  // Tests
  static testMethod void testScheduler() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        try {
          new CfG_Scheduler(null);
          System.assert(false, 'invalid class name');
        } catch (CfG_Scheduler.SchedulerException ex) {
        }
        
        try {
          new CfG_Scheduler('junk');
          System.assert(false, 'invalid class name');
        } catch (CfG_Scheduler.SchedulerException ex) {
        }
        
        CfG_Scheduler s = new CfG_Scheduler('CfG_TestJobQueuer');
        s.execute(null);
      t.stop();
    }
  }
  
  //--------------------------------------------------------------------------
  // Helpers
  static { CfG_BaseTest.t = new CfG_SchedulerTest(); }
  static CfG_BaseTest t { get { return CfG_BaseTest.t; } }
}