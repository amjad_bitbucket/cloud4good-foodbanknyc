public class EPARTflowController{
// Instantiate the Flow for use by the Controller - linked by VF "interview" attribute
 public Flow.Interview.EPART_Client_Flow goflow {get;set;}

 // Factor your PageReference as a full GET/SET
 public PageReference prFinishLocation {
 get {
 PageReference prRef = new PageReference('/' + strOutputVariable);
 prRef.setRedirect(true);
 return prRef;
 }
 set { prFinishLocation = value; }
 }

 // Factor your Flow output variable pull as a full GET / SET
 public String strOutputVariable {
 get {
 String strTemp = '';

 if(goflow != null) {
 strTemp = string.valueOf(goflow.getVariableValue('txtClientId2'));
 }

 return strTemp;
 }
 set { strOutputVariable = value; }
}
}