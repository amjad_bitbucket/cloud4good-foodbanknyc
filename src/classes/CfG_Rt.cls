/**
 * Where you use record types on several objects, a convenient way to obtain the
 * record type ID in Apex is needed. And as the record type ID requires a SOQL
 * query to obtain the org-specific value, governor limits and efficiency come in
 * to play too.
 * <p>
 * The pattern illustrated below allows the ID to be obtained using this simple
 * and flexible call:
 * <pre>
 *   Id theId = CfG_Rt.getId(Account.SObjectType, CfG_Rt.Name.TYPE);
 * </pre>
 * <p>
 * The benefits of this approach are:
 * <ol>
 * <li>Only one SOQL query done (per server request) no matter how many record
 *   type IDs are referenced or where they are referenced e.g. inside a loop.</li>
 * <li>Avoids problems of typos in strings by using the compile-time checked
 *   SObjectType field and an enum for the Name values.</li>
 * <li>Handles the case where multiple objects use the same record type name well.</li>
 * <li>Adding a new record type just requires a single line to be added to the
 *   Name enum.</li>
 * </ol>
 * <p>
 * (from <a href="http://force201.wordpress.com/2011/07/22/accessing-record-type-ids-in-apex-code/">
 * Force 201's article</a>)
 *
 * @author Steve Cox
 */
public with sharing class CfG_Rt {
  //--------------------------------------------------------------------------
  // Properties
  /** These (developer) names can apply to more than one SOBjectType. */
  public enum Name {
    Junk,
    
    // CfG_Test__c
    Test1
  }
  
  
  //--------------------------------------------------------------------------
  // Methods
  /**
   * Get the Id that can be set on or compared with an SOBject's
   * RecordTypeId field. Backed by a cache of all record type ids for the duration
   * of a request.
   * @param obType the object type to retrieve the record type of; may not be null.
   * @param developerName the record type name; may not be null
   * @return the Id for the specified object type and name
   */
  public static Id getId(SObjectType obType, Name developerName) {
    return getId(obType, developerName.name());
  }

  /** This one is needed for reserved names (e.g. System) */
  public static Id getId(SObjectType obType, String developerName) {
    CfG.preCondition(null != obType, 'CfG_Rt.getId() - invalid obType');
    CfG.preCondition(null != developerName, 'CfG_Rt.getId() - invalid developerName');
    
    if (null == CACHE) {
      CACHE = new Map<String, Id>();
      for (RecordType rt : [SELECT SObjectType, DeveloperName
        FROM RecordType WHERE IsActive = true AND DeveloperName IN :names]) {
        CACHE.put(rt.SObjectType + SEPARATOR + rt.DeveloperName, rt.Id);
      }
    }
    
    final Id result = CACHE.get(String.valueOf(obType) + SEPARATOR + developerName);
    CfG.postCondition(null != result, 'CfG_Rt.getId() - type not found');
    return result;
  }
  
  
  //--------------------------------------------------------------------------
  // Helpers
  private static final String SEPARATOR = '::::';
  private static Map<String, Id> CACHE;

  /* @return the set of all available record type names */
  private static Set<String> names {
    get {
      if (null == names) {
        names = new Set<String>();
        for (Name n : Name.values()) {
          names.add(n.name());
        }
      }
      return names;
    }
    
    private set;
  }
}