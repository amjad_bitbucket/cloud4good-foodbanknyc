@IsTest(SeeAllData=true)
class CfG_CalloutTest extends CfG_BaseTest {
  public class HttpMockResponse implements HttpCalloutMock {
    private String body;
    private Integer status;
     
    public HttpMockResponse(String body, Integer status) {
      this.body = body;
      this.status = status;
    }
    
    public HTTPResponse respond(HTTPRequest req) {
      HttpResponse result = new HttpResponse();
      result.setBody(body);
      result.setStatusCode(status);
      return result;
    }
  }
  
  
  //--------------------------------------------------------------------------
  // Tests
  static testMethod void testCallouts() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        Test.setMock(HttpCalloutMock.class, new HttpMockResponse('test', 200));
        
        // exercise all of the constructors
        final Integer TO = CfG_Callout.TIMEOUT_DEFAULT;
        final Map<String,Object> params = new Map<String,Object> { 'k' => 'v' };
        final Map<String,String> headers = new Map<String,String> { 'k' => 'v' };
        System.assertEquals('test', CfG_Callout.get('u', 'p', 'url', params, 'junk', headers, TO));
        System.assertEquals('test', CfG_Callout.get('u', 'p', 'url', params));
        System.assertEquals('test', CfG_Callout.post('u', 'p', 'url', params, 'junk', headers, TO));
        System.assertEquals('test', CfG_Callout.post('u', 'p', 'url', params));
        System.assertEquals('test', CfG_Callout.put('u', 'p', 'url', params, 'junk', headers, TO));
        System.assertEquals('test', CfG_Callout.put('u', 'p', 'url', params));
        System.assertEquals('test', CfG_Callout.del('u', 'p', 'url', params, 'junk', headers, TO));
        System.assertEquals('test', CfG_Callout.del('u', 'p', 'url', params));
        
        // simulate error code 300
        Test.setMock(HttpCalloutMock.class, new HttpMockResponse('test', 300));
        try {
          CfG_Callout.get('u', 'p', 'url', null, '[{a:1}]', null, TO);
          System.assert(false, 'HTTP should have thrown');
        } catch (CfG.HttpException e) {
        }
        
        // simulate error code 400
        Test.setMock(HttpCalloutMock.class, new HttpMockResponse('test', 400));
        try {
          CfG_Callout.get('u', 'p', 'url', null, '', null, TO);
          System.assert(false, 'HTTP should have thrown');
        } catch (CfG.HttpException e) {
        }
      t.stop();
    }
  }
  
  //--------------------------------------------------------------------------
  // Helpers
  static { CfG_BaseTest.t = new CfG_CalloutTest(); }
  static CfG_BaseTest t { get { return CfG_BaseTest.t; } }
}