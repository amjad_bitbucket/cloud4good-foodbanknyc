/**
 * This is the parent class for models. Model classes should contain
 *  the data and business rules for the application. String constants for record
 *  types, pick list values, SOQL queries, etc. should be defined here for easy
 *  maintenance.
 * <p>
 *  An CfG_BaseModel represents a single SObject record in the system. This record
 *  must be provided by the derived class. Methods include validation, CRUD
 *  access inquiries, save, and validation.
 * <p>
 *  Validate must be provided by the derived class.
 * <p>
 *  By default, save does an upsert on the current record. It can be overridden
 *  for specialized save requirements.
 *
 * @author Steve Cox
 */
public abstract with sharing class CfG_BaseModel {
  //--------------------------------------------------------------------------
  // Constants
  
  //--------------------------------------------------------------------------
  // Properties
  /** CfG Id for the current object. When changed, sObj is reset as well */
  public Id theId {
    get;
    set {
      if (value != theId) {
        theId = value;
        sObj = null;
      }
    }
  }
  
  /** The object with ID = theId */
  public SObject sObj {
    get {
      if (null == sObj) {
        sObj = CfG.isEmpty(theId) ? theType.newSObject() :
          Database.query('SELECT ' + String.join(new List<String>(fields), ',') +
            ' FROM ' + theType + ' WHERE Id = :theId LIMIT 1');
      }
      return sObj;
    }
    private set;
  }
  
  
  //--------------------------------------------------------------------------
  // Constructor
  /**
   * @param theType required
   * @param theId optional Salesforce Id of the object
   */
  public CfG_BaseModel(SObjectType theType, Id theId) {
    CfG.preCondition(null != theType, 'CfG_BaseModel() - theType is required');
    
    this.theType = theType;
    this.theId = theId;
  }
  
  
  //--------------------------------------------------------------------------
  // Methods
  /** get a record type for this model */
  public Id getRecordTypeId(CfG_Rt.Name name) {
    CfG.preCondition(null != name, 'CfG_BaseModel.getRecordTypeId() - invalid record type name');
    final Id id = CfG_Rt.getId(theType, name);
    CfG.postCondition(null != id, 'CfG_BaseModel.getRecordTypeId() - missing record type');
    return id;
  }
  
  /** add fields to this model; causes a requery of the object */
  public void addFields(SObjectField[] fields) {
    CfG.preCondition(null != fields, 'CfG_BaseModel.addFields() - fields are required');
    
    for (SObjectField f : fields) {
      final DescribeFieldResult d = f.getDescribe();
      this.fields.add(d.getName());
    }
    this.sObj = null;  // force requery
  }
  
  /** add fields to this model; causes a requery of the object */
  public void addFields(FieldSet fieldSet) {
    CfG.preCondition(null != fieldSet, 'CfG_BaseModel.addFields() - fieldSet is required');
    
    for (FieldSetMember m : fieldSet.getFields()) {
      this.fields.add(m.fieldPath);
    }
    this.sObj = null;  // force requery
  }
  
  /** validates, then saves the object */
  public virtual void save() {
    if (this.validate(sObj)) {
      //throw new CfG.ValidationException('just testing...');
      upsert sObj;
    }
  }
  
  /** override this method to provide validation of the model */
  public abstract Boolean validate(SObject obj);
  
  /** validates this object */
  public virtual Boolean validate() {
    return this.validate(sObj);
  }
  
  
  //--------------------------------------------------------------------------
  // Collection Methods
  
  
  //--------------------------------------------------------------------------
  // Accessibility Methods
  /** is viewing of this model allowed? */
  public virtual Boolean canView() {
    return objDescribe.isAccessible();
  }
  
  /** is inserting of this model allowed? */
  public virtual Boolean canInsert() {
    return objDescribe.isCreateable();
  }
  
  /** is updating of this model allowed? */
  public virtual Boolean canUpdate() {
    return objDescribe.isUpdateable();
  }
  
  /** is deleting of this model allowed? */
  public virtual Boolean canDelete() {
    return objDescribe.isDeletable();
  }
  
  
  //--------------------------------------------------------------------------
  // Helpers
  private SObjectType theType;
  
  private Set<String> fields = new Set<String> { 'Id' };
  
  private DescribeSObjectResult objDescribe {
    get {
      if (null == objDescribe) {
        objDescribe = sObj.getSObjectType().getDescribe();
      }
      return objDescribe;
    }
    private set;
  }
}