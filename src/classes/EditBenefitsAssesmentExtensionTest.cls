@isTest(SeeAllData=true)
private class EditBenefitsAssesmentExtensionTest extends CfG_BaseTest {
  
    //--------------------------------------------------------------------------
    // Test
    static testMethod void testRedirectAddress() { 
        System.runAs(t.adminUser) {
            t.init();
            
            Client__c client = createTestClient();
            Benefits_Assessment__c benefitAssesment = createBenefitAssesment(client);

            ApexPages.StandardController sc = new ApexPages.standardController(benefitAssesment);
            EditBenefitsAssesmentExtension editBenefit = new EditBenefitsAssesmentExtension(sc);
            
            PageReference pageRef;
            
            t.start();
                pageRef = editBenefit.Redirect();
            t.stop();
            
            System.assertNotEquals(null, pageRef);
            System.assertEquals(true, pageRef.getRedirect());
            System.assertEquals(benefitAssesment.Id, pageRef.getParameters().get('benefitId'));
            System.assertEquals(client.Id, pageRef.getParameters().get('Id')); 
        }
    }

    
    //--------------------------------------------------------------------------
    // Helpers
    
    private static Client__c createTestClient() {
      Client__c client = new Client__c (
        First_Name__c = 'FirstName',
        Last_Name__c = 'SecondName'
      );
      
      insert client;
      return client;
    }
    
    private static Benefits_Assessment__c  createBenefitAssesment(Client__c client) {
        Benefits_Assessment__c benefitAssesment = new Benefits_Assessment__c();
        
        Program_Relationship__c programRelationShip = createProgramRelationship(client);
        benefitAssesment.Program_Relationship__c = programRelationShip.Id;
        
        insert benefitAssesment;
        return benefitAssesment;
    }
    
    private static Program_Relationship__c createProgramRelationship(Client__c client) {
        Program_Relationship__c programRelationship = new Program_Relationship__c();
        
        programRelationship.RecordTypeId = [SELECT Id FROM RecordType WHERE Name='Benefits' LIMIT 1].Id;
        programRelationship.Client__c = client.Id;
        
        insert programRelationship;
        return programRelationship;
      }
    
    static { CfG_BaseTest.t = new EditBenefitsAssesmentExtensionTest(); }
    static CfG_BaseTest t { get { return CfG_BaseTest.t; } }
}