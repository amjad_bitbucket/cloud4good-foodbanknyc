@IsTest(SeeAllData=true)
class CfG_RollupTest extends CfG_BaseTest {
  //--------------------------------------------------------------------------
  // Tests
  static testMethod void testFields() {
    System.runAs(t.adminUser) {
      t.init();
      
      Account[] accounts = createAccounts('a');
      insert accounts;
      
      Asset[] assets = t.newAssets('a');
      for (Integer i = 0; i < assets.size(); ++i) {
        assets[i].AccountId = accounts[i].Id;
        assets[i].Price = 100 * (i + 1);
        assets[i].Quantity = 10 * (i + 1);
      }
      if (assets.size() > 2) {
        assets[2].AccountId = assets[1].AccountId;
      }
      insert assets;
      
      CfG_Rollup.FieldDef[] fieldDefs = new CfG_Rollup.FieldDef[] {};
      CfG_Rollup.Spec spec = new CfG_Rollup.Spec(fieldDefs, Asset.SObjectType, Asset.AccountId, Account.SObjectType, null);
      SObject[] records = new SObject[] {};
      
      t.start();
        SObject[] results = CfG_Rollup.fields(fieldDefs, records, Asset.SObjectType, Asset.AccountId, Account.SObjectType, null, null);
        System.assertEquals(0, results.size());
        
        // 1 record, 1 field
        if (!assets.isEmpty()) {
          records.add(assets[0]);
          fieldDefs.add(new CfG_Rollup.FieldDef(CfG_Rollup.Op.SUM, Asset.Price, Account.AnnualRevenue));
          results = CfG_Rollup.fields(fieldDefs, records, Asset.SObjectType, Asset.AccountId, Account.SObjectType, null, null);
          System.assertEquals(1, results.size());
          System.assertEquals(100, ((Account)results[0]).AnnualRevenue);
        }
        
        // 2 records on one parent, 2 fields
        if (assets.size() > 2) {
          records.clear();
          records.add(assets[1]);
          records.add(assets[2]);
          fieldDefs.add(new CfG_Rollup.FieldDef(CfG_Rollup.Op.COUNT, Asset.Id, Account.NumberOfEmployees));
          results = CfG_Rollup.fields(fieldDefs, records, Asset.SObjectType, Asset.AccountId, Account.SObjectType, null, null);
          System.assertEquals(1, results.size());
          System.assertEquals(500, ((Account)results[0]).AnnualRevenue);
          System.assertEquals(2, ((Account)results[0]).NumberOfEmployees);
        }
        
        // all records, n-1 parents, 2 fields
        if (assets.size() > 1) {
          records.add(assets[0]);
          results = CfG_Rollup.fields(fieldDefs, records, Asset.SObjectType, Asset.AccountId, Account.SObjectType, null, null);
          System.assertEquals(2, results.size());
          System.assertEquals(100, ((Account)results[0]).AnnualRevenue);
          System.assertEquals(500, ((Account)results[1]).AnnualRevenue);
          System.assertEquals(2, ((Account)results[1]).NumberOfEmployees);
        }
      t.stop();
    }
  }
  
  static testMethod void testSpecs() {
    System.runAs(t.adminUser) {
      t.init();
      
      Account[] accounts = createAccounts('a');
      insert accounts;
      
      Asset[] assets = t.newAssets('a');
      Decimal total = 0;
      for (Integer i = 0; i < assets.size(); ++i) {
        assets[i].AccountId = accounts[0].Id;
        assets[i].Price = 100 * (i + 1);
        assets[i].Quantity = 10 * (i + 1);
        total += assets[i].Price;
      }
      insert assets;
      
      CfG_Rollup.FieldDef[] fieldDefs = new CfG_Rollup.FieldDef[] {
        new CfG_Rollup.FieldDef(CfG_Rollup.Op.SUM, Asset.Price, Account.AnnualRevenue) };
      CfG_Rollup.Spec spec = new CfG_Rollup.Spec(fieldDefs, Asset.SObjectType, Asset.AccountId, Account.SObjectType, null);
      
      t.start();
        // all records, 1 parent, 1 field
        SObject[] results = CfG_Rollup.fields(spec, assets, new SObject[] {});
        if (!assets.isEmpty()) {
          System.assertEquals(1, results.size());
          System.assertEquals(total, ((Account)results[0]).AnnualRevenue);
        } else {
          System.assertEquals(0, results.size());
        }
        
        // use spec with parent records, and non-empty merge results
        if (accounts.size() > 2) {
          clearAccounts(accounts);
          results = CfG_Rollup.fields(spec, new Set<String>{accounts[0].Id, accounts[1].Id}, results);
          System.assertEquals(2, results.size());
          System.assertEquals(total, ((Account)results[0]).AnnualRevenue);
        }
      t.stop();
    }
  }
  
  static testMethod void testException() {
    System.runAs(t.adminUser) {
      t.init();
      
      Account[] accounts = createAccounts('a');
      insert accounts;
      
      CfG_Rollup.FieldDef[] fieldDefs = new CfG_Rollup.FieldDef[] {
        new CfG_Rollup.FieldDef(CfG_Rollup.Op.SUM, Asset.Price, Account.AnnualRevenue) };
      CfG_Rollup.Spec spec = new CfG_Rollup.Spec(fieldDefs, Asset.SObjectType, Asset.AccountId, Account.SObjectType, 'junk');
      
      t.start();
        try {
          CfG_Rollup.fields(spec, new Set<String>{ accounts[0].Id }, new SObject[]{});
          System.assert(false, 'invalid filter');
        } catch (Exception e) {
        }
      t.stop();
    }
  }
  
  static testMethod void testRollupUpdateBadParams() {
    System.runAs(t.adminUser) {
      t.init();
      
      CfG_Rollup.FieldDef[] defs = new CfG_Rollup.FieldDef[] {};
      
      t.start();
        try {
          new CfG_RollupUpdate(new CfG_Rollup.Spec[] {
            new CfG_Rollup.Spec(defs, Asset.SObjectType, Asset.AccountId, Account.SObjectType, null),
            new CfG_Rollup.Spec(defs, Asset.SObjectType, Asset.AccountId, CfG__c.SObjectType, null)
          });
          System.assert(false, 'multiple parent objects not allowed in 1 update');
        } catch (Exception e) {
        }
      t.stop();
    }
  }
  
  static testMethod void testRollupUpdate() {
    System.runAs(t.adminUser) {
      t.init();
      
      Account[] accounts = createAccounts('a');
      insert accounts;
      
      Decimal total = 0;
      Asset[] assets = t.newAssets('a');
      for (Integer i = 0; i < assets.size(); ++i) {
        assets[i].AccountId = accounts[0].Id;
        assets[i].Price = 100 * (i + 1);
        total += assets[i].Price;
      }
      insert assets;
      
      t.start();
        Database.executeBatch(new CfG_RollupUpdate(new CfG_Rollup.Spec[] {
          new CfG_Rollup.Spec(new CfG_Rollup.FieldDef[] {
            new CfG_Rollup.FieldDef(CfG_Rollup.Op.SUM, Asset.Price, Account.AnnualRevenue) },
            Asset.SObjectType, Asset.AccountId, Account.SObjectType, null)
        }));
      t.stop();
      
      if (!accounts.isEmpty()) {
        System.assertEquals(total, [SELECT AnnualRevenue FROM Account WHERE Id = :accounts[0].Id].AnnualRevenue);
      }
    }
  }
  
  
  //--------------------------------------------------------------------------
  // Helpers
  private static void clearAccounts(Account[] accounts) {
    for (Account a : accounts) {
      a.AnnualRevenue = 0;
      a.NumberOfEmployees = 0;
    }
    update accounts;
  }
  
  private static Account[] createAccounts(String Name) {
    RecordType[] rts = [SELECT Id FROM RecordType WHERE DeveloperName = 'Agency_Location' AND isActive = true AND SObjectType = 'Account' LIMIT 1];
    System.assert(!rts.isEmpty(), 'Should exist recordType for Account with dev name: Agency_Location');
    
    Account[] accounts = t.newAccounts(Name);
    for (Account account : accounts) {
      account.RecordTypeId = rts[0].Id;
    }
    
    return accounts;
  }
  
  static { CfG_BaseTest.t = new CfG_RollupTest(); }
  static CfG_BaseTest t { get { return CfG_BaseTest.t; } }
}