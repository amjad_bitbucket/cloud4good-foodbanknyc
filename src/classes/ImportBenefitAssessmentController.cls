public with sharing class ImportBenefitAssessmentController {
  public Boolean IsFileUploaded { get; private set; }
  public Boolean IsFileProccessed { get; private set; }
  public Boolean IsWrongFileUploaded { get; private set; }

  public Blob csvFileBody { get; set; }
  public string csvFileName{ get; set; }
  public string csvAsString { get; set; }
  public List<String> csvFileLines { get; set; }
  public List<CSVRow> CSVRows { get; set; }

  private Set<String> FirstNames { get; set; }
  private Set<String> LastNames { get; set; }
  private Set<Date> DOBs { get; set; }

  private List<Client__c> Clients { get; set; }
  private List<Program_Relationship__c> Relationships { get; set; }
  private List<Benefits_Assessment__c> Assessments { get; set; }
  private List<Household_Member__c> HouseholdMembers { get; set; }

  public Boolean IsImportButtonAvailable {
    get {
      return !isFileProccessed && !IsWrongFileUploaded;
    }
  }

  public ImportBenefitAssessmentController() {
    csvFileLines = new List<String> ();
    IsFileUploaded = false;
    isFileProccessed = false;
    IsWrongFileUploaded = false;
  }

  public void import() {
    try {
      IsWrongFileUploaded = false;
      IsFileUploaded = true;
      csvAsString = csvFileBody.toString();
      csvAsString = csvAsString.replace('\r','\n');
      csvAsString = csvAsString.replace('\n\n','\n');
      csvFileLines = csvAsString.split('\n');
      if (csvFileLines.size() <= 1)
      {
        ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR, 'At least header and one record should be present in file.');
        ApexPages.addMessage(errorMessage);
        return;
      }

      if (csvFileLines.size() > 1000)
      {
        ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR, 'File should not have more then 1000 records.');
        ApexPages.addMessage(errorMessage);
        return;
      }

      String header = csvFileLines.get(0) + csvFileLines.get(1);
      List<Integer> missingColumns = validateHeader(header);
      if (missingColumns.size() != 0)
      {
        for (Integer column : missingColumns) {
          ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR, 'Column number ' + (column + 1) + ' header is missing from the file.');
          ApexPages.addMessage(errorMessage);
          return;
        }
      }

      ParseCSVValues();
    }
    catch(Exception e) {
      ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR, 'An error has occured while importing data. Please make sure input csv file is correct.');
      ApexPages.addMessage(errorMessage);
      IsWrongFileUploaded = true;
    }
  }

  private void ParseCSVValues() {
    csvFileLines.remove(0);
    csvFileLines.remove(0);

    CSVRows = new List<CSVRow> ();
    FirstNames = new Set<String> ();
    LastNames = new Set<String> ();
    DOBs = new Set<Date> ();

    Integer rowNumber = 1;
    for (String s : csvFileLines) {
      String check = s;
      check = check.replace(',', '').trim();

      if (s != '' && check != '')
      {
        CSVRow row = new CSVRow(s, rowNumber);
        CSVRows.add(row);
        FirstNames.add(row.Client.First_Name__c);
        LastNames.add(row.Client.Last_Name__c);
        DOBs.add(row.Client.Date_of_Birth__c);
        rowNumber++;
      }
    }

    if (CSVRows.size() > 0)
    {
      processCSVRows();
    }
  }

  private List<Integer> validateHeader(String header) {
    List<Integer> missingColumns = new List<Integer> ();
    List<String> headerColumns = header.split(',', -1);
    for (Integer i = 0; i <headerColumns.size(); i++) {
      if (headerColumns.get(i).trim() == '') {
        missingColumns.Add(i);
      }
    }
    return missingColumns;
  }

  private void processCSVRows()
  {
    Savepoint sp = Database.setSavepoint();

    try {

      Clients = [Select Id, First_Name__c, Last_Name__c, Date_of_Birth__c FROM Client__c WHERE First_Name__c IN :FirstNames AND Last_Name__c IN :LastNames AND Date_of_Birth__c IN :DOBs];
      Set<Id> clientIds = (new Map<Id,Client__c> (clients)).keySet();

      Id benefitsId = [SELECT Id, SobjectType, Name FROM RecordType WHERE Name = 'Benefits' AND SobjectType = 'Program_Relationship__c' LIMIT 1].Id;

      Relationships = [SELECT Id, Location_of_Screening__c, Date__c, Client__c FROM Program_Relationship__c WHERE Client__c IN :clientIds AND RecordTypeId = :benefitsId];
      Set<Id> relationshipIds = (new Map<Id,Program_Relationship__c> (relationships)).keySet();

      Assessments = [SELECT Id, Program_Relationship__c, cfg_Prescreener__c, cfg_Date_of_Assessment__c FROM Benefits_Assessment__c WHERE Program_Relationship__c IN :relationshipIds];
      Set<Id> assessmentsIds = (new Map<Id,Benefits_Assessment__c> (assessments)).keySet();

      HouseholdMembers = [SELECT Id, Date_of_Birth__c, First_Name__c, Last_Name__c, cfg_Benefits_Assessment__c FROM Household_Member__c WHERE cfg_Benefits_Assessment__c IN :assessmentsIds];

      ProcessClients();
      upsert clients;
      ProcessRelationships();
      upsert relationships;
      ProcessAssessments();
      upsert assessments;
      ProcessHouseholdMembers();
      upsert householdMembers;
        
      isFileProccessed = true;
    }
    catch(DMLexception e) {
      for (Integer i = 0; i <e.getNumDml(); i++) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
      }
      Database.rollback(sp);
    }
    catch(Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
      Database.rollback(sp);
    }
  }

  private void ProcessClients() {
    for (CSVRow row : CSVRows) {
      if (!CheckClientExist(row.Client)) {
        Clients.add(row.Client);
      }
    }
  }

  private void ProcessRelationships() {
    for (CSVRow row : CSVRows) {
      Program_Relationship__c relationship = FindExistingProgramRelationship(row);
      if (relationship != null) {
        UpdateRelationship(relationship, row.Relationship);
      }
      else {
        row.Relationship.Client__c = row.Client.Id;
        Relationships.add(row.Relationship);
      }
    }
  }

  private void ProcessAssessments() {
    for (CSVRow row : CSVRows) {
      Benefits_Assessment__c assessment = FindExistingBenefitsAssessment(row);
      if (assessment != null) {
        UpdateBenefitsAssessment(assessment, row.Assessment);
      }
      else {
        row.Assessment.Program_Relationship__c = row.Relationship.Id;
        Assessments.add(row.Assessment);
      }
    }
  }

  private void ProcessHouseholdMembers() {
    for (CSVRow row : CSVRows) {
      for (Household_Member__c member : row.HouseholdMembers) {
        Household_Member__c mem = FindExistingMember(member, row.Assessment);
        if (mem != null) {
          UpdateHouseholdMember(mem, member);
        }
        else {
          member.cfg_Benefits_Assessment__c = row.Assessment.Id;
          HouseholdMembers.add(member);
        }
      }
    }
  }

  private Boolean CheckClientExist(Client__c client) {
    for (Client__c c : Clients) {
      if (c.First_Name__c == client.First_Name__c && c.Last_Name__c == client.Last_Name__c && c.Date_of_Birth__c == client.Date_of_Birth__c) {
        client.Id = c.Id;
        return true;
      }
    }

    return false;
  }

  private Program_Relationship__c FindExistingProgramRelationship(CSVRow row) {
    for (Program_Relationship__c r : Relationships) {
      if (r.Location_of_Screening__c == row.Relationship.Location_of_Screening__c && r.Date__c == row.Relationship.Date__c && r.Client__c == row.Client.Id) {
        row.Relationship.Id = r.Id;
          return r;
      }
    }

    return null;
  }

  private void UpdateRelationship(Program_Relationship__c relationshipToUpdate, Program_Relationship__c relationship) {
    relationshipToUpdate.Location_of_Screening__c = relationship.Location_of_Screening__c;
    relationshipToUpdate.How_did_you_hear_of_us__c = relationship.How_did_you_hear_of_us__c;
    relationshipToUpdate.Date__c = relationship.Date__c;
  }

  private Benefits_Assessment__c FindExistingBenefitsAssessment(CSVRow row) {
    for (Benefits_Assessment__c a : Assessments) {
      if (a.cfg_Prescreener__c == row.Assessment.cfg_Prescreener__c && a.cfg_Date_of_Assessment__c == row.Assessment.cfg_Date_of_Assessment__c && a.Program_Relationship__c == row.Relationship.Id) {
        row.Assessment.Id = a.Id;
          return a;
      }
    }

    return null;
  }

  private void UpdateBenefitsAssessment(Benefits_Assessment__c assessmentToUpdate, Benefits_Assessment__c assessment) {
    assessmentToUpdate.cfg_Date_of_Assessment__c = assessment.cfg_Date_of_Assessment__c;
    assessmentToUpdate.cfg_Prescreener__c = assessment.cfg_Prescreener__c;
    assessmentToUpdate.cfg_Assistance_type__c = assessment.cfg_Assistance_type__c;
    assessmentToUpdate.cfg_Homeless_Household__c = assessment.cfg_Homeless_Household__c;
    assessmentToUpdate.cfg_Disqualified_Member_SNAP_IPV__c = assessment.cfg_Disqualified_Member_SNAP_IPV__c;
    assessmentToUpdate.cfg_Any_Member_Disabled_Elderly__c = assessment.cfg_Any_Member_Disabled_Elderly__c;
    assessmentToUpdate.cfg_Num_of_people_in_the_SNAP_household__c = assessment.cfg_Num_of_people_in_the_SNAP_household__c;
    assessmentToUpdate.cfg_Num_ineligible_immigrants_in_hh__c = assessment.cfg_Num_ineligible_immigrants_in_hh__c;
    assessmentToUpdate.cfg_HH_Total_Monthly_Gross_Income__c = assessment.cfg_HH_Total_Monthly_Gross_Income__c;
    assessmentToUpdate.cgf_Monthly_Unearned_Cash_Assistance__c = assessment.cgf_Monthly_Unearned_Cash_Assistance__c;
    assessmentToUpdate.cfg_Monthly_Unearned_SSI__c = assessment.cfg_Monthly_Unearned_SSI__c;
    assessmentToUpdate.cfg_Monthly_Unearned_Other_Income__c = assessment.cfg_Monthly_Unearned_Other_Income__c;
    assessmentToUpdate.cfg_Amount_of_Child_Support__c = assessment.cfg_Amount_of_Child_Support__c;
    assessmentToUpdate.cfg_Total_Rent_Mortgage__c = assessment.cfg_Total_Rent_Mortgage__c;
    assessmentToUpdate.Screening_Outcome__c = assessment.Screening_Outcome__c;
    assessmentToUpdate.cfg_Out_of_pock_Child_Depend_Care_costs__c = assessment.cfg_Out_of_pock_Child_Depend_Care_costs__c;
    assessmentToUpdate.cfg_Unreimb_Medical_Elderly_Disabled__c = assessment.cfg_Unreimb_Medical_Elderly_Disabled__c;
    assessmentToUpdate.cfg_Amount_of_Child_Dependent_Care_Costs__c = assessment.cfg_Amount_of_Child_Dependent_Care_Costs__c;
  }

  private Household_Member__c FindExistingMember(Household_Member__c member, Benefits_Assessment__c assessment) {
    for (Household_Member__c m : HouseholdMembers) {
      if (m.First_Name__c == member.First_Name__c && m.Last_Name__c == member.Last_Name__c && m.Date_of_Birth__c == member.Date_of_Birth__c && m.cfg_Benefits_Assessment__c == assessment.Id) {
        member.Id = m.Id;
          return m;
      }
    }

    return null;
  }

  private void UpdateHouseholdMember(Household_Member__c existingMember, Household_Member__c member) {
    existingMember.Relationship__c = member.Relationship__c;
    existingMember.Citizen_Status__c = member.Citizen_Status__c;
    existingMember.Type_of_Income__c = member.Type_of_Income__c;
    existingMember.Student__c = member.Student__c;
  }

  public class CSVRow {
    public Client__c Client { get; set; }
    public Program_Relationship__c Relationship { get; set; }
    public Benefits_Assessment__c Assessment { get; set; }
    public List<Household_Member__c> HouseholdMembers { get; set; }
    public Integer RowNumber { get; set; }
    List<String> Columns {get;set;}

    public CSVRow(String rowToParse, Integer numb) {
      RowNumber = numb;

      Columns = ParseRow(rowToParse);

      Client = ParseClient();
      Relationship = ParseProgramRelationship();
      Assessment = ParseBenefitsAssesment();
      HouseholdMembers = ParseHouseHoldMembers();
    }

    private Client__c ParseClient() {
      Client__c client = new Client__c();
      client.First_Name__c = columns.get(13);
      client.Last_Name__c = columns.get(14);
      Object dob = convertColumnValue(Schema.DisplayType.DATE, 17);
      if(dob != null) {
        client.Date_of_Birth__c = (Date)dob;
      }
      client.Primary_Language__c = columns.get(6);
      client.Ethnicity__c = columns.get(7);
      client.Gender__c = columns.get(8);
      client.Zip_Code__c = columns.get(11);
      client.Phone__c = columns.get(12);

      return client;
    }

    private Program_Relationship__c ParseProgramRelationship() {
      Program_Relationship__c relationship = new Program_Relationship__c();
      relationship.Location_of_Screening__c = columns.get(4);
      relationship.How_did_you_hear_of_us__c = columns.get(5) + ' \n' + columns.get(10);
      Object relationshipDate = convertColumnValue(Schema.DisplayType.DATE, 1);
      if(relationshipDate != null) {
        relationship.Date__c = (Date)relationshipDate;
      }
      relationship.RecordTypeId = [SELECT Id, SobjectType, Name FROM RecordType WHERE Name = 'Benefits' AND SobjectType = 'Program_Relationship__c' LIMIT 1].Id;
      relationship.cfg_BA_Screening_Outcome__c = columns.get(84);
        return relationship;
    }

    private Benefits_Assessment__c ParseBenefitsAssesment() {
      Benefits_Assessment__c assessment = new Benefits_Assessment__c();
      Object assessmentdate = convertColumnValue(Schema.DisplayType.DATE, 1);
      if(assessmentdate != null) {
      	assessment.cfg_Date_of_Assessment__c = (Date)assessmentdate;
      }
      User[] prescreener = [SELECT Id FROM User WHERE Name = :columns.get(2)];
      if (prescreener.size() != 0) {
        assessment.cfg_Prescreener__c = prescreener.get(0).Id;
      }
      assessment.cfg_Assistance_type__c = columns.get(3);
      assessment.cfg_Homeless_Household__c = columns.get(69);
      assessment.cfg_Disqualified_Member_SNAP_IPV__c = columns.get(70);
      assessment.cfg_Any_Member_Disabled_Elderly__c = columns.get(71);
      assessment.cfg_Out_of_pock_Child_Depend_Care_costs__c = columns.get(72);
      Object Num_of_people_in_the_SNAP_household = convertColumnValue(Schema.DisplayType.CURRENCY, 73);
      if(Num_of_people_in_the_SNAP_household != null) {
       assessment.cfg_Num_of_people_in_the_SNAP_household__c = (Decimal)Num_of_people_in_the_SNAP_household;
      }
      Object Num_ineligible_immigrants_in_hh = convertColumnValue(Schema.DisplayType.CURRENCY, 74);
      if(Num_ineligible_immigrants_in_hh != null) {
        assessment.cfg_Num_ineligible_immigrants_in_hh__c = (Decimal)Num_ineligible_immigrants_in_hh;
      }
      Object HH_Total_Monthly_Gross_Income = convertColumnValue(Schema.DisplayType.CURRENCY, 75);
      if(HH_Total_Monthly_Gross_Income != null) {
        assessment.cfg_HH_Total_Monthly_Gross_Income__c = (Decimal)HH_Total_Monthly_Gross_Income;
      }
      Object Monthly_Unearned_Cash_Assistance = convertColumnValue(Schema.DisplayType.CURRENCY, 76);
      if(Monthly_Unearned_Cash_Assistance != null) {
        assessment.cgf_Monthly_Unearned_Cash_Assistance__c = (Decimal)Monthly_Unearned_Cash_Assistance;
      }
      Object Monthly_Unearned_SSI = convertColumnValue(Schema.DisplayType.CURRENCY, 77);
      if(Monthly_Unearned_SSI != null) {
        assessment.cfg_Monthly_Unearned_SSI__c = (Decimal)Monthly_Unearned_SSI;
      }
      Object Monthly_Unearned_Other_Income = convertColumnValue(Schema.DisplayType.CURRENCY, 78);
      if(Monthly_Unearned_Other_Income != null) {
        assessment.cfg_Monthly_Unearned_Other_Income__c = (Decimal)Monthly_Unearned_Other_Income;
      }
      Object Amount_of_Child_Support = convertColumnValue(Schema.DisplayType.CURRENCY, 79);
      if(Amount_of_Child_Support != null) {
        assessment.cfg_Amount_of_Child_Support__c = (Decimal)Amount_of_Child_Support;
      }
      Object Unreimb_Medical_Elderly_Disabled = convertColumnValue(Schema.DisplayType.CURRENCY, 80);
      if(Unreimb_Medical_Elderly_Disabled != null) {
        assessment.cfg_Unreimb_Medical_Elderly_Disabled__c = (Decimal)Unreimb_Medical_Elderly_Disabled;
      }
      Object Amount_of_Child_Dependent_Care_Costs = convertColumnValue(Schema.DisplayType.CURRENCY, 81);
      if(Amount_of_Child_Dependent_Care_Costs != null) {
        assessment.cfg_Amount_of_Child_Dependent_Care_Costs__c = (Decimal)Amount_of_Child_Dependent_Care_Costs;
      }
      Object Total_Rent_Mortgage = convertColumnValue(Schema.DisplayType.CURRENCY, 82);
      if(Total_Rent_Mortgage != null) {
        assessment.cfg_Total_Rent_Mortgage__c = (Decimal)Total_Rent_Mortgage;
      }
      
      return assessment;
    }

    private List<Household_Member__c> ParseHouseHoldMembers() {
      List<Household_Member__c> members = new List<Household_Member__c> ();
      Household_Member__c member1 = ParseHouseholdMember(21);
      if (member1 != null) {
        members.Add(member1);
      }

      Household_Member__c member2 = ParseHouseholdMember(29);
      if (member2 != null) {
        members.Add(member2);
      }

      Household_Member__c member3 = ParseHouseholdMember(37);
      if (member3 != null) {
        members.Add(member3);
      }

      Household_Member__c member4 = ParseHouseholdMember(45);
      if (member4 != null) {
        members.Add(member4);
      }

      Household_Member__c member5 = ParseHouseholdMember(53);
      if (member5 != null) {
        members.Add(member5);
      }

      Household_Member__c member6 = ParseHouseholdMember(61);
      if (member6 != null) {
        members.Add(member6);
      }

      return members;
    }

    private List<String> ParseRow(String rowToParse) {
      rowToParse = rowToParse.replaceAll(',"""', ',"DBLQT').replaceall('""",', 'DBLQT",');
      rowToParse = rowToParse.replaceAll('""', 'DBLQT');

      List<String> columns = rowToParse.split(',');
      List<String> cleancolumns = new List<String> ();
      String compositecolumn;
      Boolean makeCompositecolumn = false;
      for (String column : columns) {
        if (column.startsWith('"') && column.endsWith('"')) {
          cleancolumns.add(column.replaceAll('DBLQT', '"'));
        } else if (column.startsWith('"')) {
          makeCompositecolumn = true;
          compositecolumn = column;
        } else if (column.endsWith('"')) {
          compositecolumn += ',' + column;
          cleancolumns.add(compositecolumn.replaceAll('DBLQT', '"'));
          makeCompositecolumn = false;
        } else if (makeCompositecolumn) {
          compositecolumn += ',' + column;
        } else {
          cleancolumns.add(column.replaceAll('DBLQT', '"'));
        }
      }

      return cleancolumns;
    }

    private Household_Member__c ParseHouseholdMember(Integer nextColumnNumber) {
      String memberName = columns.get(nextColumnNumber).Trim();
      if (!IsEmptyOrBlank(memberName)) {
        Household_Member__c member = new Household_Member__c();
        member.First_Name__c = columns.get(nextColumnNumber);
        member.Last_Name__c = columns.get(nextColumnNumber + 1);
        member.Relationship__c = columns.get(nextColumnNumber + 2);
        member.Citizen_Status__c = columns.get(nextColumnNumber + 3);
        Object dob = convertColumnValue(Schema.DisplayType.DATE, nextColumnNumber + 4);
        if(dob != null){
          member.Date_of_Birth__c = (Date)dob;
        }
        member.Type_of_Income__c = columns.get(nextColumnNumber + 6);
        member.Student__c = columns.get(nextColumnNumber + 7);
        return member;
      }

      return null;
    }

    private Boolean IsEmptyOrBlank(String value) {
	    return value == null || value == '' || value == '1/0/00' || value == '1/0/1900';
    }

    private Object convertColumnValue(Schema.DisplayType fieldType, Integer columnNumber){
      return convertValue(fieldType, columns.get(columnNumber));
    }

    private Object convertValue(Schema.DisplayType fieldType, Object data) {
    Object result;
    
    String value = String.valueOf(data);
    try {
      if (fieldType == Schema.DisplayType.DATETIME) {
        result = (!IsEmptyOrBlank(value)) ? DateTime.parse(value) : null;
      } else if (fieldType == Schema.DisplayType.DATE) {
        result = (!IsEmptyOrBlank(value)) ? Date.parse(value) : null;
      } else if (fieldType == Schema.DisplayType.BOOLEAN) {
        result = (!IsEmptyOrBlank(value)) ? Boolean.valueOf(value) : null;
      } else if (fieldType == Schema.DisplayType.INTEGER) {
        result = (!IsEmptyOrBlank(value)) ? Integer.valueOf(value) : null;
      } else if ((fieldType == Schema.DisplayType.DOUBLE) ||
                 (fieldType == Schema.DisplayType.PERCENT)) {
        result = (!IsEmptyOrBlank(value)) ? Double.valueOf(value) : null;
      } else if (fieldType == Schema.DisplayType.CURRENCY) {
        result = (!IsEmptyOrBlank(value)) ? Decimal.valueOf(value) : null;
      } else if ((fieldType == Schema.DisplayType.STRING) ||
                 (fieldType == Schema.DisplayType.TEXTAREA) ||
                 (fieldType == Schema.DisplayType.PICKLIST) ||
                 (fieldType == Schema.DisplayType.ID) ||
                 (fieldType == Schema.DisplayType.PHONE) ||
                 (fieldType == Schema.DisplayType.EMAIL) ||
                 (fieldType == Schema.DisplayType.URL)) {
                 result = value;
      }
	} catch (Exception ex) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
    }

    return result;
  }
  }
}