/**
 * This class should be extended by all test classes. It contains utilities and
 * methods for use in unit tests
 * @author Steve Cox
 */
public virtual class CfG_BaseTest {
  //--------------------------------------------------------------------------
  // Constants
  private Decimal BULK_TEST_SIZE;
  private static final String SYS_ADMIN_ROLE = 'System_Administrator';
  
  
  //--------------------------------------------------------------------------
  // Properties
  /** Initialize this to your new test class, then use it to access properties & methods */
  public static CfG_BaseTest t;
  
  /** use this in conjunction with throwIfTesting to get coverage for catch blocks */
  public static Boolean testException = false;
  
  /** a default user to use in System.runAs() */
  public User adminUser {
    get {
      if (null == adminUser) {
        // all test code should execute under a user we can control so as to avoid
        // surprises when deploying to different environments.
        UserRole[] roles = [SELECT Id FROM UserRole WHERE DeveloperName = :SYS_ADMIN_ROLE];
        if (roles.isEmpty()) {
          roles.add(new UserRole(DeveloperName = SYS_ADMIN_ROLE, Name = 'r0'));
          insert roles;
        }
        
        adminUser = newUser('admin@cfg.com');
        adminUser.UserRoleId = roles[0].Id;
        insert adminUser;
      }
      return adminUser;
    }
    private set;
  }
  
  /** allows get/set of the global 'debugging' flag */
  public Boolean debugging {
    get {
      return CfG__c.getInstance().Debugging__c;
    }
    set {
      CfG__c prefs = CfG__c.getInstance();
      if (prefs.Debugging__c != value) {
        prefs.Debugging__c = value;
        upsert prefs;
      }
    }
  }
  
  
  //--------------------------------------------------------------------------
  // Methods
  /**
   * unit test initialization
   * <p>
   * Put your global initializtion within this method and provide overrides
   * in classes that require additional initialization.
   */
  public virtual void init() {
    // set preferences to testing defaults
    CfG__c prefs = (null != CfG__c.getInstance())? CfG__c.getInstance() :
      // create a record with default field values
      (CfG__c)CfG__c.sObjectType.newSObject(null, true);
    
    prefs.Log_Recipients__c = '1@cfg.com,2@cfg.com';
    // for some reason, the default (3) isn't installed with the package
    if (null == prefs.Bulk_Test_Size__c) {
      prefs.Bulk_Test_Size__c = 3;
    }
    upsert prefs;
    
    debugging = true; // start all tests with this assumption
    BULK_TEST_SIZE = Math.max(1, prefs.Bulk_Test_Size__c);
    
    // in a managed package each unit test must include an assertion; otherwise,
    // the Force.com security source scanner will complain
    System.assert(true);
  }
  
  /** use this instead of Test.startTest() to provide extra functionality */
  public void start() {
    Test.startTest();
  }
  
  /** use this instead of Test.stopTest() to provide extra functionality */
  public void stop() {
    Test.stopTest();
  }
  
  /** put this call in code to allow coverage of catch blocks */
  public static void throwIfTesting(Exception ex) {
    if (Test.isRunningTest() && testException) {
      throw ex;
    }
  }
  
  
  //--------------------------------------------------------------------------
  // Methods for creating SObjects
  public Account[] newAccounts(String name) {
    return this.newSObjects(Account.SObjectType, name);
  }
  
  public Asset[] newAssets(String name) {
    return this.newSObjects(Asset.SObjectType, name);
  }
  
  public User newUser(String username) {
    final Id profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
    return newUser(username, profileId);
  }
  
  public CfG_Log__c[] newLogs(String name) {
    CfG_Log__c[] result = this.newSObjects(CfG_Log__c.SObjectType, name);
    for (CfG_Log__c o : result) {
      o.Text__c = 'text';
    }
    
    return result;
  }
  
  public SObject[] newSObjects(SObjectType type, String name) {
    SObject[] result = new SObject[]{};
    for (Integer i = 0; i < BULK_TEST_SIZE; ++i) {
      // create a record with default field values
      SObject o = type.newSObject(null, true);
      if (null != name) {
        o.put('Name', name + i);
      }
      result.add(o);
    }
    
    return result;
  }
  
  
  //--------------------------------------------------------------------------
  // Helpers
  private User newUser(String username, Id profileId) {
    return new User(
      ProfileId = [SELECT Id FROM Profile WHERE Id = :profileId LIMIT 1].Id,
      LastName = 'last',
      Email = 'user@cfg.com',
      Username = username + System.currentTimeMillis(),
      CompanyName = 'CfG',
      Title = 'title',
      Alias = 'alias',
      TimeZoneSidKey = 'America/Los_Angeles',
      EmailEncodingKey = 'UTF-8',
      LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US'
    );
  }
}