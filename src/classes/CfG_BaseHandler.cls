/**
 * Base implementation of CfG_ITrigger. Derive your handler from this class, then
 * override methods as needed.
 *
 * @author Steve Cox
 */
public virtual class CfG_BaseHandler {
  //--------------------------------------------------------------------------
  // Properties
  // has this handler already been called within this execution context?
  protected Boolean handled = false;
  
  public SObject[] toInsert = new SObject[]{};
  private Map<Id,SObject> toUpdate = new Map<Id,SObject>();
  private Map<Id,SObject> toDelete = new Map<Id,SObject>();
  
  
  //--------------------------------------------------------------------------
  // Methods
  public virtual void bulkBefore() { CfG.preCondition(Trigger.isBefore, ''); }
  public virtual void bulkAfter() { CfG.preCondition(Trigger.isAfter, ''); }
  public virtual void beforeInsert(SObject so) { CfG.preCondition(Trigger.isBefore && Trigger.isInsert, ''); }
  public virtual void beforeUpdate(SObject oldSo, SObject so) { CfG.preCondition(Trigger.isBefore && Trigger.isUpdate, ''); }
  public virtual void beforeDelete(SObject so) { CfG.preCondition(Trigger.isBefore && Trigger.isDelete, ''); }
  public virtual void afterInsert(SObject so) { CfG.preCondition(Trigger.isAfter && Trigger.isInsert, ''); }
  public virtual void afterUpdate(SObject oldSo, SObject so) { CfG.preCondition(Trigger.isAfter && Trigger.isUpdate, ''); }
  public virtual void afterDelete(SObject so) { CfG.preCondition(Trigger.isAfter && Trigger.isDelete, ''); }
  public virtual void afterUndelete(SObject so) { CfG.preCondition(Trigger.isAfter && Trigger.isUndelete, ''); }
  
  /** insert, update, and delete the processed records */
  public virtual void andFinally() {
    insert toInsert;
    update toUpdate.values();
    delete toDelete.values();
  }
  
  /** sets the 'handled' property for this trigger. */
  public void setHandled() {
    handled = true;
  }
  
  /**
   * get the object with Id 'i' from the 'toUpdate' list. If the object isn't
   * found, a new one of the specified type is created. This is needed when
   * several functions modify the same object within a handler. The second function
   * calls this so as to not override the changes the first function made.
   */
  public SObject getUpdate(Id i, SObjectType type) {
    return getObject(i, toUpdate, type);
  }
  
  public SObject getDelete(Id i, SObjectType type) {
    return getObject(i, toDelete, type);
  }
  
  
  //--------------------------------------------------------------------------
  // Helpers
  private SObject getObject(Id i, Map<Id,SObject> objects, SObjectType type) {
    if (!objects.containsKey(i)) {
      objects.put(i, type.newSObject(i));
    }
    
    return objects.get(i);
  }
}