@IsTest(SeeAllData=true)
class CfG_TestJobQueuerTest extends CfG_BaseTest {
  //--------------------------------------------------------------------------
  // Tests
  static testMethod void test() {
    System.runAs(t.adminUser) {
      t.init();
      t.start();
        CfG_TestJobQueuer q = new CfG_TestJobQueuer();
        q.execute(null);
      t.stop();
    }
  }
  
  //--------------------------------------------------------------------------
  // Helpers
  static { CfG_BaseTest.t = new CfG_TestJobQueuerTest(); }
  static CfG_BaseTest t { get { return CfG_BaseTest.t; } }
}