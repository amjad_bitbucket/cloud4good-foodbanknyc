trigger ValidateProgramNotNullWhenIncTypeEnrollment on Opportunity (before insert, before update) {
  if(Trigger.new[0].Income_Type__c == 'Enrollment' && Trigger.new[0].CookShop_Program__c == null){
    Trigger.new[0].CookShop_Program__c.addError('You must enter a value');
  }
}