trigger Food_Pantry_Visit on Food_Pantry_Visit__c(after delete, after insert, after undelete,
  after update, before delete, before insert, before update) {
  CfG_TriggerFactory.handle(Food_Pantry_Visit__c.SObjectType);
}