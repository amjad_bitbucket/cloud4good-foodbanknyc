trigger Program_Relationship on Program_Relationship__c(after delete, after insert, after undelete,
  after update, before delete, before insert, before update) {
  CfG_TriggerFactory.handle(Program_Relationship__c.SObjectType);
}