<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Progress_Report_Name</fullName>
        <description>Updates name to formula</description>
        <field>Name</field>
        <formula>LEFT(School_Enrollment__r.cfg_School__r.School_Name__c, 18)&amp;&quot;-&quot;&amp; TEXT(Progress_Report_Type__c)</formula>
        <name>Update Progress Report Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Name</fullName>
        <actions>
            <name>Update_Progress_Report_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Progress_Report__c.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Updates the progress report name</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
