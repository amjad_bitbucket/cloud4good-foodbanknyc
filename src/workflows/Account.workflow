<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>HOC__New_Organization_Registered</fullName>
        <description>New Organization Registered</description>
        <protected>false</protected>
        <recipients>
            <recipient>PartnerApprovalManager</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HOC__HOC_Email_Templates/HOC__New_Organization_Registered</template>
    </alerts>
    <alerts>
        <fullName>HOC__Organizational_profile_updated_notification</fullName>
        <description>Organizational profile updated notification</description>
        <protected>false</protected>
        <recipients>
            <field>HOC__Opportunity_Approval_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HOC__HOC_Email_Templates/HOC__Organizational_profile_updated_notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>HOC__Upd_Account_PrimaryStreet</fullName>
        <field>HOC__Primary_Street__c</field>
        <formula>IF( ISPICKVAL( HOC__Primary_Address__c ,&apos;Billing&apos;) ,  BillingStreet  ,  IF(ISPICKVAL( HOC__Primary_Address__c ,&apos;Shipping&apos;), ShippingStreet, &apos;&apos;))</formula>
        <name>Upd_Account_PrimaryStreet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HOC__Update_Acc_PrimaryCity</fullName>
        <field>HOC__Primary_City__c</field>
        <formula>IF( ISPICKVAL( HOC__Primary_Address__c ,&apos;Billing&apos;) , BillingCity , IF(ISPICKVAL( HOC__Primary_Address__c ,&apos;Shipping&apos;), ShippingCity, &apos;&apos;))</formula>
        <name>Update_Acc_PrimaryCity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HOC__Update_Acc_PrimaryState</fullName>
        <field>HOC__Primary_State__c</field>
        <formula>IF( ISPICKVAL( HOC__Primary_Address__c ,&apos;Billing&apos;) , BillingState , IF(ISPICKVAL( HOC__Primary_Address__c ,&apos;Shipping&apos;), ShippingState, &apos;&apos;))</formula>
        <name>Update_Acc_PrimaryState</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HOC__Update_Acc_Zip</fullName>
        <field>HOC__Primary_Zip__c</field>
        <formula>IF( ISPICKVAL( HOC__Primary_Address__c ,&apos;Billing&apos;) , BillingPostalCode  , IF(ISPICKVAL( HOC__Primary_Address__c ,&apos;Shipping&apos;), ShippingPostalCode , &apos;&apos;))</formula>
        <name>Update_Acc_Zip</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HOC__Update_Approval_Manager_Email</fullName>
        <field>HOC__Opportunity_Approval_Manager_Email__c</field>
        <formula>HOC__Opportunity_Approval_Manager__r.Email</formula>
        <name>Update Approval Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HOC__Update_Website_Field</fullName>
        <field>Website</field>
        <formula>&apos;http://&apos; +  Website</formula>
        <name>Update Website Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Meal_Gap_Level_to_H</fullName>
        <field>cfg_Meal_Gap__c</field>
        <literalValue>High</literalValue>
        <name>Set Meal Gap Level to H</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Meal_Gap_Level_to_Low</fullName>
        <field>cfg_Meal_Gap__c</field>
        <literalValue>Low</literalValue>
        <name>Set Meal Gap Level to Low</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Meal_Gap_Level_to_M</fullName>
        <field>cfg_Meal_Gap__c</field>
        <literalValue>Medium</literalValue>
        <name>Set Meal Gap Level to M</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Meal_Gap_Level_to_VH</fullName>
        <field>cfg_Meal_Gap__c</field>
        <literalValue>Very High</literalValue>
        <name>Set Meal Gap Level to VH</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Name_to_School_Name</fullName>
        <description>Creates a composite school name</description>
        <field>Name</field>
        <formula>School_Number__c &amp; 
 CASE( TEXT(Borough__c),
&quot;Brooklyn&quot;, &quot; K &quot;,
&quot;Bronx&quot;, &quot; X &quot;,
&quot;Manhattan&quot;, &quot; M &quot;,
&quot;Queens&quot;, &quot; Q &quot;,
&quot;Staten Island&quot;, &quot; R &quot;,&quot;&quot;) &amp;  School_Name__c</formula>
        <name>Update Account Name to School Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Site_Name</fullName>
        <field>Name</field>
        <formula>IF( School_Host__c =&apos;&apos;, CBO_Name__c, CBO_Name__c + &quot; @ &quot; +  School_Host__r.Name )</formula>
        <name>Update Site Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CookShop%3A Meal Gap Level</fullName>
        <active>false</active>
        <description>sets meal gap level</description>
        <formula>ISCHANGED( Meals__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CookShop%3AUpdate Account Name to School Name</fullName>
        <actions>
            <name>Update_Account_Name_to_School_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>CookShop School</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CookShop%3AUpdate Account Name to Site Name</fullName>
        <actions>
            <name>Update_Site_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>CookShop Classroom for After School</value>
        </criteriaItems>
        <description>Updates the Account Name</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HOC__Append http to website</fullName>
        <actions>
            <name>HOC__Update_Website_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Website</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Website</field>
            <operation>notContain</operation>
            <value>://</value>
        </criteriaItems>
        <description>This rule updates the website field in the organization record so that it contains the http:// prefix if it is not present when the record is created.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HOC__Auto Populate Opportunity App%2E Mngr%2E Email</fullName>
        <actions>
            <name>HOC__Update_Approval_Manager_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule updates the opportunity approval manager email address in the organization record when the opportunity approval manager field is updated.</description>
        <formula>ISCHANGED(HOC__Opportunity_Approval_Manager__c) ||  ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HOC__New Organization Registered</fullName>
        <actions>
            <name>HOC__New_Organization_Registered</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Guest</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Nonprofit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nonprofit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.HOC__Mission_Statement__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This rule sends an email to the partner approval manager when a new agency registers through the agency section on the public site.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>HOC__Organizational profile updated notification</fullName>
        <actions>
            <name>HOC__Organizational_profile_updated_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule sends an email to the Partner approval manager when a partner updates their organization&apos;s mission, website, impact area, logo, Population served, or primary address fields.</description>
        <formula>(ISCHANGED(HOC__Mission_Statement__c)  || ISCHANGED(Name) || ISCHANGED(Website) || ISCHANGED( HOC__Logo_URL__c ) || ISCHANGED( HOC__Impact_Area__c ) || ISCHANGED( HOC__Populations_Served__c ) || ISCHANGED(  BillingCity  ) || ISCHANGED(   BillingCountry ) || ISCHANGED(   BillingState ) || ISCHANGED(   BillingStreet ) || ISCHANGED(    BillingPostalCode )) &amp;&amp;  ($Profile.Name = &apos;Partner Staff&apos; || CONTAINS(HOC__LastModifiedByV2__c, &apos;Partner Staff v2&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HOC__TaskWhenPartnerRegister</fullName>
        <actions>
            <name>HOC__New_Organization_has_registered</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>This rule creates a task when a new organization has been created and is awaiting being granted partner access. To access this organization click on the organization name in the &quot;related to&quot; field.</description>
        <formula>ISPICKVAL(CreatedBy.UserType, &apos;Guest&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>HOC__Update_Account_PrimaryAdd</fullName>
        <actions>
            <name>HOC__Upd_Account_PrimaryStreet</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>HOC__Update_Acc_PrimaryCity</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>HOC__Update_Acc_PrimaryState</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>HOC__Update_Acc_Zip</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule updates the Primary Address fields in the organization record to the correct address when the &quot;Primary Address?&quot; field or any of the components of the shipping or billing address fields are updated.</description>
        <formula>ISCHANGED( HOC__Primary_Address__c) || ISCHANGED(  BillingCity ) || ISCHANGED(  BillingStreet )  || ISCHANGED(  BillingState ) || ISCHANGED(  BillingPostalCode )  || ISCHANGED(  ShippingCity ) || ISCHANGED(  ShippingState ) || ISCHANGED(  ShippingStreet ) || ISCHANGED(  ShippingPostalCode ) ||  ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Meal Gap Level to H</fullName>
        <actions>
            <name>Set_Meal_Gap_Level_to_H</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Meals__c</field>
            <operation>greaterThan</operation>
            <value>4300000</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Meals__c</field>
            <operation>lessOrEqual</operation>
            <value>5800000</value>
        </criteriaItems>
        <description>Sets meal gap level to high when meals &gt;4.3M and &lt;5.8M</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Meal Gap Level to Low</fullName>
        <actions>
            <name>Set_Meal_Gap_Level_to_Low</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Meals__c</field>
            <operation>lessOrEqual</operation>
            <value>2900000</value>
        </criteriaItems>
        <description>Sets meal gap level to low when meals &lt;2.9M</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Meal Gap Level to M</fullName>
        <actions>
            <name>Set_Meal_Gap_Level_to_M</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Meals__c</field>
            <operation>lessOrEqual</operation>
            <value>4300000</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Meals__c</field>
            <operation>greaterThan</operation>
            <value>2900000</value>
        </criteriaItems>
        <description>Sets meal gap level to medium when meals &gt;2.9M and &lt;4.3M</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Meal Gap Level to VH</fullName>
        <actions>
            <name>Set_Meal_Gap_Level_to_VH</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Meals__c</field>
            <operation>greaterThan</operation>
            <value>5800000</value>
        </criteriaItems>
        <description>Sets meal gap level to high when meals &gt;5.8M</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>HOC__New_Organization_has_registered</fullName>
        <assignedTo>iobilo@foodbanknyc.org</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new organization has been created and is awaiting being granted partner access.  To access this organization click on the organization name in the &quot;related to&quot; field.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New Organization has registered</subject>
    </tasks>
</Workflow>
