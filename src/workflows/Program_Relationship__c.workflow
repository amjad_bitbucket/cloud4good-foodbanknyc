<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Add_to_Print_Tax_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Paper_File</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Add to Print Tax Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PR_Recertification_Needed</fullName>
        <description>Changes status to Recertification Needed</description>
        <field>Registration_Status__c</field>
        <literalValue>Recertification Needed</literalValue>
        <name>PR_Recertification Needed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PR_Set_Date_Benefits</fullName>
        <description>Set date=screening date</description>
        <field>Date__c</field>
        <formula>cfg_BA_Screening_Date__c</formula>
        <name>PR - Set Date - Benefits</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PR_Set_Date_Health</fullName>
        <description>Sets date = date of appointment</description>
        <field>Date__c</field>
        <formula>h_Appointment_Date__c</formula>
        <name>PR - Set Date - Health</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PR_Set_Date_Pantry</fullName>
        <description>Sets date for pantry record to Registration Date</description>
        <field>Date__c</field>
        <formula>Registration_Date__c</formula>
        <name>PR - Set Date - Pantry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PR_Update_Recert_Date</fullName>
        <description>Updates recertification date when client is recertified</description>
        <field>Date_Recertified__c</field>
        <formula>TODAY()</formula>
        <name>PR_Update Recert Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Program_Rel_Name</fullName>
        <field>Name</field>
        <formula>CASE( RecordType.Name, 
&quot;Food Pantry&quot;, &quot;P-&quot; &amp;  Auto_Number__c ,
&quot;Tax&quot;, &quot;T-&quot; &amp; Auto_Number__c ,
&quot;Benefits&quot;, &quot;B-&quot;&amp; Auto_Number__c ,
&quot;Health Navigation&quot;, &quot;H-&quot; &amp; Auto_Number__c ,
Name)</formula>
        <name>Set Program Rel Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Screening_Outcome</fullName>
        <description>Sets screening outcome to eligible</description>
        <field>cfg_BA_Screening_Outcome__c</field>
        <formula>&apos;Eligible&apos;</formula>
        <name>Set Screening Outcome</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stop_Tax_Calls</fullName>
        <description>Updates stop tax calls when limit is reached</description>
        <field>cfg_Stop_Calls__c</field>
        <literalValue>1</literalValue>
        <name>Stop Tax Calls Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tax_Move_Return_to_e_file_queue</fullName>
        <description>Moves the return that are not filed to the Awaiting Efile queue</description>
        <field>OwnerId</field>
        <lookupValue>Awaiting_EFile</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Tax - Move Return to e-file queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_follow_up_date</fullName>
        <description>sets follow up date to 30 days from submission date</description>
        <field>Follow_up_date__c</field>
        <formula>Submission_Date__c + 30</formula>
        <name>Update follow up date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PR - Set Date - Benefits</fullName>
        <actions>
            <name>PR_Set_Date_Benefits</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Program_Relationship__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Benefits</value>
        </criteriaItems>
        <criteriaItems>
            <field>Program_Relationship__c.cfg_BA_Screening_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Date is a generic field used by no record type. Workflow will copy different values depending on record type</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PR - Set Date - Health</fullName>
        <actions>
            <name>PR_Set_Date_Health</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Program_Relationship__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Health Navigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Program_Relationship__c.h_Appointment_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Date is a generic field used by no record type. Workflow will copy different values depending on record type</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PR - Set Date - Pantry</fullName>
        <actions>
            <name>PR_Set_Date_Pantry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Program_Relationship__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Food Pantry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Program_Relationship__c.Registration_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Date is a generic field used by no record type. Workflow will copy different values depending on record type</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PR_Recertification Completed</fullName>
        <actions>
            <name>PR_Update_Recert_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Has recertification been completed?</description>
        <formula>ISPICKVAL(PRIORVALUE ( Registration_Status__c ),  &quot;Recertification Needed&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PR_Recertification Needed</fullName>
        <active>true</active>
        <description>Sets status to &quot;recertification needed&quot; 1 year after registration date or 1 year after last recert date</description>
        <formula>OR(Registration_Date__c =TODAY(), Date_Recertified__c=TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>PR_Recertification_Needed</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>365</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Program Rel Name</fullName>
        <actions>
            <name>Set_Program_Rel_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Program_Relationship__c.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>sets program relationship name to start with letter of program</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Screening Outcome</fullName>
        <actions>
            <name>Set_Screening_Outcome</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Program_Relationship__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Benefits</value>
        </criteriaItems>
        <criteriaItems>
            <field>Program_Relationship__c.Estimated_Benefits__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>sets screening outcome if estimated SNAP &gt;0</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Stop Tax Calls</fullName>
        <actions>
            <name>Add_to_Print_Tax_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Stop_Tax_Calls</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Program_Relationship__c.cfg_Total_Tax_Calls__c</field>
            <operation>greaterOrEqual</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Program_Relationship__c.cfg_Stop_Calls__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Program_Relationship__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Tax</value>
        </criteriaItems>
        <description>When &quot;total tax calls&quot; reaches 3 or client is reached, set stop calls</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Tax - Return Ready to e-file</fullName>
        <actions>
            <name>Tax_Move_Return_to_e_file_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Program_Relationship__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Tax</value>
        </criteriaItems>
        <criteriaItems>
            <field>Program_Relationship__c.Preparation_Status__c</field>
            <operation>equals</operation>
            <value>Authorization to e-file received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Program_Relationship__c.Filing_Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>checks the tax record to see if it is ready to e-file</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Agency Name</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Program_Relationship__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Benefits</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update follow up date</fullName>
        <actions>
            <name>Update_follow_up_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Program_Relationship__c.Submission_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Program_Relationship__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Benefits</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
