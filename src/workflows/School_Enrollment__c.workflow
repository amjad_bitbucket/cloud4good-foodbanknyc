<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>cfg_Rename_School_Enrollment</fullName>
        <description>Rename School Enrollment Name to School Name + School Year + CookShop Enrollment</description>
        <field>Name</field>
        <formula>cfg_School__r.Name &amp; &quot;-&quot; &amp; TEXT (cfg_School_Year__c) &amp; &quot; CookShop Enrollment&quot;</formula>
        <name>Rename School Enrollment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>cfg_School_Enrollment_Classroom_Enrolled</fullName>
        <description>Changes Record Type from Classroom Application to Classroom Enrollment when School Enrollment Application Stage is set to Enrolled.</description>
        <field>RecordTypeId</field>
        <lookupValue>cfg_Classroom_Enrollment</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>School Enrollment - Classroom Enrolled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>cfg_School_Enrollment_EatWise_Enrolled</fullName>
        <description>Changes Record Type from EatWise Application to EatWise Enrollment when School Enrollment Application Stage is set to Enrolled.</description>
        <field>RecordTypeId</field>
        <lookupValue>cfg_EatWise_Enrollment</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>School Enrollment - EatWise Enrolled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <flowActions>
        <fullName>Update_Primary_CookShop_Coord</fullName>
        <description>Flow trigger to update the primary coordinator on the associated school record</description>
        <flow>Update_Primary_Coordinator_on_School</flow>
        <flowInputs>
            <name>varCoordinatorid</name>
            <value>{!cfg_Primary_Coordinator__r.Id}</value>
        </flowInputs>
        <flowInputs>
            <name>varSchoolId</name>
            <value>{!cfg_School__r.Id}</value>
        </flowInputs>
        <label>Update Primary CookShop Coord</label>
        <language>en_US</language>
        <protected>false</protected>
    </flowActions>
    <rules>
        <fullName>Set Name on CookShop School Enrollment</fullName>
        <actions>
            <name>cfg_Rename_School_Enrollment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>School_Enrollment__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CookShop for Classrooms Enrollment,EatWise Enrollment,CookShop Classroom for After School Enrollment</value>
        </criteriaItems>
        <description>Updates the name on CookShop School Enrollment</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Task when all classroom enrollments submitted</fullName>
        <actions>
            <name>Send_Classroom_Enrollment_Summary</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>School_Enrollment__c.All_Classrooms_Submitted__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When Coordinator indicates that all Classroom Enrollments have been submitted, assign a task to the account owner</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update to Primary Coordinator</fullName>
        <actions>
            <name>Update_Primary_CookShop_Coord</name>
            <type>FlowAction</type>
        </actions>
        <active>true</active>
        <description>checks to see if primary coordinator has been updated</description>
        <formula>ISCHANGED(cfg_Primary_Coordinator__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Send_Classroom_Enrollment_Summary</fullName>
        <assignedToType>owner</assignedToType>
        <description>The Coordinator has indicated that they have submitted all the Classroom Enrollments for their school. Please generate the Classroom Enrollment report and send it to the Coordinator.</description>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Classroom Enrollment Summary</subject>
    </tasks>
</Workflow>
