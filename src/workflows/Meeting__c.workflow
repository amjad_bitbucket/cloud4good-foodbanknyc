<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Registration_is_closed</fullName>
        <description>Sets Registration closed to True when meeting is at capacity</description>
        <field>Registration_is_Closed__c</field>
        <literalValue>1</literalValue>
        <name>Registration is closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Time_for_Meeting</fullName>
        <description>Set the value of the Date &amp; Time field based on Meeting Date and Meeting Time.</description>
        <field>Date_Time__c</field>
        <formula>DATETIMEVALUE(TEXT( Meeting_Date__c )+&quot; &quot;+LEFT( TEXT(Meeting_Time__c), LEN( TEXT(Meeting_Time__c))-3)+&quot;:00&quot;)+ 0.166666667</formula>
        <name>Update Date &amp; Time for Meeting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Close Reg when at capacity</fullName>
        <actions>
            <name>Registration_is_closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(cfg_Total_Attendees__c =  Meeting_Capacity__c , NOT( Allow_Waitlist__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Registration closed 1 day before event</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Meeting__c.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Close registration on the day before the event</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Registration_is_closed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Meeting__c.Meeting_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Date %26 Time for Meeting</fullName>
        <actions>
            <name>Update_Date_Time_for_Meeting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the standard Date &amp; Time field when date or time changes</description>
        <formula>OR(ISCHANGED( Meeting_Date__c ),  ISCHANGED(Meeting_Time__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
