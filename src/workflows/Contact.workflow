<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>HOC__New_volunteer_has_expressed_interest_in_self_organizing</fullName>
        <description>New volunteer has expressed interest in self-organizing</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HOC__HOC_Email_Templates/HOC__Volunteer_has_expressed_interest_in_self_organizing</template>
    </alerts>
    <alerts>
        <fullName>HOC__Organization_Registration_Activation</fullName>
        <description>Organization Registration Activation</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HOC__HOC_Email_Templates/HOC__Organization_Registration_Activation</template>
    </alerts>
    <alerts>
        <fullName>HOC__Registration_Confirmation</fullName>
        <description>Registration Confirmation</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HOC__HOC_Email_Templates/HOC__Registration_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>HOC__Self_Organized_Access_Granted</fullName>
        <description>Self-Organized Access Granted</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HOC__HOC_Email_Templates/HOC__Self_Organizing_Volunteer_Registration</template>
    </alerts>
    <alerts>
        <fullName>HOC__Volunteer_Leader_Registration</fullName>
        <description>Volunteer Leader Registration</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HOC__HOC_Email_Templates/HOC__Volunteer_Leader_Registration</template>
    </alerts>
    <alerts>
        <fullName>HOC__Volunteer_Registration</fullName>
        <description>Volunteer Registration</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HOC__HOC_Email_Templates/HOC__Volunteer_Registration</template>
    </alerts>
    <alerts>
        <fullName>HOC__Volunteer_Registration_activation</fullName>
        <description>Volunteer Registration activation</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>volunteer@foodbanknyc.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>HOC__HOC_Email_Templates/HOC__Volunteer_Registration_activation</template>
    </alerts>
    <alerts>
        <fullName>HOC__Volunteer_Registration_activation_for_an_existing_Contact</fullName>
        <description>Volunteer Registration activation for an existing Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HOC__HOC_Email_Templates/HOC__Volunteer_Registration_activation</template>
    </alerts>
    <fieldUpdates>
        <fullName>HOC__Upd_PCity</fullName>
        <field>HOC__Primary_City__c</field>
        <formula>IF( ISPICKVAL( HOC__Primary_Address__c ,&apos;Home&apos;) , MailingCity , IF(ISPICKVAL( HOC__Primary_Address__c ,&apos;Business&apos;), OtherCity, &apos;&apos;))</formula>
        <name>Upd_PCity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HOC__Upd_PPhone</fullName>
        <field>HOC__Primary_Phoneno__c</field>
        <formula>IF(ISPICKVAL(HOC__Primary_Phone__c ,&apos;Home&apos; ) ,  HomePhone , IF(ISPICKVAL(HOC__Primary_Phone__c ,&apos;Other&apos; ) , OtherPhone ,  IF(ISPICKVAL(HOC__Primary_Phone__c ,&apos;Business&apos; ) ,  Phone,  IF(ISPICKVAL(HOC__Primary_Phone__c ,&apos;Mobile&apos; ) ,  MobilePhone, &apos;&apos;) ) ) )</formula>
        <name>Upd_PPhone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HOC__Upd_PState</fullName>
        <field>HOC__Primary_State__c</field>
        <formula>IF( ISPICKVAL( HOC__Primary_Address__c ,&apos;Home&apos;) , MailingState  , IF(ISPICKVAL( HOC__Primary_Address__c ,&apos;Business&apos;), OtherState  , &apos;&apos;))</formula>
        <name>Upd_PState</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HOC__Upd_PStreet</fullName>
        <field>HOC__Primary_Street__c</field>
        <formula>IF( ISPICKVAL( HOC__Primary_Address__c ,&apos;Home&apos;) , MailingStreet  , IF(ISPICKVAL( HOC__Primary_Address__c ,&apos;Business&apos;), OtherStreet , &apos;&apos;))</formula>
        <name>Upd_PStreet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HOC__Upd_Zip</fullName>
        <field>HOC__Primary_Zip__c</field>
        <formula>IF( ISPICKVAL( HOC__Primary_Address__c ,&apos;Home&apos;) , MailingPostalCode  , IF(ISPICKVAL( HOC__Primary_Address__c ,&apos;Business&apos;), OtherPostalCode  , &apos;&apos;))</formula>
        <name>Upd_Zip</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HOC__Update_Registration_Status</fullName>
        <field>HOC__Registration_Status__c</field>
        <literalValue>Not Registered</literalValue>
        <name>Update Registration Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>cfg_Mark_CookShop_Families_Trained_TRUE</fullName>
        <description>Mark CookShop for Families Trained</description>
        <field>cfg_CookShop_for_Families_Trained__c</field>
        <literalValue>1</literalValue>
        <name>Mark CookShop Families Trained TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <flowActions>
        <fullName>Update_Numeric_Training</fullName>
        <description>Flow trigger to update the numeric training indicators on the CookShop Role record when the contact is updated.</description>
        <flow>UpdateCookShopRoleTrainingNumFields</flow>
        <flowInputs>
            <name>var_UpdatedContact</name>
            <value>{!Id}</value>
        </flowInputs>
        <label>Update Numeric Training</label>
        <language>en_US</language>
        <protected>false</protected>
    </flowActions>
    <rules>
        <fullName>CookShop%3A TrainingWasUpdated</fullName>
        <actions>
            <name>Update_Numeric_Training</name>
            <type>FlowAction</type>
        </actions>
        <active>true</active>
        <description>Checks to see if training fields were updated</description>
        <formula>OR(ISCHANGED( cfg_CookShop_for_Classroom_K2_Trained__c ),
 ISCHANGED( cfg_CookShop_for_Classroom_3_5_Trained__c ),
 ISCHANGED( cfg_CookShop_for_Families_Trained__c ),
ISCHANGED(  CookShop_for_Classroom_CCas_Trained__c  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HOC__New Self Organized Volunteer Registered</fullName>
        <actions>
            <name>HOC__New_volunteer_has_expressed_interest_in_self_organizing</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.HOC__Self_Organized__c</field>
            <operation>equals</operation>
            <value>Pending for Approval</value>
        </criteriaItems>
        <description>Rule to send to Partner Approval Manager when a Volunteer wants to be Self Organized.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HOC__Organization Registration Activation</fullName>
        <actions>
            <name>HOC__Organization_Registration_Activation</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.HOC__Profile_Name__c</field>
            <operation>equals</operation>
            <value>Partner Staff</value>
        </criteriaItems>
        <description>This rule sends an email to the contact when their user profile is set to Partner Staff.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>HOC__Registration Confirmation</fullName>
        <actions>
            <name>HOC__Registration_Confirmation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Guest</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nonprofit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Nonprofit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.HOC__Mission_Statement__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.HOC__Contact_Type__c</field>
            <operation>notEqual</operation>
            <value>Volunteer Coordinator</value>
        </criteriaItems>
        <description>This rule sends and email to the primary contact when a nonprofit agency registers on the public site.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>HOC__Self-Organized Access Granted</fullName>
        <actions>
            <name>HOC__Self_Organized_Access_Granted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(HOC__Self_Organized__c, &quot;Active&quot;) ,OR(ISNEW(),ISCHANGED(HOC__Self_Organized__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HOC__Upd_Primary_Phone</fullName>
        <actions>
            <name>HOC__Upd_PPhone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule updates the Primary Phone field to the correct phone number when the &quot;Primary Phone?&quot; field or any of the phone number fields are updated.</description>
        <formula>ISCHANGED(HOC__Primary_Phone__c ) || ISCHANGED( Phone )  || ISCHANGED( HomePhone  ) || ISCHANGED( MobilePhone) || ISCHANGED( OtherPhone  ) ||  ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HOC__Upd_Primay_Address</fullName>
        <actions>
            <name>HOC__Upd_PCity</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>HOC__Upd_PState</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>HOC__Upd_PStreet</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>HOC__Upd_Zip</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule updates the Primary Address fields in the contact record to the correct address when the &quot;Primary Address?&quot; field or any of the components of the shipping or billing address fields are updated.</description>
        <formula>ISCHANGED( HOC__Primary_Address__c ) || ISCHANGED( MailingCity ) ||  ISCHANGED(  MailingState ) || ISCHANGED(  MailingStreet ) || ISCHANGED(  MailingPostalCode ) || ISCHANGED(  OtherCity ) ||  ISCHANGED(   OtherState ) || ISCHANGED(   OtherStreet ) || ISCHANGED(   OtherPostalCode ) ||  ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HOC__Update Registration Status</fullName>
        <actions>
            <name>HOC__Update_Registration_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule sets the Registration Status in the contact record to &quot;Not Registered&quot; when the contact is not created via the public site.</description>
        <formula>AND( NOT(ISPICKVAL($User.UserType, &apos;High Volume Portal&apos;)), NOT(ISPICKVAL($User.UserType, &apos;Customer Portal Manager&apos;)), NOT(ISPICKVAL($User.UserType, &apos;Overage Customer Portal Manager Custom&apos;)), NOT(ISPICKVAL(HOC__Registration_Status__c ,&apos;Pending (Created via Portal)&apos;)) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>HOC__Volunteer Leader Registration</fullName>
        <actions>
            <name>HOC__Volunteer_Leader_Registration</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>This rule sends an email to the contact when their user profile is set to Volunteer Leader.</description>
        <formula>IF(  	AND(  		( HOC__Profile_Name__c &lt;&gt; PRIORVALUE(HOC__Profile_Name__c) ),  		(HOC__Profile_Name__c = &apos;Volunteer Leader&apos; || HOC__Profile_Name__c  = &apos;Volunteer Leader v2&apos;) ),true,false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HOC__Volunteer Registration</fullName>
        <active>false</active>
        <description>Deprecated. The function of this workflow has been moved to a trigger.</description>
        <formula>AND(ISCHANGED(HOC__Profile_Name__c),  HOC__Profile_Name__c = &apos;Volunteer&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HOC__Volunteer Registration activation</fullName>
        <actions>
            <name>HOC__Volunteer_Registration_activation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule sends an email to a contact when they register via the public site when the Profile equals Volunteer.</description>
        <formula>AND(ISPICKVAL(CreatedBy.UserType,  &apos;Guest&apos;),  Account.Name = &apos;Individual&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>HOC__Volunteer Registration activation for an existing Contact</fullName>
        <actions>
            <name>HOC__Volunteer_Registration_activation_for_an_existing_Contact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule sends an email out to a contact that already exists in the system and are updated to profile equal to Volunteer, such as when a contact is invited as a Team Member and then logs in an completes their registration.</description>
        <formula>AND(ISPICKVAL(LastModifiedBy.UserType, &apos;Guest&apos;), HOC__IsCustomerPortalUser__c  = True, HOC__Username__c =  Email )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
