<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Curriculum</fullName>
        <description>Set Curriculum on CookShop Role</description>
        <field>Curriculum__c</field>
        <formula>TEXT(Classroom_Enrollment__r.cfg_CookShop_Curriculum__c )</formula>
        <name>Set Curriculum</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_School_Year</fullName>
        <field>School_Year_number__c</field>
        <formula>VALUE(RIGHT(Classroom_Enrollment__r.cfg_School_Year__c, 4))</formula>
        <name>Set School Year</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CookShop_Role_Name</fullName>
        <field>Name</field>
        <formula>Contact__r.Full_Name__c &amp; &quot; - &quot; &amp;  TEXT(Role__c)</formula>
        <name>Update CookShop Role Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <flowActions>
        <fullName>Update_Primary_Facilitator_ID_and_Name</fullName>
        <flow>Update_Primary_Facilitator_ID_on_Classroom_Enrollment</flow>
        <flowInputs>
            <name>ClassroomEnrollment</name>
            <value>{!Classroom_Enrollment_ID__c}</value>
        </flowInputs>
        <flowInputs>
            <name>ContactID</name>
            <value>{!Contact_ID__c}</value>
        </flowInputs>
        <flowInputs>
            <name>PrimaryFacilitatorName</name>
            <value>{!Contact__r.Full_Name__c}</value>
        </flowInputs>
        <label>Update Primary Facilitator ID and Name</label>
        <language>en_US</language>
        <protected>false</protected>
    </flowActions>
    <flowActions>
        <fullName>Update_Secondary_Facilitator_ID_and_Name</fullName>
        <flow>Update_Secondary_Facilitator_ID_on_Classroom_Enrollment</flow>
        <flowInputs>
            <name>ClassroomEnrollment</name>
            <value>{!Classroom_Enrollment_ID__c}</value>
        </flowInputs>
        <flowInputs>
            <name>ContactID</name>
            <value>{!Contact_ID__c}</value>
        </flowInputs>
        <flowInputs>
            <name>SecondaryFacilitatorName</name>
            <value>{!Contact__r.Full_Name__c}</value>
        </flowInputs>
        <label>Update Secondary Facilitator ID and Name</label>
        <language>en_US</language>
        <protected>false</protected>
    </flowActions>
    <rules>
        <fullName>CookShop Role Field Updates</fullName>
        <actions>
            <name>Set_Curriculum</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_School_Year</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CookShop_Role__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update CookShop Role fields based on Classroom Enrollment values so that they can be used in Contact roll up summary fields (not possible with formula fields)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update CookShop Primary Facilitator ID</fullName>
        <actions>
            <name>Update_Primary_Facilitator_ID_and_Name</name>
            <type>FlowAction</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CookShop_Role__c.Role__c</field>
            <operation>contains</operation>
            <value>Primary</value>
        </criteriaItems>
        <description>Triggers an updated to the Classroom Enrollment with the ID of the Primary Facilitator when a primary CookShop Role is created or edited.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update CookShop Role Name</fullName>
        <actions>
            <name>Update_CookShop_Role_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CookShop_Role__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update CookShop Secondary Facilitator ID</fullName>
        <actions>
            <name>Update_Secondary_Facilitator_ID_and_Name</name>
            <type>FlowAction</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CookShop_Role__c.Role__c</field>
            <operation>contains</operation>
            <value>Secondary</value>
        </criteriaItems>
        <description>Triggers an update to the Classroom Enrollment with the ID of the Secondary Facilitator when a secondary CookShop Role is created or edited.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
