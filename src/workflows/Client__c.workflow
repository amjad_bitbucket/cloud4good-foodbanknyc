<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Assign_to_Call_Queue</fullName>
        <description>Assign client records to call queue</description>
        <field>OwnerId</field>
        <lookupValue>New_Pantry_Client_less_than_3_calls</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Call Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Client_Clear_Documented_Status</fullName>
        <field>Documented_Non_Citizen__c</field>
        <name>Client - Clear Documented Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Client_Clear_Ethnicity</fullName>
        <field>Ethnicity__c</field>
        <name>Client - Clear Ethnicity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Client_Clear_Marital_Status</fullName>
        <field>Marital_Status__c</field>
        <name>Client - Clear Marital Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Client_Clear_Race</fullName>
        <field>cfg_Race__c</field>
        <name>Client - Clear Race</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Client_Clear_Veteran_Status</fullName>
        <field>Veteran_status__c</field>
        <name>Client - Clear Veteran Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Move_to_Call_Queue</fullName>
        <description>Changes ownership to queue for calling</description>
        <field>OwnerId</field>
        <lookupValue>New_Client_no_benefits_no_calls</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Move to Call Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Client_Name</fullName>
        <description>Updates the client name field</description>
        <field>Name</field>
        <formula>Last_Name__c &amp; &quot;, &quot; &amp;  First_Name__c</formula>
        <name>Update Client Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_DOB</fullName>
        <field>DOB__c</field>
        <formula>TEXT(Month(Date_of_Birth__c))&amp; &quot;/&quot; &amp; TEXT(Day( Date_of_Birth__c)) &amp; &quot;/&quot; &amp; TEXT(Year( Date_of_Birth__c))</formula>
        <name>Update DOB</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Client - Clear Documented Status</fullName>
        <actions>
            <name>Client_Clear_Documented_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Clear Client Documented Status field if flow populates it with &quot;None&quot;</description>
        <formula>TEXT( Documented_Non_Citizen__c  ) = &quot;None&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Client - Clear Ethnicity</fullName>
        <actions>
            <name>Client_Clear_Ethnicity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Clear Client Race field if flow populates it with &quot;None&quot;</description>
        <formula>TEXT( Ethnicity__c) = &quot;None&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Client - Clear Marital Status</fullName>
        <actions>
            <name>Client_Clear_Marital_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Clear Client Marital Status field if flow populates it with &quot;None&quot;</description>
        <formula>TEXT( Marital_Status__c ) = &quot;None&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Client - Clear Race</fullName>
        <actions>
            <name>Client_Clear_Race</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Clear Client Race field if flow populates it with &quot;None&quot;</description>
        <formula>TEXT(cfg_Race__c) = &quot;None&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Client - Clear Veteran Status</fullName>
        <actions>
            <name>Client_Clear_Veteran_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Clear Client Veteran Status field if flow populates it with &quot;None&quot;</description>
        <formula>TEXT( Veteran_status__c ) = &quot;None&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy birthdate to DOB</fullName>
        <actions>
            <name>Update_DOB</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>needed for searching by DOB</description>
        <formula>AND(ISBLANK( DOB__c ), NOT(ISBLANK(Date_of_Birth__c )))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Pantry Client</fullName>
        <actions>
            <name>Move_to_Call_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Client__c.Receiving_SNAP__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Client__c.Pantry_Client__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Client__c.Call_Count__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <description>Identifies new pantry clients to put on the queue for benefits followup</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Pantry Client - 1-3 calls</fullName>
        <actions>
            <name>Assign_to_Call_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Client__c.Call_Count__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Client__c.Call_Count__c</field>
            <operation>lessThan</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Client__c.Receiving_SNAP__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Client__c.StopCalls__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Client Name to Last%2C First</fullName>
        <actions>
            <name>Update_Client_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>sets the client name field to last, first</description>
        <formula>OR( DATEVALUE(CreatedDate) = TODAY(),  ISCHANGED(First_Name__c)  , ISCHANGED(Last_Name__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
