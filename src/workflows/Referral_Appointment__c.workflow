<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_End_Time</fullName>
        <description>Sets the end time of an appointment to be 90 minutes after the start</description>
        <field>End_Time__c</field>
        <formula>Start_Time__c + 60/1440</formula>
        <name>Set End Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_End_Time_to_30_mins_after_start</fullName>
        <description>Sets the end time for 30 minutes after start for recertification appointments</description>
        <field>End_Time__c</field>
        <formula>Start_Time__c + 30/1440</formula>
        <name>Set End Time to 30 mins after start</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Start_Time</fullName>
        <description>Updates start time for appointment referral</description>
        <field>Start_Time__c</field>
        <formula>datetimevalue( Text_Start_Time__c )</formula>
        <name>Update Start Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set end time for New Applications</fullName>
        <actions>
            <name>Set_End_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Referral_Appointment__c.Appointment_Type__c</field>
            <operation>equals</operation>
            <value>New Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Referral_Appointment__c.Duration__c</field>
            <operation>notEqual</operation>
            <value>60</value>
        </criteriaItems>
        <description>Sets the duration of an appointment based on the type of appointment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set end time for recertification</fullName>
        <actions>
            <name>Set_End_Time_to_30_mins_after_start</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Referral_Appointment__c.Appointment_Type__c</field>
            <operation>equals</operation>
            <value>Recertification</value>
        </criteriaItems>
        <criteriaItems>
            <field>Referral_Appointment__c.Duration__c</field>
            <operation>notEqual</operation>
            <value>30</value>
        </criteriaItems>
        <description>Sets the duration of an appointment for recertification appointments</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
