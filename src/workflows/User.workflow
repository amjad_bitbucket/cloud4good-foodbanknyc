<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Tax_Community_User_Manager_Update</fullName>
        <field>ManagerId</field>
        <lookupValue>gtejeda@foodbanknyc.org</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Tax Community User Manager Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Tax Community User Manager Update</fullName>
        <actions>
            <name>Tax_Community_User_Manager_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>startsWith</operation>
            <value>Tax Community</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ManagerId</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>All Tax Community Users must have German as their manager</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
