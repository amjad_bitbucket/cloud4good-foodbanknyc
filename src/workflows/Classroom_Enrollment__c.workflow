<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Classroom_Enrollment_Name</fullName>
        <field>Name</field>
        <formula>&quot;Room #&quot; &amp; cfg_Room_Number__c &amp; &quot; - &quot; &amp; cfg_School_Year__c</formula>
        <name>Set Classroom Enrollment Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <flowActions>
        <fullName>Create_LVF</fullName>
        <flow>Create_LVF_2</flow>
        <flowInputs>
            <name>varClassroomEnrollmentID</name>
            <value>{!Id}</value>
        </flowInputs>
        <flowInputs>
            <name>varCurriculum</name>
            <value>{!cfg_CookShop_Curriculum__c}</value>
        </flowInputs>
        <label>Create LVF</label>
        <language>en_US</language>
        <protected>false</protected>
    </flowActions>
    <rules>
        <fullName>Create LVFs - Flow Trigger</fullName>
        <actions>
            <name>Create_LVF</name>
            <type>FlowAction</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Classroom_Enrollment__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to launch flow trigger to create LVFs for each classroom enrollment</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Create LVFs - Flow Trigger for Older Enrollments not triggered</fullName>
        <actions>
            <name>Create_LVF</name>
            <type>FlowAction</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Classroom_Enrollment__c.Create_LVF__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Workflow to launch flow trigger to create LVFs for each classroom enrollment for older enrollments that were not already created.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Name on Classroom Enrollment</fullName>
        <actions>
            <name>Set_Classroom_Enrollment_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Classroom_Enrollment__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Updates the name on classroom enrollment</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
