<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_status_of_LVF_to_Overdue</fullName>
        <description>Updates Verification Form Status to Overdue if the Verification Form is not submitted and the due date has passed.</description>
        <field>Stage__c</field>
        <literalValue>Overdue</literalValue>
        <name>Change status of LVF to Overdue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_LVF_to_Over_Due</fullName>
        <field>Stage__c</field>
        <literalValue>Not Submitted</literalValue>
        <name>Set LVF to Over Due</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Submitted_Date</fullName>
        <field>Submitted_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Submitted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_Submitted</fullName>
        <description>Update status from Not Submitted to Submitted</description>
        <field>Stage__c</field>
        <literalValue>Submitted</literalValue>
        <name>Update status to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change status of LVF to Overdue</fullName>
        <active>true</active>
        <description>Change status of LVF to Overdue if Is LVF Overdue is true.</description>
        <formula>Is_Verification_Form_Overdue__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Change_status_of_LVF_to_Overdue</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lesson_Verification_Form__c.Overdue_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Change status of LVF to Submitted</fullName>
        <actions>
            <name>Update_Submitted_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_status_to_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lesson_Verification_Form__c.Lesson_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Change status of LVF to Submitted when Facilitator and Date are completed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
