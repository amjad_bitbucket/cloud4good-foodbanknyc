<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_CookShop_Case_Opened</fullName>
        <description>New CookShop Case Opened</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderAddress>cookshop@foodbanknyc.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CookShop_Templates/New_CookShop_Case</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_CookShop_Case_Type_to_Class_Enrol</fullName>
        <description>Updates Case Type to Classroom Enrollment</description>
        <field>Type</field>
        <literalValue>Classroom Enrollment</literalValue>
        <name>Update CookShop Case Type to Class Enrol</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CookShop_Case_Type_to_CookShop_fo</fullName>
        <description>Update Case Type with CookShop for Families Participants</description>
        <field>Type</field>
        <literalValue>CookShop for Families Participants</literalValue>
        <name>Update CookShop Case Type to CookShop fo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CookShop_Case_Type_to_Curriculum</fullName>
        <description>Update Case Type with Curriculum</description>
        <field>Type</field>
        <literalValue>Curriculum</literalValue>
        <name>Update CookShop Case Type to Curriculum</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CookShop_Case_Type_to_FBNYC_Resou</fullName>
        <description>Update Case Type with FBNYC Resources Beyond CookShop</description>
        <field>Type</field>
        <literalValue>FBNYC Resources Beyond CookShop</literalValue>
        <name>Update CookShop Case Type to FBNYC Resou</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CookShop_Case_Type_to_Facilitator</fullName>
        <description>Update Case Type with Facilitator Feedback</description>
        <field>Type</field>
        <literalValue>Facilitator Feedback</literalValue>
        <name>Update CookShop Case Type to Facilitator</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CookShop_Case_Type_to_Grocery_Ord</fullName>
        <description>Update Case Type with Grocery Ordering</description>
        <field>Type</field>
        <literalValue>Grocery Ordering</literalValue>
        <name>Update CookShop Case Type to Grocery Ord</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CookShop_Case_Type_to_Lesson_Veri</fullName>
        <description>Update Case Type with Lesson Verification Forms</description>
        <field>Type</field>
        <literalValue>Lesson Verification Forms</literalValue>
        <name>Update CookShop Case Type to Lesson Veri</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CookShop_Case_Type_to_Program_Mat</fullName>
        <description>Update Case Type with Program Materials</description>
        <field>Type</field>
        <literalValue>Program Materials</literalValue>
        <name>Update CookShop Case Type to Program Mat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CookShop_Case_Type_to_Program_Sch</fullName>
        <description>Update Case Type with Program Schedule</description>
        <field>Type</field>
        <literalValue>Program Schedule</literalValue>
        <name>Update CookShop Case Type to Program Sch</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CookShop_Case_Type_to_Recruitment</fullName>
        <description>Update Case Type with Recruitment</description>
        <field>Type</field>
        <literalValue>Recruitment</literalValue>
        <name>Update CookShop Case Type to Recruitment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CookShop_Case_Type_to_Training</fullName>
        <description>Update Case Type with Training</description>
        <field>Type</field>
        <literalValue>Training</literalValue>
        <name>Update CookShop Case Type to Training</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>New CookShop Case</fullName>
        <actions>
            <name>New_CookShop_Case_Opened</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CookShop External</value>
        </criteriaItems>
        <description>Tests to see if a new case has been created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update CookShop Case Type to Classroom Enrollment</fullName>
        <actions>
            <name>Update_CookShop_Case_Type_to_Class_Enrol</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CookShop_Case_Type__c</field>
            <operation>equals</operation>
            <value>Classroom Enrollment</value>
        </criteriaItems>
        <description>Updates standard Case Type field with Classroom Enrollment</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update CookShop Case Type to CookShop for Families Participants</fullName>
        <actions>
            <name>Update_CookShop_Case_Type_to_CookShop_fo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CookShop_Case_Type__c</field>
            <operation>equals</operation>
            <value>CookShop for Families Participants</value>
        </criteriaItems>
        <description>Updates standard Case Type field with CookShop for Families Participants</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update CookShop Case Type to Curriculum</fullName>
        <actions>
            <name>Update_CookShop_Case_Type_to_Curriculum</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CookShop_Case_Type__c</field>
            <operation>equals</operation>
            <value>Curriculum</value>
        </criteriaItems>
        <description>Updates standard Case Type field with Curriculum</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update CookShop Case Type to FBNYC Resources Beyond CookShop</fullName>
        <actions>
            <name>Update_CookShop_Case_Type_to_FBNYC_Resou</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CookShop_Case_Type__c</field>
            <operation>equals</operation>
            <value>FBNYC Resources Beyond CookShop</value>
        </criteriaItems>
        <description>Updates standard Case Type field with FBNYC Resources Beyond CookShop</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update CookShop Case Type to Facilitator Feedback</fullName>
        <actions>
            <name>Update_CookShop_Case_Type_to_Facilitator</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CookShop_Case_Type__c</field>
            <operation>equals</operation>
            <value>Facilitator Feedback</value>
        </criteriaItems>
        <description>Updates standard Case Type field with Facilitator Feedback</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update CookShop Case Type to Grocery Ordering</fullName>
        <actions>
            <name>Update_CookShop_Case_Type_to_Grocery_Ord</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CookShop_Case_Type__c</field>
            <operation>equals</operation>
            <value>Grocery Ordering</value>
        </criteriaItems>
        <description>Updates standard Case Type field with Grocery Ordering</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update CookShop Case Type to Lesson Verification Forms</fullName>
        <actions>
            <name>Update_CookShop_Case_Type_to_Lesson_Veri</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CookShop_Case_Type__c</field>
            <operation>equals</operation>
            <value>Lesson Verification Forms</value>
        </criteriaItems>
        <description>Updates standard Case Type field with Lesson Verification Forms</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update CookShop Case Type to Program Materials</fullName>
        <actions>
            <name>Update_CookShop_Case_Type_to_Program_Mat</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CookShop_Case_Type__c</field>
            <operation>equals</operation>
            <value>Program Materials</value>
        </criteriaItems>
        <description>Updates standard Case Type field with Program Materials</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update CookShop Case Type to Program Schedule</fullName>
        <actions>
            <name>Update_CookShop_Case_Type_to_Program_Sch</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CookShop_Case_Type__c</field>
            <operation>equals</operation>
            <value>Program Schedule</value>
        </criteriaItems>
        <description>Updates standard Case Type field with Program Schedule</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update CookShop Case Type to Recruitment</fullName>
        <actions>
            <name>Update_CookShop_Case_Type_to_Recruitment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CookShop_Case_Type__c</field>
            <operation>equals</operation>
            <value>Recruitment</value>
        </criteriaItems>
        <description>Updates standard Case Type field with Recruitment</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update CookShop Case Type to Training</fullName>
        <actions>
            <name>Update_CookShop_Case_Type_to_Training</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CookShop_Case_Type__c</field>
            <operation>equals</operation>
            <value>Training</value>
        </criteriaItems>
        <description>Updates standard Case Type field with Training</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
