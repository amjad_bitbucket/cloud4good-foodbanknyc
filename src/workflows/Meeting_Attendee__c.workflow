<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_meeting_attendee_with</fullName>
        <description>Thank you email to meeting attendee</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CookShop_Templates/Thank_You_for_attending</template>
    </alerts>
    <alerts>
        <fullName>Registration_Confirmation_Email</fullName>
        <ccEmails>creuman@gmail.com</ccEmails>
        <description>Registration Confirmation Email</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CookShop_Templates/Registration_Confirmation</template>
    </alerts>
    <fieldUpdates>
        <fullName>Did_not_attend</fullName>
        <description>Updates attended field to false</description>
        <field>Attended__c</field>
        <literalValue>0</literalValue>
        <name>Did not attend</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Did_not_attend_CCas</fullName>
        <description>Unchecks CCas Training field</description>
        <field>CookShop_for_Classroom_CCas_Trained__c</field>
        <literalValue>0</literalValue>
        <name>Did not attend - CCas</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Not_trained_Families</fullName>
        <description>Unchecks Families training box</description>
        <field>cfg_CookShop_for_Families_Trained__c</field>
        <literalValue>0</literalValue>
        <name>Not trained - Families</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Attended</fullName>
        <field>Attended__c</field>
        <literalValue>1</literalValue>
        <name>Set Attended</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Attended_Orientation</fullName>
        <field>cfg_Attended_Orientation__c</field>
        <literalValue>1</literalValue>
        <name>Set Attended Orientation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Coordinator_Trained</fullName>
        <description>unchecks coordinator trained box</description>
        <field>CookShop_Coordinator_Trained__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Coordinator Trained</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_3_5_Training</fullName>
        <description>Updates 3-5 Training field on contact</description>
        <field>cfg_CookShop_for_Classroom_3_5_Trained__c</field>
        <literalValue>1</literalValue>
        <name>Update 3-5 Training</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_3_5_Training_uncheck</fullName>
        <description>unchecks 3-5 Training field</description>
        <field>cfg_CookShop_for_Classroom_3_5_Trained__c</field>
        <literalValue>0</literalValue>
        <name>Update 3-5 Training - uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CCas_Training</fullName>
        <description>Updates CCas Training field on Contact</description>
        <field>CookShop_for_Classroom_CCas_Trained__c</field>
        <literalValue>1</literalValue>
        <name>Update CCas Training</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Coordinator_Training</fullName>
        <description>Updates coordinator trained fields</description>
        <field>CookShop_Coordinator_Trained__c</field>
        <literalValue>1</literalValue>
        <name>Update Coordinator Training</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Families_Training</fullName>
        <description>Updates Families Training Fields</description>
        <field>cfg_CookShop_for_Families_Trained__c</field>
        <literalValue>1</literalValue>
        <name>Update Families Training</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_K_2_Training</fullName>
        <description>Updates K-2 Training field on contact</description>
        <field>cfg_CookShop_for_Classroom_K2_Trained__c</field>
        <literalValue>1</literalValue>
        <name>Update K-2 Training</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_K_2_Training_uncheck</fullName>
        <description>unchecks K-2 Trained field</description>
        <field>cfg_CookShop_for_Classroom_K2_Trained__c</field>
        <literalValue>0</literalValue>
        <name>Update K-2 Training - uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Attendee checked in%2Fout - 3-5</fullName>
        <actions>
            <name>Set_Attended</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_3_5_Training</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_in__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_out__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Curriculum__c</field>
            <operation>equals</operation>
            <value>CookShop 3-5 Curriculum</value>
        </criteriaItems>
        <description>Updates attended and 3-5 Trained fields</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Attendee checked in%2Fout - CCas</fullName>
        <actions>
            <name>Set_Attended</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_CCas_Training</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_in__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_out__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Curriculum__c</field>
            <operation>contains</operation>
            <value>CookShop Classroom for After-School</value>
        </criteriaItems>
        <description>Updates attended and CCas Trained fields</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Attendee checked in%2Fout - Coordinator</fullName>
        <actions>
            <name>Set_Attended</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Coordinator_Training</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_in__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_out__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Curriculum__c</field>
            <operation>equals</operation>
            <value>CookShop Coordinators</value>
        </criteriaItems>
        <description>Updates attended and Coordinator Trained fields</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Attendee checked in%2Fout - Family</fullName>
        <actions>
            <name>Set_Attended</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Families_Training</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_in__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_out__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Curriculum__c</field>
            <operation>equals</operation>
            <value>CookShop for Families</value>
        </criteriaItems>
        <description>Updates attended and Family Trained fields</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Attendee checked in%2Fout - K-2</fullName>
        <actions>
            <name>Set_Attended</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_K_2_Training</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_in__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_out__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Curriculum__c</field>
            <operation>equals</operation>
            <value>CookShop K-2 Curriculum</value>
        </criteriaItems>
        <description>Updates attended and K-2 Trained fields</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Attendee checked in%2Fout - Orientation</fullName>
        <actions>
            <name>Set_Attended</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Attended_Orientation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_in__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_out__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Curriculum__c</field>
            <operation>contains</operation>
            <value>Orientation</value>
        </criteriaItems>
        <description>Updates attended and Orientation fields</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Did not attend - 3-5</fullName>
        <actions>
            <name>Did_not_attend</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_3_5_Training_uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_in__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_out__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Curriculum__c</field>
            <operation>equals</operation>
            <value>CookShop 3-5 Curriculum</value>
        </criteriaItems>
        <description>Updates attended and 3-5 Trained fields</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Did not attend - CCas</fullName>
        <actions>
            <name>Did_not_attend</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Did_not_attend_CCas</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_in__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_out__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Curriculum__c</field>
            <operation>contains</operation>
            <value>CookShop Classroom for After-School</value>
        </criteriaItems>
        <description>Updates attended and CCas Trained fields</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Did not attend - Coordinator</fullName>
        <actions>
            <name>Did_not_attend</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_Coordinator_Trained</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_in__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_out__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Curriculum__c</field>
            <operation>equals</operation>
            <value>CookShop Coordinators</value>
        </criteriaItems>
        <description>Updates attended and Coordinator Trained fields if contact did not attend</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Did not attend - Families</fullName>
        <actions>
            <name>Did_not_attend</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Not_trained_Families</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_in__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_out__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Curriculum__c</field>
            <operation>equals</operation>
            <value>CookShop for Families</value>
        </criteriaItems>
        <description>Updates attended and Family Trained fields</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Did not attend - K-2</fullName>
        <actions>
            <name>Did_not_attend</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_K_2_Training_uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_in__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Check_out__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Meeting_Attendee__c.Curriculum__c</field>
            <operation>equals</operation>
            <value>CookShop K-2 Curriculum</value>
        </criteriaItems>
        <description>Updates attended and K-2 Trained fields</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Meeting Attendance thank you email</fullName>
        <actions>
            <name>Email_meeting_attendee_with</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Meeting_Attendee__c.Attended__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Email meeting attendee when they attend</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Registration Confirmation email</fullName>
        <actions>
            <name>Registration_Confirmation_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Meeting_Attendee__c.Registration_Status__c</field>
            <operation>equals</operation>
            <value>Registered</value>
        </criteriaItems>
        <description>Email meeting attendee when they register</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
