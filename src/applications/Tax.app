<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Tax app for the FBNYC tax program.</description>
    <label>Tax</label>
    <tab>standard-Account</tab>
    <tab>Client__c</tab>
    <tab>Program_Relationship__c</tab>
    <tab>Tax_In_Person_Intake</tab>
    <tab>Taxes_Awaiting_Assignment</tab>
    <tab>Taxes_for_Call_Back</tab>
    <tab>Virtual_Intake</tab>
    <tab>Look_Up_Tax_Case_Status</tab>
    <tab>View_Site_Tax_Cases</tab>
    <tab>standard-report</tab>
    <tab>Taxes_Ready_to_E_file</tab>
    <tab>Tax_Self_Preparation</tab>
    <tab>Authorization_Attachedpg</tab>
    <tab>Tax_Cases_Updated</tab>
    <tab>View_Cases_Assigned_to_Preparers</tab>
    <tab>View_Site_Cases_Virtual_Only</tab>
    <tab>Send_by_DocuSign</tab>
    <tab>View_State_Only_Returns</tab>
    <tab>Unable_to_Reach_Client</tab>
    <tab>Site_Tax_Cases_Awaiting_Quality_Review</tab>
    <tab>View_Assigned_to_Quality_Review</tab>
    <tab>VITA_Program</tab>
    <tab>Tax_Cases_that_need_help</tab>
    <tab>Tax_Cases_with_Incoming_Texts</tab>
    <tab>Tax_Program_Relationship</tab>
</CustomApplication>
