<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Used by benefits specialists to access clients, created benefits assessments and follow up information</description>
    <label>Benefits Access</label>
    <tab>Client__c</tab>
    <tab>Client_Lookup</tab>
    <tab>SLCA2__Calendar_Anything</tab>
    <tab>Program_Relationship__c</tab>
    <tab>standard-Case</tab>
    <tab>Import_EPART</tab>
    <tab>standard-report</tab>
</CustomApplication>
